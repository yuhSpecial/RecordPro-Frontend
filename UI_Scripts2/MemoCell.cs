﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Scripts2
{
    public partial class MemoCell : UserControl
    {
        private Api api;
        public int MemoID;
        public int UserID;

        public MemoCell()
        {
            InitializeComponent();
            api = new Api();
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            UCinbox_memo panel = (UCinbox_memo)EditButton.Parent.Parent.Parent;
            panel.timer1.Start();
            panel.AddMemoPanel.Visible = true;
            panel.AddBtn.Text = "UPDATE";
            panel.MemoID = this.MemoID;
            panel.UserID = this.UserID;

            panel.remarkTextBox.Text = DescLabel.Text;
            if (DescLabel.Text == "No remarks.")
                panel.remarkTextBox.Clear();
            if (TypeLabel.Text == "Birth")
                panel.Birth.Checked = true;
            else if (TypeLabel.Text == "Important")
                panel.Important.Checked = true;
            else if (TypeLabel.Text == "Special")
                panel.Special.Checked = true;
            panel.DateTimePicker.Value = Convert.ToDateTime(DateLabel.Text);
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            UCinbox_memo panel = (UCinbox_memo)DeleteButton.Parent.Parent.Parent;
            string state = api.DeleteUserMemosAsync(Api.Token, MemoID);
            if (state == "OK")
            {
                MessageBox.Show("Deleting succeed");
                panel.MemosShow();
            }
            else
                MessageBox.Show(state);
        }
    }
}
