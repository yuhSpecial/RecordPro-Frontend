﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace UI_Scripts2
{
    public class NotesStatistic
    {
        [JsonProperty("negative_ratio")]
        public float negative_ratio;

        [JsonProperty("positive_ratio")]
        public float positive_ratio;

        [JsonProperty("neutral_ratio")]
        public float neutral_ratio;

        [JsonProperty("sentimen_mean")]
        public int sentimen_mean;
        //        {
        //  "negative_ratio": 0.5,
        //  "positive_ratio": 0.5,
        //  "neutral_ratio": 0,
        //  "sentiment_mean": 1,

        [JsonProperty("sentiment_mode")]
        public Sentiment_Mode sentiment_mode;

        //  "sentiment_mode": {
        //    "sentiment": 2,
        //    "times": 3
        //  },
        //  "wordcount_mean": 19,
        [JsonProperty("wordcount_mean")]
        public int wordcount_mean;

        [JsonProperty("wordcount_least")]
        public WordCount_Least wordcount_least;
        //  "wordcount_least": {
        //    "content": "拉跨",
        //    "wordcount": 2
        //  },
        [JsonProperty("topthree")]
        public List<TopThree> topthree;

        //  "topthree": [
        //    {
        //      "key": "哔哩哔",
        //      "count": 3
        //    },
        //    {
        //      "key": "进程",
        //      "count": 1
        //    },
        //    {
        //      "key": "资源",
        //      "count": 1
        //    }
        //  ]
        //}


    }
    public class Sentiment_Mode
    {
        [JsonProperty("sentiment")]
        public int sentiment;

        [JsonProperty("times")]
        public int times;
    }
    public class TopThree
    {
        [JsonProperty("key")]
        public string key;

        [JsonProperty("count")]
        public int count;
    }
    public class WordCount_Least
    {
        [JsonProperty("content")]
        public string content;

        [JsonProperty("wordcount")]
        public int wordcount;
    }
}
