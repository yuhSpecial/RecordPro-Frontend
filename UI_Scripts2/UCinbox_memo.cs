﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Scripts2
{
    public partial class UCinbox_memo : UserControl
    {
        private Api api;
        DateTime Today = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));  //当前日期
        List<UserMemorials> userMemos;  //纪念日列表
        public int MemoID;
        public int UserID;

        public UCinbox_memo() //构造函数
        {
            InitializeComponent();
            api = new Api();
            todayLabel.Text = "Today: "+ Today.ToString("yyyy-MM-dd");
            AddMemoPanel.Visible = false;
            //userMemos = api.GetUserMemosAsync(Api.Token);
            MemosShow();
        }

        private void closePic_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void MemosShow() //显示纪念日流水
        {
            userMemos = api.GetUserMemosAsync(Api.Token);
            userMemos = userMemos.OrderByDescending(u => u.Date).ToList();
            Today = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
            MemoTable.Controls.Clear();
            foreach (var userMemo in userMemos)
            {
                MemoCell memoCell = new MemoCell();

                memoCell.MemoID = userMemo.Id;
                memoCell.UserID = userMemo.UserId;
                memoCell.TypeLabel.Text = userMemo.Type;
                memoCell.DateLabel.Text = userMemo.Date.ToString("yyyy-MM-dd");
                memoCell.DescLabel.Text = userMemo.Desc;

                int daysleft;
                System.TimeSpan duration = userMemo.Date - Today;
                daysleft = duration.Days;
                memoCell.LeftDaysLabel.Text = daysleft.ToString();

                MemoTable.Controls.Add(memoCell); //加入到MemoTable中
                memoCell.Dock = DockStyle.Left;
            }
        }

        private void AddBtn_Click(object sender, EventArgs e) //确认添加或更新一个纪念日
        {
            string type = "Special";
            if (Birth.Checked)
                type = "Birth";
            else if (Important.Checked)
                type = "Important";
            else if (Special.Checked)
                type = "Special";
            else
            {
                MessageBox.Show("Please check the type"); return;
            }

            string desc;
            if (remarkTextBox.Text != "")
                desc = remarkTextBox.Text;
            else
                desc = "No remarks.";

            DateTime date = DateTimePicker.Value;

            //调用api
            if (AddBtn.Text == "ADD")
            {
                PostMemo postMemo = new PostMemo(type, desc, date, Api.Token);
                string state = api.PostUserMemosAsync(postMemo);
                if (state == "Created")
                {
                    MessageBox.Show("Post succeed");
                    Birth.Checked = false;
                    Important.Checked = false;
                    Special.Checked = false;
                    remarkTextBox.Clear();
                    userMemos = api.GetUserMemosAsync(Api.Token);
                    MemosShow();
                }
                else
                    MessageBox.Show(state);
            }
            else if (AddBtn.Text == "UPDATE")
            {
                PutMemo putMemo = new PutMemo(MemoID, type, desc, date, UserID);
                string state = api.PutUserMemosAsync(UserID, putMemo);
                //MessageBox.Show("MemoID: " + MemoID + "\ntype: " + type + "\ndesc: " + desc + "\ndate: " + date + "\nuser id: " + UserID);
                if (state == "OK")
                {
                    MessageBox.Show("Update succeed");
                    Birth.Checked = false;
                    Important.Checked = false;
                    Special.Checked = false;
                    remarkTextBox.Clear();
                    AddBtn.Text = "ADD";
                    //userMemos = api.GetUserMemosAsync(Api.Token);
                    MemosShow();
                }
                else
                    MessageBox.Show(state);
            }
        }


        private void AddMemoButton_Click(object sender, EventArgs e) //开始添加一条纪念日
        {
            if (AddMemoButton.Text == "+")
            {
                AddMemoPanel.Visible = true;
                DateTimePicker.Value = DateTime.Now;
                AddMemoPanel.Location = new Point(3, 537 + 134);

                AddBtn.Text = "ADD";
                remarkTextBox.Clear();

                timer1.Start();
            }
            else
            {
                timer1.Start();
                AddBtn.Text = "ADD";
                Birth.Checked = false;
                Important.Checked = false;
                Special.Checked = false;
                remarkTextBox.Clear();
            }
        }

        private int loc_y = 537 + 134;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (AddMemoButton.Text == "+")
            {
                if (loc_y > 527)
                {
                    loc_y -= 30;
                    AddMemoPanel.Location = new Point(3, loc_y);
                    AddMemoButton.Location = new Point(453, loc_y - 70);
                }
                else
                {
                    AddMemoButton.Text = "×";
                    timer1.Stop();
                    todayLabel.Visible = false;
                    return;
                }
            }
            else
            {
                if (loc_y < 527 + 134)
                {
                    loc_y += 30;
                    AddMemoPanel.Location = new Point(3, loc_y);
                    AddMemoButton.Location = new Point(453, loc_y - 70);
                }
                else
                {
                    AddMemoButton.Text = "+";
                    timer1.Stop();
                    AddMemoPanel.Visible = false;
                    todayLabel.Visible = true;
                    return;
                }
            }
        }
    }
}
