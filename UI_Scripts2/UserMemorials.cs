﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace UI_Scripts2
{
    //    {
    //id integer($int32)
    //纪念日ID

    //type    string
    //nullable: true
    //纪念日类型:Birth/Important/Special

    //date    string ($date-time)
    //纪念日时间，时、分、秒均为0

    //desc    string
    //nullable: true
    //备注

    //userid  integer($int32)
    //用户ID

    //}
    class UserMemorials
    { 
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("desc")]
        public string Desc { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("userid")]
        public int UserId { get; set; }

        public override string ToString()
        {
            return Date.ToString();
        }
    }

    class PutMemo
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("desc")]
        public string Desc { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("userid")]
        public int UserId { get; set; }

        public PutMemo(int id, string type, string desc, DateTime date, int userid)
        {
            Id = id;
            Type = type;
            Desc = desc;
            Date = date;
            UserId = userid;
        }
    }

    class PostMemo
    {

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("desc")]
        public string Desc { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        public PostMemo( string type, string desc, DateTime date, string token)
        {
            Type = type;
            Desc = desc;
            Date = date;
            Token = token;
        }
    }
}
