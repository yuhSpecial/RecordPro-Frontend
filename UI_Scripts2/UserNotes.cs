﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace UI_Scripts2
{
    //{
    //id integer($int32)
    //文本id

    //content string
    //nullable: true
    //文本内容

    //wordcount   integer($int32)
    //字数

    //sentiment   integer($int32)
    //情感极性，0：负向；1：中性；2：正向

    //tags    string
    //nullable: true
    //文本关注点标签

    //dateTime    string ($date-time)
    //时间

    //userid  integer($int32)
    //用户id

    //}
    class UserNotes
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("content")]
        public string Content{ get; set; }

        [JsonProperty("wordcount")]
        public int Wordcount { get; set; }

        [JsonProperty("sentiment")]
        public int Sentiment { get; set; }

        [JsonProperty("tags")]
        public string Tags { get; set; }

        [JsonProperty("dateTime")]
        public DateTime Date { get; set; }

        [JsonProperty("userid")]
        public int UserId { get; set; }

        public override string ToString()
        {
            return Date.ToString();
        }
    }
    class PostNote
    {
        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("wordcount")]
        public int Wordcount { get; set; }

        [JsonProperty("dateTime")]
        public DateTime Date { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }
        public PostNote(string content, int wordcount, DateTime dateTime, string token)
        {
            Content = content;
            Wordcount = wordcount;
            Date = dateTime;
            Token = token;
        }
    }

}