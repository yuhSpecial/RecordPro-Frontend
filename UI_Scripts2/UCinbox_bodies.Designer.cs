﻿namespace UI_Scripts2
{
    partial class UCinbox_bodies
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BodyTable = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.ShowBodies = new Guna.UI2.WinForms.Guna2Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DaysAmount = new Guna.UI2.WinForms.Guna2NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.StartAddButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.AddingBodyPanel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.AddData = new Guna.UI2.WinForms.Guna2TileButton();
            this.guna2ShadowPanel4 = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.Hips_textbox = new System.Windows.Forms.TextBox();
            this.guna2Button6 = new Guna.UI2.WinForms.Guna2Button();
            this.Waist_textbox = new System.Windows.Forms.TextBox();
            this.Bust_textbox = new System.Windows.Forms.TextBox();
            this.Height_textbox = new System.Windows.Forms.TextBox();
            this.Shoulder_textbox = new System.Windows.Forms.TextBox();
            this.Weight_textbox = new System.Windows.Forms.TextBox();
            this.guna2Button5 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button4 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button3 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button2 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.DateTimePicker = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2ShadowPanel1 = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.Weight = new System.Windows.Forms.Label();
            this.Height = new System.Windows.Forms.Label();
            this.cartesianChart2 = new LiveCharts.WinForms.CartesianChart();
            this.cartesianChart1 = new LiveCharts.WinForms.CartesianChart();
            this.CloseButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DaysAmount)).BeginInit();
            this.AddingBodyPanel.SuspendLayout();
            this.guna2ShadowPanel4.SuspendLayout();
            this.guna2ShadowPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BodyTable
            // 
            this.BodyTable.AutoScroll = true;
            this.BodyTable.BackColor = System.Drawing.Color.Transparent;
            this.BodyTable.EdgeWidth = 1;
            this.BodyTable.FillColor = System.Drawing.Color.White;
            this.BodyTable.Location = new System.Drawing.Point(24, 74);
            this.BodyTable.Name = "BodyTable";
            this.BodyTable.Radius = 20;
            this.BodyTable.ShadowColor = System.Drawing.Color.MidnightBlue;
            this.BodyTable.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.BodyTable.Size = new System.Drawing.Size(439, 437);
            this.BodyTable.TabIndex = 47;
            // 
            // ShowBodies
            // 
            this.ShowBodies.BorderRadius = 10;
            this.ShowBodies.CheckedState.Parent = this.ShowBodies;
            this.ShowBodies.CustomImages.Parent = this.ShowBodies;
            this.ShowBodies.FillColor = System.Drawing.Color.MidnightBlue;
            this.ShowBodies.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.ShowBodies.ForeColor = System.Drawing.Color.White;
            this.ShowBodies.HoverState.Parent = this.ShowBodies;
            this.ShowBodies.Location = new System.Drawing.Point(277, 23);
            this.ShowBodies.Name = "ShowBodies";
            this.ShowBodies.ShadowDecoration.Parent = this.ShowBodies;
            this.ShowBodies.Size = new System.Drawing.Size(130, 45);
            this.ShowBodies.TabIndex = 64;
            this.ShowBodies.Text = "SHOW";
            this.ShowBodies.Click += new System.EventHandler(this.ShowBodies_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.DaysAmount);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(53, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 57);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 29);
            this.label1.TabIndex = 48;
            this.label1.Text = "Last";
            // 
            // DaysAmount
            // 
            this.DaysAmount.BackColor = System.Drawing.Color.Transparent;
            this.DaysAmount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.DaysAmount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.DaysAmount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.DaysAmount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.DaysAmount.DisabledState.Parent = this.DaysAmount;
            this.DaysAmount.DisabledState.UpDownButtonFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(177)))), ((int)(((byte)(177)))));
            this.DaysAmount.DisabledState.UpDownButtonForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(203)))), ((int)(((byte)(203)))));
            this.DaysAmount.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.DaysAmount.FocusedState.Parent = this.DaysAmount;
            this.DaysAmount.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DaysAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.DaysAmount.Location = new System.Drawing.Point(58, 17);
            this.DaysAmount.Name = "DaysAmount";
            this.DaysAmount.ShadowDecoration.Parent = this.DaysAmount;
            this.DaysAmount.Size = new System.Drawing.Size(65, 30);
            this.DaysAmount.TabIndex = 47;
            this.DaysAmount.UpDownButtonFillColor = System.Drawing.Color.MidnightBlue;
            this.DaysAmount.UpDownButtonForeColor = System.Drawing.Color.White;
            this.DaysAmount.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(129, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 29);
            this.label2.TabIndex = 49;
            this.label2.Text = "Days";
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 30;
            this.guna2Elipse1.TargetControl = this;
            // 
            // timer1
            // 
            this.timer1.Interval = 5;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // StartAddButton
            // 
            this.StartAddButton.CheckedState.Parent = this.StartAddButton;
            this.StartAddButton.CustomImages.Parent = this.StartAddButton;
            this.StartAddButton.FillColor = System.Drawing.Color.MidnightBlue;
            this.StartAddButton.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartAddButton.ForeColor = System.Drawing.Color.White;
            this.StartAddButton.HoverState.Parent = this.StartAddButton;
            this.StartAddButton.Location = new System.Drawing.Point(3, 562);
            this.StartAddButton.Name = "StartAddButton";
            this.StartAddButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.StartAddButton.ShadowDecoration.Parent = this.StartAddButton;
            this.StartAddButton.Size = new System.Drawing.Size(70, 70);
            this.StartAddButton.TabIndex = 66;
            this.StartAddButton.Text = "+";
            this.StartAddButton.TextOffset = new System.Drawing.Point(0, -2);
            this.StartAddButton.Click += new System.EventHandler(this.StartAddButton_Click);
            // 
            // AddingBodyPanel
            // 
            this.AddingBodyPanel.BackColor = System.Drawing.Color.Transparent;
            this.AddingBodyPanel.Controls.Add(this.AddData);
            this.AddingBodyPanel.Controls.Add(this.guna2ShadowPanel4);
            this.AddingBodyPanel.Controls.Add(this.DateTimePicker);
            this.AddingBodyPanel.FillColor = System.Drawing.Color.MidnightBlue;
            this.AddingBodyPanel.Location = new System.Drawing.Point(505, 606);
            this.AddingBodyPanel.Name = "AddingBodyPanel";
            this.AddingBodyPanel.Radius = 10;
            this.AddingBodyPanel.ShadowColor = System.Drawing.Color.Black;
            this.AddingBodyPanel.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.AddingBodyPanel.Size = new System.Drawing.Size(459, 591);
            this.AddingBodyPanel.TabIndex = 67;
            // 
            // AddData
            // 
            this.AddData.BorderRadius = 10;
            this.AddData.CheckedState.Parent = this.AddData;
            this.AddData.CustomImages.Parent = this.AddData;
            this.AddData.FillColor = System.Drawing.Color.White;
            this.AddData.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold);
            this.AddData.ForeColor = System.Drawing.Color.MidnightBlue;
            this.AddData.HoverState.Parent = this.AddData;
            this.AddData.Location = new System.Drawing.Point(39, 512);
            this.AddData.Name = "AddData";
            this.AddData.ShadowDecoration.Parent = this.AddData;
            this.AddData.Size = new System.Drawing.Size(373, 54);
            this.AddData.TabIndex = 58;
            this.AddData.Text = "ADD Data";
            this.AddData.Click += new System.EventHandler(this.AddData_Click);
            // 
            // guna2ShadowPanel4
            // 
            this.guna2ShadowPanel4.BackColor = System.Drawing.Color.Transparent;
            this.guna2ShadowPanel4.Controls.Add(this.Hips_textbox);
            this.guna2ShadowPanel4.Controls.Add(this.guna2Button6);
            this.guna2ShadowPanel4.Controls.Add(this.Waist_textbox);
            this.guna2ShadowPanel4.Controls.Add(this.Bust_textbox);
            this.guna2ShadowPanel4.Controls.Add(this.Height_textbox);
            this.guna2ShadowPanel4.Controls.Add(this.Shoulder_textbox);
            this.guna2ShadowPanel4.Controls.Add(this.Weight_textbox);
            this.guna2ShadowPanel4.Controls.Add(this.guna2Button5);
            this.guna2ShadowPanel4.Controls.Add(this.guna2Button4);
            this.guna2ShadowPanel4.Controls.Add(this.guna2Button3);
            this.guna2ShadowPanel4.Controls.Add(this.guna2Button2);
            this.guna2ShadowPanel4.Controls.Add(this.guna2Button1);
            this.guna2ShadowPanel4.FillColor = System.Drawing.Color.White;
            this.guna2ShadowPanel4.Location = new System.Drawing.Point(8, 57);
            this.guna2ShadowPanel4.Name = "guna2ShadowPanel4";
            this.guna2ShadowPanel4.Radius = 10;
            this.guna2ShadowPanel4.ShadowColor = System.Drawing.Color.Black;
            this.guna2ShadowPanel4.ShadowDepth = 0;
            this.guna2ShadowPanel4.Size = new System.Drawing.Size(439, 439);
            this.guna2ShadowPanel4.TabIndex = 57;
            // 
            // Hips_textbox
            // 
            this.Hips_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Hips_textbox.Location = new System.Drawing.Point(249, 374);
            this.Hips_textbox.Name = "Hips_textbox";
            this.Hips_textbox.Size = new System.Drawing.Size(123, 35);
            this.Hips_textbox.TabIndex = 75;
            // 
            // guna2Button6
            // 
            this.guna2Button6.BorderRadius = 10;
            this.guna2Button6.CheckedState.Parent = this.guna2Button6;
            this.guna2Button6.CustomImages.Parent = this.guna2Button6;
            this.guna2Button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.guna2Button6.ForeColor = System.Drawing.Color.White;
            this.guna2Button6.HoverState.Parent = this.guna2Button6;
            this.guna2Button6.Location = new System.Drawing.Point(73, 366);
            this.guna2Button6.Name = "guna2Button6";
            this.guna2Button6.ShadowDecoration.Parent = this.guna2Button6;
            this.guna2Button6.Size = new System.Drawing.Size(128, 45);
            this.guna2Button6.TabIndex = 74;
            this.guna2Button6.Text = "臀围";
            // 
            // Waist_textbox
            // 
            this.Waist_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Waist_textbox.Location = new System.Drawing.Point(249, 311);
            this.Waist_textbox.Name = "Waist_textbox";
            this.Waist_textbox.Size = new System.Drawing.Size(123, 35);
            this.Waist_textbox.TabIndex = 73;
            // 
            // Bust_textbox
            // 
            this.Bust_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Bust_textbox.Location = new System.Drawing.Point(249, 242);
            this.Bust_textbox.Name = "Bust_textbox";
            this.Bust_textbox.Size = new System.Drawing.Size(123, 35);
            this.Bust_textbox.TabIndex = 72;
            // 
            // Height_textbox
            // 
            this.Height_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Height_textbox.Location = new System.Drawing.Point(249, 173);
            this.Height_textbox.Name = "Height_textbox";
            this.Height_textbox.Size = new System.Drawing.Size(123, 35);
            this.Height_textbox.TabIndex = 71;
            // 
            // Shoulder_textbox
            // 
            this.Shoulder_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Shoulder_textbox.Location = new System.Drawing.Point(249, 103);
            this.Shoulder_textbox.Name = "Shoulder_textbox";
            this.Shoulder_textbox.Size = new System.Drawing.Size(123, 35);
            this.Shoulder_textbox.TabIndex = 70;
            // 
            // Weight_textbox
            // 
            this.Weight_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Weight_textbox.Location = new System.Drawing.Point(249, 34);
            this.Weight_textbox.Name = "Weight_textbox";
            this.Weight_textbox.Size = new System.Drawing.Size(123, 35);
            this.Weight_textbox.TabIndex = 69;
            // 
            // guna2Button5
            // 
            this.guna2Button5.BorderRadius = 10;
            this.guna2Button5.CheckedState.Parent = this.guna2Button5;
            this.guna2Button5.CustomImages.Parent = this.guna2Button5;
            this.guna2Button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.guna2Button5.ForeColor = System.Drawing.Color.White;
            this.guna2Button5.HoverState.Parent = this.guna2Button5;
            this.guna2Button5.Location = new System.Drawing.Point(73, 303);
            this.guna2Button5.Name = "guna2Button5";
            this.guna2Button5.ShadowDecoration.Parent = this.guna2Button5;
            this.guna2Button5.Size = new System.Drawing.Size(128, 45);
            this.guna2Button5.TabIndex = 68;
            this.guna2Button5.Text = "腰围";
            // 
            // guna2Button4
            // 
            this.guna2Button4.BorderRadius = 10;
            this.guna2Button4.CheckedState.Parent = this.guna2Button4;
            this.guna2Button4.CustomImages.Parent = this.guna2Button4;
            this.guna2Button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.guna2Button4.ForeColor = System.Drawing.Color.White;
            this.guna2Button4.HoverState.Parent = this.guna2Button4;
            this.guna2Button4.Location = new System.Drawing.Point(73, 235);
            this.guna2Button4.Name = "guna2Button4";
            this.guna2Button4.ShadowDecoration.Parent = this.guna2Button4;
            this.guna2Button4.Size = new System.Drawing.Size(128, 45);
            this.guna2Button4.TabIndex = 67;
            this.guna2Button4.Text = "胸围";
            // 
            // guna2Button3
            // 
            this.guna2Button3.BorderRadius = 10;
            this.guna2Button3.CheckedState.Parent = this.guna2Button3;
            this.guna2Button3.CustomImages.Parent = this.guna2Button3;
            this.guna2Button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.guna2Button3.ForeColor = System.Drawing.Color.White;
            this.guna2Button3.HoverState.Parent = this.guna2Button3;
            this.guna2Button3.Location = new System.Drawing.Point(73, 165);
            this.guna2Button3.Name = "guna2Button3";
            this.guna2Button3.ShadowDecoration.Parent = this.guna2Button3;
            this.guna2Button3.Size = new System.Drawing.Size(128, 45);
            this.guna2Button3.TabIndex = 66;
            this.guna2Button3.Text = "身高";
            // 
            // guna2Button2
            // 
            this.guna2Button2.BorderRadius = 10;
            this.guna2Button2.CheckedState.Parent = this.guna2Button2;
            this.guna2Button2.CustomImages.Parent = this.guna2Button2;
            this.guna2Button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.guna2Button2.ForeColor = System.Drawing.Color.White;
            this.guna2Button2.HoverState.Parent = this.guna2Button2;
            this.guna2Button2.Location = new System.Drawing.Point(73, 95);
            this.guna2Button2.Name = "guna2Button2";
            this.guna2Button2.ShadowDecoration.Parent = this.guna2Button2;
            this.guna2Button2.Size = new System.Drawing.Size(128, 45);
            this.guna2Button2.TabIndex = 65;
            this.guna2Button2.Text = "肩宽";
            // 
            // guna2Button1
            // 
            this.guna2Button1.BorderRadius = 10;
            this.guna2Button1.CheckedState.Parent = this.guna2Button1;
            this.guna2Button1.CustomImages.Parent = this.guna2Button1;
            this.guna2Button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.guna2Button1.ForeColor = System.Drawing.Color.White;
            this.guna2Button1.HoverState.Parent = this.guna2Button1;
            this.guna2Button1.Location = new System.Drawing.Point(73, 27);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.ShadowDecoration.Parent = this.guna2Button1;
            this.guna2Button1.Size = new System.Drawing.Size(128, 45);
            this.guna2Button1.TabIndex = 64;
            this.guna2Button1.Text = "体重";
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.BackColor = System.Drawing.Color.Transparent;
            this.DateTimePicker.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DateTimePicker.BorderRadius = 10;
            this.DateTimePicker.BorderThickness = 2;
            this.DateTimePicker.CheckedState.Parent = this.DateTimePicker;
            this.DateTimePicker.FillColor = System.Drawing.Color.White;
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.DateTimePicker.ForeColor = System.Drawing.Color.MidnightBlue;
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.DateTimePicker.HoverState.Parent = this.DateTimePicker;
            this.DateTimePicker.Location = new System.Drawing.Point(8, 9);
            this.DateTimePicker.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.DateTimePicker.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.ShadowDecoration.Parent = this.DateTimePicker;
            this.DateTimePicker.Size = new System.Drawing.Size(188, 42);
            this.DateTimePicker.TabIndex = 55;
            this.DateTimePicker.Value = new System.DateTime(2020, 5, 23, 15, 36, 56, 695);
            // 
            // guna2ShadowPanel1
            // 
            this.guna2ShadowPanel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2ShadowPanel1.Controls.Add(this.Weight);
            this.guna2ShadowPanel1.Controls.Add(this.Height);
            this.guna2ShadowPanel1.Controls.Add(this.cartesianChart2);
            this.guna2ShadowPanel1.Controls.Add(this.cartesianChart1);
            this.guna2ShadowPanel1.FillColor = System.Drawing.Color.White;
            this.guna2ShadowPanel1.Location = new System.Drawing.Point(505, 74);
            this.guna2ShadowPanel1.Name = "guna2ShadowPanel1";
            this.guna2ShadowPanel1.Radius = 10;
            this.guna2ShadowPanel1.ShadowColor = System.Drawing.Color.Black;
            this.guna2ShadowPanel1.Size = new System.Drawing.Size(374, 437);
            this.guna2ShadowPanel1.TabIndex = 68;
            // 
            // Weight
            // 
            this.Weight.AutoSize = true;
            this.Weight.Font = new System.Drawing.Font("方正粗黑宋简体", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Weight.Location = new System.Drawing.Point(12, 233);
            this.Weight.Name = "Weight";
            this.Weight.Size = new System.Drawing.Size(73, 34);
            this.Weight.TabIndex = 3;
            this.Weight.Text = "体重";
            // 
            // Height
            // 
            this.Height.AutoSize = true;
            this.Height.Font = new System.Drawing.Font("方正粗黑宋简体", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Height.Location = new System.Drawing.Point(12, 12);
            this.Height.Name = "Height";
            this.Height.Size = new System.Drawing.Size(73, 34);
            this.Height.TabIndex = 2;
            this.Height.Text = "身高";
            // 
            // cartesianChart2
            // 
            this.cartesianChart2.Location = new System.Drawing.Point(94, 264);
            this.cartesianChart2.Name = "cartesianChart2";
            this.cartesianChart2.Size = new System.Drawing.Size(261, 149);
            this.cartesianChart2.TabIndex = 1;
            this.cartesianChart2.Text = "cartesianChart2";
            // 
            // cartesianChart1
            // 
            this.cartesianChart1.Location = new System.Drawing.Point(74, 51);
            this.cartesianChart1.Name = "cartesianChart1";
            this.cartesianChart1.Size = new System.Drawing.Size(281, 168);
            this.cartesianChart1.TabIndex = 0;
            this.cartesianChart1.Text = "cartesianChart1";
            // 
            // CloseButton
            // 
            this.CloseButton.CheckedState.Parent = this.CloseButton;
            this.CloseButton.CustomImages.Parent = this.CloseButton;
            this.CloseButton.FillColor = System.Drawing.Color.White;
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.CloseButton.ForeColor = System.Drawing.Color.MidnightBlue;
            this.CloseButton.HoverState.Parent = this.CloseButton;
            this.CloseButton.Location = new System.Drawing.Point(972, 9);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.CloseButton.ShadowDecoration.Parent = this.CloseButton;
            this.CloseButton.Size = new System.Drawing.Size(32, 32);
            this.CloseButton.TabIndex = 69;
            this.CloseButton.Text = "×";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // UCinbox_bodies
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.AddingBodyPanel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.guna2ShadowPanel1);
            this.Controls.Add(this.StartAddButton);
            this.Controls.Add(this.ShowBodies);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BodyTable);
            this.Name = "UCinbox_bodies";
            this.Size = new System.Drawing.Size(1015, 672);
            this.Load += new System.EventHandler(this.UCinbox_bodies_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DaysAmount)).EndInit();
            this.AddingBodyPanel.ResumeLayout(false);
            this.guna2ShadowPanel4.ResumeLayout(false);
            this.guna2ShadowPanel4.PerformLayout();
            this.guna2ShadowPanel1.ResumeLayout(false);
            this.guna2ShadowPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2ShadowPanel BodyTable;
        private Guna.UI2.WinForms.Guna2Button ShowBodies;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2NumericUpDown DaysAmount;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private System.Windows.Forms.Timer timer1;
        public Guna.UI2.WinForms.Guna2CircleButton StartAddButton;
        public Guna.UI2.WinForms.Guna2ShadowPanel AddingBodyPanel;
        private Guna.UI2.WinForms.Guna2ShadowPanel guna2ShadowPanel4;
        public Guna.UI2.WinForms.Guna2DateTimePicker DateTimePicker;
        private System.Windows.Forms.TextBox Hips_textbox;
        private Guna.UI2.WinForms.Guna2Button guna2Button6;
        private System.Windows.Forms.TextBox Waist_textbox;
        private System.Windows.Forms.TextBox Bust_textbox;
        private System.Windows.Forms.TextBox Height_textbox;
        private System.Windows.Forms.TextBox Shoulder_textbox;
        private System.Windows.Forms.TextBox Weight_textbox;
        private Guna.UI2.WinForms.Guna2Button guna2Button5;
        private Guna.UI2.WinForms.Guna2Button guna2Button4;
        private Guna.UI2.WinForms.Guna2Button guna2Button3;
        private Guna.UI2.WinForms.Guna2Button guna2Button2;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        public Guna.UI2.WinForms.Guna2TileButton AddData;
        private Guna.UI2.WinForms.Guna2ShadowPanel guna2ShadowPanel1;
        private System.Windows.Forms.Label Weight;
        private System.Windows.Forms.Label Height;
        private LiveCharts.WinForms.CartesianChart cartesianChart2;
        private LiveCharts.WinForms.CartesianChart cartesianChart1;
        private Guna.UI2.WinForms.Guna2CircleButton CloseButton;
    }
}
