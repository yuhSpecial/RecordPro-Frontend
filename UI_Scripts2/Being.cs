﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_Scripts2
{
    public class Being
    {
        public Double emotion{ set; get; }
        public Double health { set; get; }
        public int beauty { set; get; }
        public Double yanquan { set; get; }
        public DateTime datetime { set; get; }
        public Being(UserFaces face)
        {
            this.emotion = Convert.ToDouble(face.emotion.Split(':', '/')[1]);
            this.health= Convert.ToDouble(face.skinstatus.Split('/')[0]);
            this.beauty = Convert.ToInt32(face.beauty.Split('/')[0]);
            this.yanquan = Convert.ToDouble(face.skinstatus.Split('/')[3]);
            this.datetime = face.datetime;
        }
    }
}
