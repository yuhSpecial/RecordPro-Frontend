﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace UI_Scripts2
{
    public partial class UCinbox_pictures : UserControl
    {
        
        private Api api;
        private DirectoryInfo dir;
        private string FolderPath { get; set; }
        private string ImagePath { get; set; }
        private int Index { set; get; }
        bool panelMax { set; get; }
        public byte[] GetPictureData(string imagepath)
        {
            //根据图片文件的路径使用文件流打开，并保存为byte[] 
            FileStream fs = new FileStream(imagepath, FileMode.Open);//可以是其他重载方法 
            byte[] byData = new byte[fs.Length];
            fs.Read(byData, 0, byData.Length);
            fs.Close();
            return byData;
        }
        private void hover()
        {
            ToolTip toolTip1 = new ToolTip();
            this.guna2DateTimePicker1.Value = DateTime.Today;
            // 设置显示样式
            //toolTip1.AutoPopDelay = 5000;//提示信息的可见时间
            toolTip1.InitialDelay = 500;//事件触发多久后出现提示
            //toolTip1.ReshowDelay = 500;//指针从一个控件移向另一个控件时，经过多久才会显示下一个提示框
            toolTip1.ShowAlways = true;//是否显示提示框
            //  设置伴随的对象.
            toolTip1.SetToolTip(guna2GetButton, "获取七天内皮肤状态分析");//设置提示按钮和提示内容
            toolTip1.SetToolTip(guna2PostButton, "上传人脸照片\n不超过2MB");//设置提示按钮和提示内容
            toolTip1.SetToolTip(guna2ClearButton, "清除Panel");//设置提示按钮和提示内容
        }
        private void guna2CircleButton2_Click(object sender, EventArgs e) //弹出一个选择目录的对话框
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.ShowDialog();
            this.FolderPath = path.SelectedPath;
            try
            {
                dir = new DirectoryInfo(FolderPath);
                GetImages();
            }
            catch
                (Exception ex)
                {
                Console.WriteLine(ex.ToString());
            }
        }
        private void guna2CircleButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            DialogResult result = fileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                ImagePath = fileDialog.FileName;
                ListViewItem lt = new ListViewItem();
                lt.Text = (Index+1).ToString();
                lt.SubItems.Add(ImagePath.ToString());     
                //将lt数据添加到listView1控件中
                listView1.Items.Add(lt);
                //this.pbStu.Image = Image.FromFile(ImagePath);
                Stream s = File.Open(ImagePath, FileMode.Open);
                Image img = Image.FromStream(s);
                s.Close();
                var temp = new Panel();
                temp.Width = 520;
                temp.Height = 255;
                temp.Dock = System.Windows.Forms.DockStyle.Left;

                temp.Controls.Add(new PictureBox
                {
                    BackColor = System.Drawing.Color.Transparent,
                    BackgroundImageLayout = ImageLayout.Stretch,
                    Location=new Point(0,15),
                    BackgroundImage = img,
                    Width = 528,
                    Height = 440,
                    //Dock = System.Windows.Forms.DockStyle.Bottom

                });
                temp.Controls.Add(new Label
                {
                    Text = (Index + 1).ToString(),
                    Width = 528,
                    Height=15,
                    Location = new Point(0, 0),
                    TextAlign = ContentAlignment.BottomCenter,
                    //Dock = System.Windows.Forms.DockStyle.Top

                });
                this.guna2ShadowPanel1.Controls.Add(temp);
                Index = Index + 1;
            }
        }
        
        //相对路径，和程序exe同目录下
        public void GetImages()
        {
            FileInfo[] fileInfo = dir.GetFiles();
            List<string> fileNames = new List<string>();
            foreach (FileInfo item in fileInfo)
            {
                if(item.Extension==".jpg"|| item.Extension == ".png"|| item.Extension == ".jpeg")
                fileNames.Add(item.Name);
            }

            //图片展示
            for (int i = 0; i < fileNames.Count; i++)
            {
                string fileName = fileNames[i];
                
                ListViewItem lt = new ListViewItem();
                lt.Text = (Index+1).ToString();
                lt.SubItems.Add(FolderPath + "\\" + fileName);
                //将lt数据添加到listView1控件中
                listView1.Items.Add(lt);
                Stream s = File.Open(FolderPath + "\\" + fileName, FileMode.Open);
                Image img = Image.FromStream(s);
                s.Close();
                var temp = new Panel();
                temp.Width = 520;
                temp.Height = 255;
                temp.Dock = System.Windows.Forms.DockStyle.Left;

                temp.Controls.Add(new PictureBox
                {
                    BackColor = System.Drawing.Color.Transparent,
                    BackgroundImageLayout = ImageLayout.Stretch,
                    Location = new Point(0, 15),
                    BackgroundImage = img,
                    Width = 528,
                    Height = 440,
                    //Dock = System.Windows.Forms.DockStyle.Bottom

                });
                temp.Controls.Add(new Label
                {
                    Text = (Index+1).ToString(),
                    Width = 528,
                    Height = 15,
                    Location = new Point(0, 0),
                    TextAlign = ContentAlignment.BottomCenter,
                    //Dock = System.Windows.Forms.DockStyle.Top

                });
                this.guna2ShadowPanel1.Controls.Add(temp);
                Index = Index + 1;
            }
        }
       

        public UCinbox_pictures()
        {
            InitializeComponent();
            guna2ShadowPanel1.AutoScroll = true;
            this.api = new Api();
            this.Index = 0;
            this.panelMax = false;
            
            hover();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void guna2PostButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            DialogResult result = fileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                ImagePath = fileDialog.FileName;
                ListViewItem lt = new ListViewItem();
                lt.Text = (Index+1).ToString();
                lt.SubItems.Add(ImagePath.ToString());
                //将lt数据添加到listView1控件中
                listView1.Items.Add(lt);
                //this.pbStu.Image = Image.FromFile(ImagePath);
                string fileName = ImagePath.Substring(ImagePath.LastIndexOf("\\") + 1);
                byte[] img=GetPictureData(ImagePath);


                Stream s = File.Open(ImagePath, FileMode.Open);
                Image Img = Image.FromStream(s);
                s.Close();

                if(api.PostUserFacesAsync(Api.Token, img, fileName)=="Created")
                {
                    List<UserFaces> faces;
                    faces = api.GetUserFacesAsync(Api.Token, 1);
                    //this.Controls.Add(new FacesDetail(faces.Last<UserFaces>(),img));
                    var temp = new Panel();
                    temp.Width = 520;
                    temp.Height = 255;
                    temp.Dock = System.Windows.Forms.DockStyle.Left;

                    temp.Controls.Add(new PictureBox
                    {
                        BackColor = System.Drawing.Color.Transparent,
                        BackgroundImageLayout = ImageLayout.Stretch,
                        Location = new Point(0, 15),
                        BackgroundImage = Img,
                        Width = 528,
                        Height = 440,
                        //Dock = System.Windows.Forms.DockStyle.Bottom

                    });
                    temp.Controls.Add(new Label
                    {
                        Text = (Index + 1).ToString(),
                        Width = 528,
                        Height = 15,
                        Location = new Point(0, 0),
                        TextAlign = ContentAlignment.BottomCenter,
                        //Dock = System.Windows.Forms.DockStyle.Top

                    });
                    this.guna2ShadowPanel1.Controls.Add(temp);
                    Index = Index + 1;
                    Form form = new Face(faces.Last<UserFaces>(), Img);
                    form.Show();
                }
                else
                {
                    MessageBox.Show("提交失败");
                }
                
                
            }
        }

        private void guna2ClearButton_Click(object sender, EventArgs e)
        {
            this.guna2ShadowPanel1.Controls.Clear();
            this.Index = 0;
        }

        private void guna2GetButton_Click(object sender, EventArgs e)
        {
            List<UserFaces> faces;
            try
            {
                faces = api.GetUserFacesAsync(Api.Token, 7);
                Console.WriteLine(faces[0].id);
                Form Form2 = new FaceDaily(faces);
                //this.Parent.con
                Form2.Show();
            }
            catch(Exception ex)
            {
                MessageBox.Show("近七日没有上传人脸照片");
            }
            
            
        }

        private void guna2TileButton3_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            DialogResult result = fileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                ImagePath = fileDialog.FileName;
                ListViewItem lt = new ListViewItem();
                lt.Text = (Index+1).ToString();
                lt.SubItems.Add(ImagePath.ToString());
                //将lt数据添加到listView1控件中
                listView1.Items.Add(lt);
                //this.pbStu.Image = Image.FromFile(ImagePath);
                Stream s = File.Open(ImagePath, FileMode.Open);
                Image img = Image.FromStream(s);
                s.Close();
                var temp = new Panel();
                temp.Width = 520;
                temp.Height = 255;
                temp.Dock = System.Windows.Forms.DockStyle.Left;

                temp.Controls.Add(new PictureBox
                {
                    BackColor = System.Drawing.Color.Transparent,
                    BackgroundImageLayout = ImageLayout.Stretch,
                    Location = new Point(0, 15),
                    BackgroundImage = img,
                    Width = 528,
                    Height = 440,
                    //Dock = System.Windows.Forms.DockStyle.Bottom

                });
                temp.Controls.Add(new Label
                {
                    Text = (Index + 1).ToString(),
                    Width = 528,
                    Height = 15,
                    Location = new Point(0, 0),
                    TextAlign = ContentAlignment.BottomCenter,
                    //Dock = System.Windows.Forms.DockStyle.Top

                });
                this.guna2ShadowPanel1.Controls.Add(temp);
                Index = Index + 1;
            }
        }

        private void guna2ShadowPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2ShadowPanel1_Click(object sender, EventArgs e)
        {
            //Rectangle rect = new Rectangle();
            
            ////rect = Screen.GetWorkingArea(this) ;//获取工作分辨率会出现一个问题，若任务栏放在最左边，则会在最右边留下任务栏的宽度
            //rect = Screen.GetBounds(this);
            //if(!panelMax)
            //{
            //    var MaximumSize = new Size(rect.Width, rect.Height);
            //    //设置相应控件的位置
            //    guna2ShadowPanel1.Location = new System.Drawing.Point(0, 0);


            //    //设置主窗体和相应控件的宽高
            //    guna2ShadowPanel1.Width = rect.Width;
            //    guna2ShadowPanel1.Height = rect.Height;

            //}
            //if (panelMax)
            //{
            //    //528 455
            //    //342 91
            //    guna2ShadowPanel1.Location = new System.Drawing.Point(342, 91);
            //    guna2ShadowPanel1.Width = 528;
            //    guna2ShadowPanel1.Height = 455;

            //}

        }

        private void guna2TileButton1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.ShowDialog();
            this.FolderPath = path.SelectedPath;
            try
            {
                dir = new DirectoryInfo(FolderPath);
                GetImages();
            }
            catch
                (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void guna2GetButton_MouseHover(object sender, EventArgs e)
        {
            // 创建the ToolTip 
            
        }

        private void guna2PostButton_MouseHover(object sender, EventArgs e)
        {
            
        }

        private void guna2ClearButton_MouseHover(object sender, EventArgs e)
        {
           
        }

        //private void listView1_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        //{
        //    e.Cancel = true;
        //    e.NewWidth = listView1.Columns[e.ColumnIndex].Width;
        //}

        private void listView1_SizeChanged(object sender, EventArgs e)
        {
            
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            MainPage mainPage = (MainPage)CloseButton.Parent.Parent.Parent;
            mainPage.timer1.Start();
        }
    }
}
