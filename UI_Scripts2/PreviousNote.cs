﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Scripts2
{
    public partial class PreviousNote : UserControl
    {
        public PreviousNote()
        {
            InitializeComponent();
            guna2TextBox1.Enabled = false;
        }

        private void guna2CircleButton1_Click(object sender, EventArgs e) //delete
        {
            guna2TextBox1.Clear();
        }

        private void guna2CircleButton2_Click(object sender, EventArgs e) //edit
        {
            guna2TextBox1.Enabled = true;
        }
    }
}
