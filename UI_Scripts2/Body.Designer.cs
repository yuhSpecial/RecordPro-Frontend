﻿namespace UI_Scripts2
{
    partial class Body
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Body));
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.Shoulder = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.EditButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.DeleteButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.guna2Button2 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button3 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button4 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button5 = new Guna.UI2.WinForms.Guna2Button();
            this.Height = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.Bust = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.Waist = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.Weight_textbox = new System.Windows.Forms.TextBox();
            this.Shoulder_textbox = new System.Windows.Forms.TextBox();
            this.Height_textbox = new System.Windows.Forms.TextBox();
            this.Bust_textbox = new System.Windows.Forms.TextBox();
            this.Waist_textbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Confirm = new Guna.UI2.WinForms.Guna2CircleButton();
            this.guna2Button6 = new Guna.UI2.WinForms.Guna2Button();
            this.Hips_textbox = new System.Windows.Forms.TextBox();
            this.Hips = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Weight = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.SuspendLayout();
            // 
            // guna2Button1
            // 
            this.guna2Button1.BorderRadius = 10;
            this.guna2Button1.CheckedState.Parent = this.guna2Button1;
            this.guna2Button1.CustomImages.Parent = this.guna2Button1;
            this.guna2Button1.Font = new System.Drawing.Font("方正粗黑宋简体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.guna2Button1.ForeColor = System.Drawing.Color.White;
            this.guna2Button1.HoverState.Parent = this.guna2Button1;
            this.guna2Button1.Location = new System.Drawing.Point(28, 58);
            this.guna2Button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.ShadowDecoration.Parent = this.guna2Button1;
            this.guna2Button1.Size = new System.Drawing.Size(144, 54);
            this.guna2Button1.TabIndex = 0;
            this.guna2Button1.Text = "体重";
            // 
            // Shoulder
            // 
            this.Shoulder.BackColor = System.Drawing.Color.Transparent;
            this.Shoulder.Font = new System.Drawing.Font("方正粗黑宋简体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Shoulder.Location = new System.Drawing.Point(240, 156);
            this.Shoulder.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Shoulder.Name = "Shoulder";
            this.Shoulder.Size = new System.Drawing.Size(243, 36);
            this.Shoulder.TabIndex = 2;
            this.Shoulder.Text = "guna2HtmlLabel2";
            this.Shoulder.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EditButton
            // 
            this.EditButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.EditButton.BorderThickness = 2;
            this.EditButton.CheckedState.Parent = this.EditButton;
            this.EditButton.CustomImages.Parent = this.EditButton;
            this.EditButton.FillColor = System.Drawing.Color.White;
            this.EditButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.EditButton.ForeColor = System.Drawing.Color.White;
            this.EditButton.HoverState.Parent = this.EditButton;
            this.EditButton.Image = ((System.Drawing.Image)(resources.GetObject("EditButton.Image")));
            this.EditButton.Location = new System.Drawing.Point(294, 535);
            this.EditButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.EditButton.Name = "EditButton";
            this.EditButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.EditButton.ShadowDecoration.Parent = this.EditButton;
            this.EditButton.Size = new System.Drawing.Size(45, 48);
            this.EditButton.TabIndex = 46;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DeleteButton.BorderThickness = 2;
            this.DeleteButton.CheckedState.Parent = this.DeleteButton;
            this.DeleteButton.CustomImages.Parent = this.DeleteButton;
            this.DeleteButton.FillColor = System.Drawing.Color.White;
            this.DeleteButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.DeleteButton.ForeColor = System.Drawing.Color.White;
            this.DeleteButton.HoverState.Parent = this.DeleteButton;
            this.DeleteButton.Image = ((System.Drawing.Image)(resources.GetObject("DeleteButton.Image")));
            this.DeleteButton.Location = new System.Drawing.Point(345, 535);
            this.DeleteButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.DeleteButton.ShadowDecoration.Parent = this.DeleteButton;
            this.DeleteButton.Size = new System.Drawing.Size(45, 48);
            this.DeleteButton.TabIndex = 45;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // guna2Button2
            // 
            this.guna2Button2.BorderRadius = 10;
            this.guna2Button2.CheckedState.Parent = this.guna2Button2;
            this.guna2Button2.CustomImages.Parent = this.guna2Button2;
            this.guna2Button2.Font = new System.Drawing.Font("方正粗黑宋简体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.guna2Button2.ForeColor = System.Drawing.Color.White;
            this.guna2Button2.HoverState.Parent = this.guna2Button2;
            this.guna2Button2.Location = new System.Drawing.Point(28, 139);
            this.guna2Button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.guna2Button2.Name = "guna2Button2";
            this.guna2Button2.ShadowDecoration.Parent = this.guna2Button2;
            this.guna2Button2.Size = new System.Drawing.Size(144, 54);
            this.guna2Button2.TabIndex = 47;
            this.guna2Button2.Text = "肩宽";
            // 
            // guna2Button3
            // 
            this.guna2Button3.BorderRadius = 10;
            this.guna2Button3.CheckedState.Parent = this.guna2Button3;
            this.guna2Button3.CustomImages.Parent = this.guna2Button3;
            this.guna2Button3.Font = new System.Drawing.Font("方正粗黑宋简体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.guna2Button3.ForeColor = System.Drawing.Color.White;
            this.guna2Button3.HoverState.Parent = this.guna2Button3;
            this.guna2Button3.Location = new System.Drawing.Point(28, 223);
            this.guna2Button3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.guna2Button3.Name = "guna2Button3";
            this.guna2Button3.ShadowDecoration.Parent = this.guna2Button3;
            this.guna2Button3.Size = new System.Drawing.Size(144, 54);
            this.guna2Button3.TabIndex = 48;
            this.guna2Button3.Text = "身高";
            // 
            // guna2Button4
            // 
            this.guna2Button4.BorderRadius = 10;
            this.guna2Button4.CheckedState.Parent = this.guna2Button4;
            this.guna2Button4.CustomImages.Parent = this.guna2Button4;
            this.guna2Button4.Font = new System.Drawing.Font("方正粗黑宋简体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.guna2Button4.ForeColor = System.Drawing.Color.White;
            this.guna2Button4.HoverState.Parent = this.guna2Button4;
            this.guna2Button4.Location = new System.Drawing.Point(28, 307);
            this.guna2Button4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.guna2Button4.Name = "guna2Button4";
            this.guna2Button4.ShadowDecoration.Parent = this.guna2Button4;
            this.guna2Button4.Size = new System.Drawing.Size(144, 54);
            this.guna2Button4.TabIndex = 49;
            this.guna2Button4.Text = "胸围";
            // 
            // guna2Button5
            // 
            this.guna2Button5.BorderRadius = 10;
            this.guna2Button5.CheckedState.Parent = this.guna2Button5;
            this.guna2Button5.CustomImages.Parent = this.guna2Button5;
            this.guna2Button5.Font = new System.Drawing.Font("方正粗黑宋简体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.guna2Button5.ForeColor = System.Drawing.Color.White;
            this.guna2Button5.HoverState.Parent = this.guna2Button5;
            this.guna2Button5.Location = new System.Drawing.Point(28, 389);
            this.guna2Button5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.guna2Button5.Name = "guna2Button5";
            this.guna2Button5.ShadowDecoration.Parent = this.guna2Button5;
            this.guna2Button5.Size = new System.Drawing.Size(144, 54);
            this.guna2Button5.TabIndex = 50;
            this.guna2Button5.Text = "腰围";
            // 
            // Height
            // 
            this.Height.BackColor = System.Drawing.Color.Transparent;
            this.Height.Font = new System.Drawing.Font("方正粗黑宋简体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Height.Location = new System.Drawing.Point(240, 240);
            this.Height.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Height.Name = "Height";
            this.Height.Size = new System.Drawing.Size(243, 36);
            this.Height.TabIndex = 51;
            this.Height.Text = "guna2HtmlLabel2";
            this.Height.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Bust
            // 
            this.Bust.BackColor = System.Drawing.Color.Transparent;
            this.Bust.Font = new System.Drawing.Font("方正粗黑宋简体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Bust.Location = new System.Drawing.Point(240, 329);
            this.Bust.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Bust.Name = "Bust";
            this.Bust.Size = new System.Drawing.Size(243, 36);
            this.Bust.TabIndex = 52;
            this.Bust.Text = "guna2HtmlLabel2";
            this.Bust.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Waist
            // 
            this.Waist.BackColor = System.Drawing.Color.Transparent;
            this.Waist.Font = new System.Drawing.Font("方正粗黑宋简体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Waist.Location = new System.Drawing.Point(240, 410);
            this.Waist.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Waist.Name = "Waist";
            this.Waist.Size = new System.Drawing.Size(243, 36);
            this.Waist.TabIndex = 53;
            this.Waist.Text = "guna2HtmlLabel2";
            this.Waist.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Weight_textbox
            // 
            this.Weight_textbox.Font = new System.Drawing.Font("Lucida Console", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Weight_textbox.Location = new System.Drawing.Point(261, 71);
            this.Weight_textbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Weight_textbox.Name = "Weight_textbox";
            this.Weight_textbox.Size = new System.Drawing.Size(138, 35);
            this.Weight_textbox.TabIndex = 54;
            this.Weight_textbox.TextChanged += new System.EventHandler(this.Weight_textbox_TextChanged);
            // 
            // Shoulder_textbox
            // 
            this.Shoulder_textbox.Font = new System.Drawing.Font("Lucida Console", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shoulder_textbox.Location = new System.Drawing.Point(261, 232);
            this.Shoulder_textbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Shoulder_textbox.Name = "Shoulder_textbox";
            this.Shoulder_textbox.Size = new System.Drawing.Size(138, 35);
            this.Shoulder_textbox.TabIndex = 55;
            // 
            // Height_textbox
            // 
            this.Height_textbox.Font = new System.Drawing.Font("Lucida Console", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Height_textbox.Location = new System.Drawing.Point(261, 150);
            this.Height_textbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Height_textbox.Name = "Height_textbox";
            this.Height_textbox.Size = new System.Drawing.Size(138, 35);
            this.Height_textbox.TabIndex = 56;
            // 
            // Bust_textbox
            // 
            this.Bust_textbox.Font = new System.Drawing.Font("Lucida Console", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Bust_textbox.Location = new System.Drawing.Point(261, 318);
            this.Bust_textbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Bust_textbox.Name = "Bust_textbox";
            this.Bust_textbox.Size = new System.Drawing.Size(138, 35);
            this.Bust_textbox.TabIndex = 57;
            // 
            // Waist_textbox
            // 
            this.Waist_textbox.Font = new System.Drawing.Font("Lucida Console", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Waist_textbox.Location = new System.Drawing.Point(261, 401);
            this.Waist_textbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Waist_textbox.Name = "Waist_textbox";
            this.Waist_textbox.Size = new System.Drawing.Size(138, 35);
            this.Waist_textbox.TabIndex = 58;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(35, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 22);
            this.label1.TabIndex = 59;
            this.label1.Text = "June, 19th, 2020";
            // 
            // Confirm
            // 
            this.Confirm.BorderColor = System.Drawing.Color.MidnightBlue;
            this.Confirm.BorderThickness = 2;
            this.Confirm.CheckedState.Parent = this.Confirm;
            this.Confirm.CustomImages.Parent = this.Confirm;
            this.Confirm.FillColor = System.Drawing.Color.White;
            this.Confirm.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Confirm.ForeColor = System.Drawing.Color.White;
            this.Confirm.HoverState.Parent = this.Confirm;
            this.Confirm.Image = ((System.Drawing.Image)(resources.GetObject("Confirm.Image")));
            this.Confirm.ImageSize = new System.Drawing.Size(45, 45);
            this.Confirm.Location = new System.Drawing.Point(184, 514);
            this.Confirm.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Confirm.Name = "Confirm";
            this.Confirm.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.Confirm.ShadowDecoration.Parent = this.Confirm;
            this.Confirm.Size = new System.Drawing.Size(65, 65);
            this.Confirm.TabIndex = 60;
            this.Confirm.Click += new System.EventHandler(this.Confirm_Click);
            // 
            // guna2Button6
            // 
            this.guna2Button6.BorderRadius = 10;
            this.guna2Button6.CheckedState.Parent = this.guna2Button6;
            this.guna2Button6.CustomImages.Parent = this.guna2Button6;
            this.guna2Button6.Font = new System.Drawing.Font("方正粗黑宋简体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.guna2Button6.ForeColor = System.Drawing.Color.White;
            this.guna2Button6.HoverState.Parent = this.guna2Button6;
            this.guna2Button6.Location = new System.Drawing.Point(28, 464);
            this.guna2Button6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.guna2Button6.Name = "guna2Button6";
            this.guna2Button6.ShadowDecoration.Parent = this.guna2Button6;
            this.guna2Button6.Size = new System.Drawing.Size(144, 54);
            this.guna2Button6.TabIndex = 61;
            this.guna2Button6.Text = "臀围";
            // 
            // Hips_textbox
            // 
            this.Hips_textbox.Font = new System.Drawing.Font("Lucida Console", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Hips_textbox.Location = new System.Drawing.Point(261, 475);
            this.Hips_textbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Hips_textbox.Name = "Hips_textbox";
            this.Hips_textbox.Size = new System.Drawing.Size(138, 35);
            this.Hips_textbox.TabIndex = 63;
            // 
            // Hips
            // 
            this.Hips.BackColor = System.Drawing.Color.Transparent;
            this.Hips.Font = new System.Drawing.Font("方正粗黑宋简体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Hips.Location = new System.Drawing.Point(240, 481);
            this.Hips.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Hips.Name = "Hips";
            this.Hips.Size = new System.Drawing.Size(243, 36);
            this.Hips.TabIndex = 62;
            this.Hips.Text = "guna2HtmlLabel2";
            this.Hips.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightBlue;
            this.panel1.Location = new System.Drawing.Point(0, 586);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(494, 43);
            this.panel1.TabIndex = 64;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(428, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 29);
            this.label2.TabIndex = 65;
            this.label2.Text = "kg";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(428, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 29);
            this.label3.TabIndex = 66;
            this.label3.Text = "cm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(428, 240);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 29);
            this.label4.TabIndex = 67;
            this.label4.Text = "cm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(428, 326);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 29);
            this.label5.TabIndex = 68;
            this.label5.Text = "cm";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(428, 407);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 29);
            this.label6.TabIndex = 69;
            this.label6.Text = "cm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(428, 478);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 29);
            this.label7.TabIndex = 70;
            this.label7.Text = "cm";
            // 
            // Weight
            // 
            this.Weight.BackColor = System.Drawing.Color.Transparent;
            this.Weight.Font = new System.Drawing.Font("方正粗黑宋简体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Weight.Location = new System.Drawing.Point(240, 73);
            this.Weight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Weight.Name = "Weight";
            this.Weight.Size = new System.Drawing.Size(243, 36);
            this.Weight.TabIndex = 71;
            this.Weight.Text = "guna2HtmlLabel2";
            this.Weight.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Body
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Hips_textbox);
            this.Controls.Add(this.Hips);
            this.Controls.Add(this.guna2Button6);
            this.Controls.Add(this.Confirm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Waist_textbox);
            this.Controls.Add(this.Bust_textbox);
            this.Controls.Add(this.Height_textbox);
            this.Controls.Add(this.Shoulder_textbox);
            this.Controls.Add(this.Weight_textbox);
            this.Controls.Add(this.Waist);
            this.Controls.Add(this.Bust);
            this.Controls.Add(this.Height);
            this.Controls.Add(this.guna2Button5);
            this.Controls.Add(this.guna2Button4);
            this.Controls.Add(this.guna2Button3);
            this.Controls.Add(this.guna2Button2);
            this.Controls.Add(this.EditButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.Shoulder);
            this.Controls.Add(this.guna2Button1);
            this.Controls.Add(this.Weight);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Body";
            this.Size = new System.Drawing.Size(494, 628);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private Guna.UI2.WinForms.Guna2HtmlLabel Shoulder;
        public Guna.UI2.WinForms.Guna2CircleButton EditButton;
        public Guna.UI2.WinForms.Guna2CircleButton DeleteButton;
        private Guna.UI2.WinForms.Guna2Button guna2Button2;
        private Guna.UI2.WinForms.Guna2Button guna2Button3;
        private Guna.UI2.WinForms.Guna2Button guna2Button4;
        private Guna.UI2.WinForms.Guna2Button guna2Button5;
        private Guna.UI2.WinForms.Guna2HtmlLabel Height;
        private Guna.UI2.WinForms.Guna2HtmlLabel Bust;
        private Guna.UI2.WinForms.Guna2HtmlLabel Waist;
        private System.Windows.Forms.TextBox Weight_textbox;
        private System.Windows.Forms.TextBox Shoulder_textbox;
        private System.Windows.Forms.TextBox Height_textbox;
        private System.Windows.Forms.TextBox Bust_textbox;
        private System.Windows.Forms.TextBox Waist_textbox;
        private System.Windows.Forms.Label label1;
        public Guna.UI2.WinForms.Guna2CircleButton Confirm;
        private Guna.UI2.WinForms.Guna2Button guna2Button6;
        private System.Windows.Forms.TextBox Hips_textbox;
        private Guna.UI2.WinForms.Guna2HtmlLabel Hips;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2HtmlLabel Weight;
    }
}
