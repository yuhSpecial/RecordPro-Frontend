﻿namespace UI_Scripts2
{
    partial class UCinbox_statics
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.FacePanel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.guna2Separator6 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2VSeparator1 = new Guna.UI2.WinForms.Guna2VSeparator();
            this.guna2Separator5 = new Guna.UI2.WinForms.Guna2Separator();
            this.smileRatioProgressBar9 = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.label40 = new System.Windows.Forms.Label();
            this.recordFre = new System.Windows.Forms.Label();
            this.beautyRatingStar1 = new Guna.UI2.WinForms.Guna2RatingStar();
            this.pieChart2 = new LiveCharts.WinForms.PieChart();
            this.label38 = new System.Windows.Forms.Label();
            this.pieChart1 = new LiveCharts.WinForms.PieChart();
            this.label37 = new System.Windows.Forms.Label();
            this.Third_emotion = new System.Windows.Forms.Label();
            this.Second_emotion = new System.Windows.Forms.Label();
            this.First_emotion = new System.Windows.Forms.Label();
            this.Third_emotionProgressBar4 = new Guna.UI2.WinForms.Guna2VProgressBar();
            this.Second_emotionProgressBar5 = new Guna.UI2.WinForms.Guna2VProgressBar();
            this.First_emotionProgressBar6 = new Guna.UI2.WinForms.Guna2VProgressBar();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.BillPanel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.guna2Separator4 = new Guna.UI2.WinForms.Guna2Separator();
            this.label28 = new System.Windows.Forms.Label();
            this.investmentRatioProgressBar8 = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.label27 = new System.Windows.Forms.Label();
            this.salaryRatioProgressBar7 = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.mean_outcome = new System.Windows.Forms.Label();
            this.mean_income = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.outcomeRatioProgressBar4 = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.label23 = new System.Windows.Forms.Label();
            this.medium_outcome = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.medium_income = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.incomeRatioProgressBar6 = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.label16 = new System.Windows.Forms.Label();
            this.CloseButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.NotePanel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.mean_mode = new System.Windows.Forms.Label();
            this.guna2Separator3 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Separator2 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Separator1 = new Guna.UI2.WinForms.Guna2Separator();
            this.wordAccountProgressBar5 = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.label15 = new System.Windows.Forms.Label();
            this.Third = new System.Windows.Forms.Label();
            this.Second = new System.Windows.Forms.Label();
            this.First = new System.Windows.Forms.Label();
            this.ThirdProgressBar3 = new Guna.UI2.WinForms.Guna2VProgressBar();
            this.SecondProgressBar2 = new Guna.UI2.WinForms.Guna2VProgressBar();
            this.FirstProgressBar1 = new Guna.UI2.WinForms.Guna2VProgressBar();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.most_mode = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.negativeModeBar = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.neutralModeBar = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.positiveModeBar = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.FacePanel.SuspendLayout();
            this.BillPanel.SuspendLayout();
            this.NotePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 30;
            this.guna2Elipse1.TargetControl = this;
            // 
            // FacePanel
            // 
            this.FacePanel.BackColor = System.Drawing.Color.Transparent;
            this.FacePanel.Controls.Add(this.guna2Separator6);
            this.FacePanel.Controls.Add(this.guna2VSeparator1);
            this.FacePanel.Controls.Add(this.guna2Separator5);
            this.FacePanel.Controls.Add(this.smileRatioProgressBar9);
            this.FacePanel.Controls.Add(this.label40);
            this.FacePanel.Controls.Add(this.recordFre);
            this.FacePanel.Controls.Add(this.beautyRatingStar1);
            this.FacePanel.Controls.Add(this.pieChart2);
            this.FacePanel.Controls.Add(this.label38);
            this.FacePanel.Controls.Add(this.pieChart1);
            this.FacePanel.Controls.Add(this.label37);
            this.FacePanel.Controls.Add(this.Third_emotion);
            this.FacePanel.Controls.Add(this.Second_emotion);
            this.FacePanel.Controls.Add(this.First_emotion);
            this.FacePanel.Controls.Add(this.Third_emotionProgressBar4);
            this.FacePanel.Controls.Add(this.Second_emotionProgressBar5);
            this.FacePanel.Controls.Add(this.First_emotionProgressBar6);
            this.FacePanel.Controls.Add(this.label35);
            this.FacePanel.Controls.Add(this.label36);
            this.FacePanel.Controls.Add(this.label31);
            this.FacePanel.Controls.Add(this.label30);
            this.FacePanel.Controls.Add(this.label29);
            this.FacePanel.FillColor = System.Drawing.Color.White;
            this.FacePanel.Location = new System.Drawing.Point(689, 80);
            this.FacePanel.Name = "FacePanel";
            this.FacePanel.Radius = 10;
            this.FacePanel.ShadowColor = System.Drawing.Color.Black;
            this.FacePanel.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.FacePanel.Size = new System.Drawing.Size(303, 535);
            this.FacePanel.TabIndex = 21;
            // 
            // guna2Separator6
            // 
            this.guna2Separator6.Location = new System.Drawing.Point(16, 278);
            this.guna2Separator6.Name = "guna2Separator6";
            this.guna2Separator6.Size = new System.Drawing.Size(266, 10);
            this.guna2Separator6.TabIndex = 110;
            // 
            // guna2VSeparator1
            // 
            this.guna2VSeparator1.Location = new System.Drawing.Point(138, 298);
            this.guna2VSeparator1.Name = "guna2VSeparator1";
            this.guna2VSeparator1.Size = new System.Drawing.Size(10, 221);
            this.guna2VSeparator1.TabIndex = 109;
            // 
            // guna2Separator5
            // 
            this.guna2Separator5.Location = new System.Drawing.Point(16, 112);
            this.guna2Separator5.Name = "guna2Separator5";
            this.guna2Separator5.Size = new System.Drawing.Size(266, 10);
            this.guna2Separator5.TabIndex = 108;
            // 
            // smileRatioProgressBar9
            // 
            this.smileRatioProgressBar9.BorderColor = System.Drawing.Color.RoyalBlue;
            this.smileRatioProgressBar9.BorderThickness = 1;
            this.smileRatioProgressBar9.FillColor = System.Drawing.Color.White;
            this.smileRatioProgressBar9.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.smileRatioProgressBar9.Location = new System.Drawing.Point(154, 87);
            this.smileRatioProgressBar9.Name = "smileRatioProgressBar9";
            this.smileRatioProgressBar9.ProgressColor = System.Drawing.Color.AliceBlue;
            this.smileRatioProgressBar9.ProgressColor2 = System.Drawing.Color.Crimson;
            this.smileRatioProgressBar9.ShadowDecoration.Parent = this.smileRatioProgressBar9;
            this.smileRatioProgressBar9.Size = new System.Drawing.Size(93, 19);
            this.smileRatioProgressBar9.TabIndex = 107;
            this.smileRatioProgressBar9.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.smileRatioProgressBar9.Value = 67;
            this.smileRatioProgressBar9.MouseHover += new System.EventHandler(this.smileRatioProgressBar9_MouseHover);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label40.Location = new System.Drawing.Point(220, 49);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(68, 29);
            this.label40.TabIndex = 106;
            this.label40.Text = "次/天";
            // 
            // recordFre
            // 
            this.recordFre.AutoSize = true;
            this.recordFre.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recordFre.Location = new System.Drawing.Point(158, 52);
            this.recordFre.Name = "recordFre";
            this.recordFre.Size = new System.Drawing.Size(24, 24);
            this.recordFre.TabIndex = 105;
            this.recordFre.Text = "2";
            // 
            // beautyRatingStar1
            // 
            this.beautyRatingStar1.Location = new System.Drawing.Point(146, 14);
            this.beautyRatingStar1.Name = "beautyRatingStar1";
            this.beautyRatingStar1.RatingColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.beautyRatingStar1.Size = new System.Drawing.Size(101, 23);
            this.beautyRatingStar1.TabIndex = 104;
            this.beautyRatingStar1.Value = 2.5F;
            // 
            // pieChart2
            // 
            this.pieChart2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.pieChart2.Location = new System.Drawing.Point(138, 334);
            this.pieChart2.Name = "pieChart2";
            this.pieChart2.Size = new System.Drawing.Size(150, 198);
            this.pieChart2.TabIndex = 103;
            this.pieChart2.Text = "pieChart2";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label38.Location = new System.Drawing.Point(153, 298);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(157, 29);
            this.label38.TabIndex = 102;
            this.label38.Text = "皮肤状态比率";
            // 
            // pieChart1
            // 
            this.pieChart1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.pieChart1.Location = new System.Drawing.Point(3, 334);
            this.pieChart1.Name = "pieChart1";
            this.pieChart1.Size = new System.Drawing.Size(143, 198);
            this.pieChart1.TabIndex = 100;
            this.pieChart1.Text = "pieChart1";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label37.Location = new System.Drawing.Point(13, 298);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(133, 29);
            this.label37.TabIndex = 99;
            this.label37.Text = "黑眼圈比率";
            // 
            // Third_emotion
            // 
            this.Third_emotion.AutoSize = true;
            this.Third_emotion.Font = new System.Drawing.Font("等线", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Third_emotion.Location = new System.Drawing.Point(212, 251);
            this.Third_emotion.Name = "Third_emotion";
            this.Third_emotion.Size = new System.Drawing.Size(34, 19);
            this.Third_emotion.TabIndex = 98;
            this.Third_emotion.Text = "3rd";
            // 
            // Second_emotion
            // 
            this.Second_emotion.AutoSize = true;
            this.Second_emotion.Font = new System.Drawing.Font("等线", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Second_emotion.Location = new System.Drawing.Point(130, 251);
            this.Second_emotion.Name = "Second_emotion";
            this.Second_emotion.Size = new System.Drawing.Size(38, 19);
            this.Second_emotion.TabIndex = 97;
            this.Second_emotion.Text = "2nd";
            // 
            // First_emotion
            // 
            this.First_emotion.AutoSize = true;
            this.First_emotion.Font = new System.Drawing.Font("等线", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.First_emotion.Location = new System.Drawing.Point(54, 251);
            this.First_emotion.Name = "First_emotion";
            this.First_emotion.Size = new System.Drawing.Size(31, 19);
            this.First_emotion.TabIndex = 96;
            this.First_emotion.Text = "1st";
            // 
            // Third_emotionProgressBar4
            // 
            this.Third_emotionProgressBar4.BorderColor = System.Drawing.Color.MidnightBlue;
            this.Third_emotionProgressBar4.BorderThickness = 1;
            this.Third_emotionProgressBar4.FillColor = System.Drawing.Color.White;
            this.Third_emotionProgressBar4.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.Third_emotionProgressBar4.Location = new System.Drawing.Point(217, 155);
            this.Third_emotionProgressBar4.Name = "Third_emotionProgressBar4";
            this.Third_emotionProgressBar4.ProgressColor = System.Drawing.Color.MidnightBlue;
            this.Third_emotionProgressBar4.ProgressColor2 = System.Drawing.Color.Orange;
            this.Third_emotionProgressBar4.ShadowDecoration.Parent = this.Third_emotionProgressBar4;
            this.Third_emotionProgressBar4.Size = new System.Drawing.Size(30, 93);
            this.Third_emotionProgressBar4.TabIndex = 95;
            this.Third_emotionProgressBar4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Third_emotionProgressBar4.Value = 50;
            this.Third_emotionProgressBar4.MouseHover += new System.EventHandler(this.guna2VProgressBar4_MouseHover);
            // 
            // Second_emotionProgressBar5
            // 
            this.Second_emotionProgressBar5.BorderColor = System.Drawing.Color.MidnightBlue;
            this.Second_emotionProgressBar5.BorderThickness = 1;
            this.Second_emotionProgressBar5.FillColor = System.Drawing.Color.White;
            this.Second_emotionProgressBar5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.Second_emotionProgressBar5.Location = new System.Drawing.Point(138, 155);
            this.Second_emotionProgressBar5.Name = "Second_emotionProgressBar5";
            this.Second_emotionProgressBar5.ProgressColor = System.Drawing.Color.MidnightBlue;
            this.Second_emotionProgressBar5.ProgressColor2 = System.Drawing.Color.Orange;
            this.Second_emotionProgressBar5.ShadowDecoration.Parent = this.Second_emotionProgressBar5;
            this.Second_emotionProgressBar5.Size = new System.Drawing.Size(30, 93);
            this.Second_emotionProgressBar5.TabIndex = 94;
            this.Second_emotionProgressBar5.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Second_emotionProgressBar5.Value = 60;
            this.Second_emotionProgressBar5.MouseLeave += new System.EventHandler(this.guna2VProgressBar5_MouseLeave);
            this.Second_emotionProgressBar5.MouseHover += new System.EventHandler(this.guna2VProgressBar5_MouseHover);
            // 
            // First_emotionProgressBar6
            // 
            this.First_emotionProgressBar6.BorderColor = System.Drawing.Color.MidnightBlue;
            this.First_emotionProgressBar6.BorderThickness = 1;
            this.First_emotionProgressBar6.FillColor = System.Drawing.Color.White;
            this.First_emotionProgressBar6.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.First_emotionProgressBar6.Location = new System.Drawing.Point(58, 155);
            this.First_emotionProgressBar6.Name = "First_emotionProgressBar6";
            this.First_emotionProgressBar6.ProgressColor = System.Drawing.Color.MidnightBlue;
            this.First_emotionProgressBar6.ProgressColor2 = System.Drawing.Color.Orange;
            this.First_emotionProgressBar6.ShadowDecoration.Parent = this.First_emotionProgressBar6;
            this.First_emotionProgressBar6.Size = new System.Drawing.Size(30, 93);
            this.First_emotionProgressBar6.TabIndex = 93;
            this.First_emotionProgressBar6.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.First_emotionProgressBar6.Value = 70;
            this.First_emotionProgressBar6.MouseHover += new System.EventHandler(this.guna2VProgressBar6_MouseHover);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label35.Location = new System.Drawing.Point(120, 125);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(71, 29);
            this.label35.TabIndex = 92;
            this.label35.Text = "Top3";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label36.Location = new System.Drawing.Point(11, 125);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(109, 29);
            this.label36.TabIndex = 91;
            this.label36.Text = "情绪频率";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label31.Location = new System.Drawing.Point(19, 82);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(133, 29);
            this.label31.TabIndex = 84;
            this.label31.Text = "笑容比率：";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.Location = new System.Drawing.Point(19, 49);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(133, 29);
            this.label30.TabIndex = 83;
            this.label30.Text = "记录频次：";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label29.Location = new System.Drawing.Point(19, 14);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(133, 29);
            this.label29.TabIndex = 82;
            this.label29.Text = "颜值排名：";
            // 
            // BillPanel
            // 
            this.BillPanel.BackColor = System.Drawing.Color.Transparent;
            this.BillPanel.Controls.Add(this.guna2Separator4);
            this.BillPanel.Controls.Add(this.label28);
            this.BillPanel.Controls.Add(this.investmentRatioProgressBar8);
            this.BillPanel.Controls.Add(this.label27);
            this.BillPanel.Controls.Add(this.salaryRatioProgressBar7);
            this.BillPanel.Controls.Add(this.mean_outcome);
            this.BillPanel.Controls.Add(this.mean_income);
            this.BillPanel.Controls.Add(this.label24);
            this.BillPanel.Controls.Add(this.outcomeRatioProgressBar4);
            this.BillPanel.Controls.Add(this.label23);
            this.BillPanel.Controls.Add(this.medium_outcome);
            this.BillPanel.Controls.Add(this.label21);
            this.BillPanel.Controls.Add(this.medium_income);
            this.BillPanel.Controls.Add(this.label18);
            this.BillPanel.Controls.Add(this.label17);
            this.BillPanel.Controls.Add(this.incomeRatioProgressBar6);
            this.BillPanel.Controls.Add(this.label16);
            this.BillPanel.FillColor = System.Drawing.Color.White;
            this.BillPanel.Location = new System.Drawing.Point(354, 80);
            this.BillPanel.Name = "BillPanel";
            this.BillPanel.Radius = 10;
            this.BillPanel.ShadowColor = System.Drawing.Color.Black;
            this.BillPanel.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.BillPanel.Size = new System.Drawing.Size(303, 535);
            this.BillPanel.TabIndex = 22;
            // 
            // guna2Separator4
            // 
            this.guna2Separator4.Location = new System.Drawing.Point(21, 217);
            this.guna2Separator4.Name = "guna2Separator4";
            this.guna2Separator4.Size = new System.Drawing.Size(266, 10);
            this.guna2Separator4.TabIndex = 98;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("方正粗黑宋简体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label28.Location = new System.Drawing.Point(17, 145);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(136, 22);
            this.label28.TabIndex = 97;
            this.label28.Text = "投资所处比例：";
            // 
            // investmentRatioProgressBar8
            // 
            this.investmentRatioProgressBar8.BorderColor = System.Drawing.Color.MediumBlue;
            this.investmentRatioProgressBar8.BorderThickness = 1;
            this.investmentRatioProgressBar8.FillColor = System.Drawing.Color.White;
            this.investmentRatioProgressBar8.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.investmentRatioProgressBar8.Location = new System.Drawing.Point(172, 146);
            this.investmentRatioProgressBar8.Name = "investmentRatioProgressBar8";
            this.investmentRatioProgressBar8.ProgressColor = System.Drawing.Color.AliceBlue;
            this.investmentRatioProgressBar8.ProgressColor2 = System.Drawing.Color.Teal;
            this.investmentRatioProgressBar8.ShadowDecoration.Parent = this.investmentRatioProgressBar8;
            this.investmentRatioProgressBar8.Size = new System.Drawing.Size(93, 19);
            this.investmentRatioProgressBar8.TabIndex = 96;
            this.investmentRatioProgressBar8.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.investmentRatioProgressBar8.Value = 67;
            this.investmentRatioProgressBar8.MouseHover += new System.EventHandler(this.investmentRatioProgressBar8_MouseHover);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("方正粗黑宋简体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label27.Location = new System.Drawing.Point(17, 112);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(136, 22);
            this.label27.TabIndex = 95;
            this.label27.Text = "工资收入比例：";
            // 
            // salaryRatioProgressBar7
            // 
            this.salaryRatioProgressBar7.BorderColor = System.Drawing.Color.MediumBlue;
            this.salaryRatioProgressBar7.BorderThickness = 1;
            this.salaryRatioProgressBar7.FillColor = System.Drawing.Color.White;
            this.salaryRatioProgressBar7.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.salaryRatioProgressBar7.Location = new System.Drawing.Point(172, 113);
            this.salaryRatioProgressBar7.Name = "salaryRatioProgressBar7";
            this.salaryRatioProgressBar7.ProgressColor = System.Drawing.Color.AliceBlue;
            this.salaryRatioProgressBar7.ProgressColor2 = System.Drawing.Color.SkyBlue;
            this.salaryRatioProgressBar7.ShadowDecoration.Parent = this.salaryRatioProgressBar7;
            this.salaryRatioProgressBar7.Size = new System.Drawing.Size(93, 19);
            this.salaryRatioProgressBar7.TabIndex = 94;
            this.salaryRatioProgressBar7.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.salaryRatioProgressBar7.Value = 67;
            this.salaryRatioProgressBar7.MouseHover += new System.EventHandler(this.salaryRatioProgressBar7_MouseHover);
            // 
            // mean_outcome
            // 
            this.mean_outcome.AutoSize = true;
            this.mean_outcome.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mean_outcome.Location = new System.Drawing.Point(208, 276);
            this.mean_outcome.Name = "mean_outcome";
            this.mean_outcome.Size = new System.Drawing.Size(52, 24);
            this.mean_outcome.TabIndex = 93;
            this.mean_outcome.Text = "1.4";
            // 
            // mean_income
            // 
            this.mean_income.AutoSize = true;
            this.mean_income.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mean_income.Location = new System.Drawing.Point(208, 19);
            this.mean_income.Name = "mean_income";
            this.mean_income.Size = new System.Drawing.Size(52, 24);
            this.mean_income.TabIndex = 92;
            this.mean_income.Text = "1.4";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.Location = new System.Drawing.Point(16, 340);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(181, 29);
            this.label24.TabIndex = 91;
            this.label24.Text = "支出所处比例：";
            // 
            // outcomeRatioProgressBar4
            // 
            this.outcomeRatioProgressBar4.BorderColor = System.Drawing.Color.MediumBlue;
            this.outcomeRatioProgressBar4.BorderThickness = 1;
            this.outcomeRatioProgressBar4.FillColor = System.Drawing.Color.White;
            this.outcomeRatioProgressBar4.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.outcomeRatioProgressBar4.Location = new System.Drawing.Point(203, 350);
            this.outcomeRatioProgressBar4.Name = "outcomeRatioProgressBar4";
            this.outcomeRatioProgressBar4.ProgressColor = System.Drawing.Color.AliceBlue;
            this.outcomeRatioProgressBar4.ProgressColor2 = System.Drawing.Color.DeepPink;
            this.outcomeRatioProgressBar4.ShadowDecoration.Parent = this.outcomeRatioProgressBar4;
            this.outcomeRatioProgressBar4.Size = new System.Drawing.Size(93, 19);
            this.outcomeRatioProgressBar4.TabIndex = 90;
            this.outcomeRatioProgressBar4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.outcomeRatioProgressBar4.Value = 67;
            this.outcomeRatioProgressBar4.ValueChanged += new System.EventHandler(this.outcomeRatioProgressBar4_ValueChanged);
            this.outcomeRatioProgressBar4.MouseHover += new System.EventHandler(this.outcomeRatioProgressBar4_MouseHover);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("方正粗黑宋简体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.Location = new System.Drawing.Point(17, 77);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(136, 22);
            this.label23.TabIndex = 89;
            this.label23.Text = "收入所处比例：";
            // 
            // medium_outcome
            // 
            this.medium_outcome.AutoSize = true;
            this.medium_outcome.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.medium_outcome.Location = new System.Drawing.Point(208, 306);
            this.medium_outcome.Name = "medium_outcome";
            this.medium_outcome.Size = new System.Drawing.Size(52, 24);
            this.medium_outcome.TabIndex = 88;
            this.medium_outcome.Text = "1.4";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label21.Location = new System.Drawing.Point(17, 306);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(157, 29);
            this.label21.TabIndex = 87;
            this.label21.Text = "支出中位数：";
            // 
            // medium_income
            // 
            this.medium_income.AutoSize = true;
            this.medium_income.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.medium_income.Location = new System.Drawing.Point(208, 45);
            this.medium_income.Name = "medium_income";
            this.medium_income.Size = new System.Drawing.Size(52, 24);
            this.medium_income.TabIndex = 86;
            this.medium_income.Text = "1.4";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(17, 45);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(157, 29);
            this.label18.TabIndex = 85;
            this.label18.Text = "收入中位数：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(17, 278);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(133, 29);
            this.label17.TabIndex = 83;
            this.label17.Text = "平均支出：";
            // 
            // incomeRatioProgressBar6
            // 
            this.incomeRatioProgressBar6.BorderColor = System.Drawing.Color.MediumBlue;
            this.incomeRatioProgressBar6.BorderThickness = 1;
            this.incomeRatioProgressBar6.FillColor = System.Drawing.Color.White;
            this.incomeRatioProgressBar6.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.incomeRatioProgressBar6.Location = new System.Drawing.Point(172, 81);
            this.incomeRatioProgressBar6.Name = "incomeRatioProgressBar6";
            this.incomeRatioProgressBar6.ProgressColor = System.Drawing.Color.AliceBlue;
            this.incomeRatioProgressBar6.ProgressColor2 = System.Drawing.Color.Green;
            this.incomeRatioProgressBar6.ShadowDecoration.Parent = this.incomeRatioProgressBar6;
            this.incomeRatioProgressBar6.Size = new System.Drawing.Size(93, 19);
            this.incomeRatioProgressBar6.TabIndex = 82;
            this.incomeRatioProgressBar6.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.incomeRatioProgressBar6.Value = 67;
            this.incomeRatioProgressBar6.MouseHover += new System.EventHandler(this.incomeRatioProgressBar6_MouseHover);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(17, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(133, 29);
            this.label16.TabIndex = 81;
            this.label16.Text = "平均收入：";
            // 
            // CloseButton
            // 
            this.CloseButton.CheckedState.Parent = this.CloseButton;
            this.CloseButton.CustomImages.Parent = this.CloseButton;
            this.CloseButton.FillColor = System.Drawing.Color.White;
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.CloseButton.ForeColor = System.Drawing.Color.MidnightBlue;
            this.CloseButton.HoverState.Parent = this.CloseButton;
            this.CloseButton.Location = new System.Drawing.Point(972, 9);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.CloseButton.ShadowDecoration.Parent = this.CloseButton;
            this.CloseButton.Size = new System.Drawing.Size(32, 32);
            this.CloseButton.TabIndex = 70;
            this.CloseButton.Text = "×";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // NotePanel
            // 
            this.NotePanel.BackColor = System.Drawing.Color.Transparent;
            this.NotePanel.Controls.Add(this.mean_mode);
            this.NotePanel.Controls.Add(this.guna2Separator3);
            this.NotePanel.Controls.Add(this.guna2Separator2);
            this.NotePanel.Controls.Add(this.guna2Separator1);
            this.NotePanel.Controls.Add(this.wordAccountProgressBar5);
            this.NotePanel.Controls.Add(this.label15);
            this.NotePanel.Controls.Add(this.Third);
            this.NotePanel.Controls.Add(this.Second);
            this.NotePanel.Controls.Add(this.First);
            this.NotePanel.Controls.Add(this.ThirdProgressBar3);
            this.NotePanel.Controls.Add(this.SecondProgressBar2);
            this.NotePanel.Controls.Add(this.FirstProgressBar1);
            this.NotePanel.Controls.Add(this.label11);
            this.NotePanel.Controls.Add(this.label10);
            this.NotePanel.Controls.Add(this.most_mode);
            this.NotePanel.Controls.Add(this.label8);
            this.NotePanel.Controls.Add(this.negativeModeBar);
            this.NotePanel.Controls.Add(this.neutralModeBar);
            this.NotePanel.Controls.Add(this.positiveModeBar);
            this.NotePanel.Controls.Add(this.label7);
            this.NotePanel.Controls.Add(this.label6);
            this.NotePanel.Controls.Add(this.label5);
            this.NotePanel.Controls.Add(this.label4);
            this.NotePanel.FillColor = System.Drawing.Color.White;
            this.NotePanel.Location = new System.Drawing.Point(16, 80);
            this.NotePanel.Name = "NotePanel";
            this.NotePanel.Radius = 10;
            this.NotePanel.ShadowColor = System.Drawing.Color.Black;
            this.NotePanel.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.NotePanel.Size = new System.Drawing.Size(303, 535);
            this.NotePanel.TabIndex = 71;
            this.NotePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.NotePanel_Paint);
            // 
            // mean_mode
            // 
            this.mean_mode.AutoSize = true;
            this.mean_mode.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mean_mode.Location = new System.Drawing.Point(153, 18);
            this.mean_mode.Name = "mean_mode";
            this.mean_mode.Size = new System.Drawing.Size(52, 24);
            this.mean_mode.TabIndex = 96;
            this.mean_mode.Text = "1.4";
            // 
            // guna2Separator3
            // 
            this.guna2Separator3.Location = new System.Drawing.Point(18, 432);
            this.guna2Separator3.Name = "guna2Separator3";
            this.guna2Separator3.Size = new System.Drawing.Size(266, 10);
            this.guna2Separator3.TabIndex = 95;
            // 
            // guna2Separator2
            // 
            this.guna2Separator2.Location = new System.Drawing.Point(18, 217);
            this.guna2Separator2.Name = "guna2Separator2";
            this.guna2Separator2.Size = new System.Drawing.Size(266, 10);
            this.guna2Separator2.TabIndex = 94;
            // 
            // guna2Separator1
            // 
            this.guna2Separator1.Location = new System.Drawing.Point(18, 90);
            this.guna2Separator1.Name = "guna2Separator1";
            this.guna2Separator1.Size = new System.Drawing.Size(266, 10);
            this.guna2Separator1.TabIndex = 93;
            // 
            // wordAccountProgressBar5
            // 
            this.wordAccountProgressBar5.BorderThickness = 1;
            this.wordAccountProgressBar5.FillColor = System.Drawing.Color.White;
            this.wordAccountProgressBar5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.wordAccountProgressBar5.Location = new System.Drawing.Point(151, 471);
            this.wordAccountProgressBar5.Name = "wordAccountProgressBar5";
            this.wordAccountProgressBar5.ProgressColor = System.Drawing.Color.AliceBlue;
            this.wordAccountProgressBar5.ProgressColor2 = System.Drawing.Color.Indigo;
            this.wordAccountProgressBar5.ShadowDecoration.Parent = this.wordAccountProgressBar5;
            this.wordAccountProgressBar5.Size = new System.Drawing.Size(116, 19);
            this.wordAccountProgressBar5.TabIndex = 92;
            this.wordAccountProgressBar5.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.wordAccountProgressBar5.Value = 67;
            this.wordAccountProgressBar5.MouseHover += new System.EventHandler(this.wordAccountProgressBar5_MouseHover);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(14, 461);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(133, 29);
            this.label15.TabIndex = 91;
            this.label15.Text = "平均字数：";
            // 
            // Third
            // 
            this.Third.AutoSize = true;
            this.Third.Font = new System.Drawing.Font("等线", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Third.Location = new System.Drawing.Point(208, 385);
            this.Third.Name = "Third";
            this.Third.Size = new System.Drawing.Size(39, 21);
            this.Third.TabIndex = 90;
            this.Third.Text = "3rd";
            // 
            // Second
            // 
            this.Second.AutoSize = true;
            this.Second.Font = new System.Drawing.Font("等线", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Second.Location = new System.Drawing.Point(126, 385);
            this.Second.Name = "Second";
            this.Second.Size = new System.Drawing.Size(43, 21);
            this.Second.TabIndex = 89;
            this.Second.Text = "2nd";
            // 
            // First
            // 
            this.First.AutoSize = true;
            this.First.Font = new System.Drawing.Font("等线", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.First.Location = new System.Drawing.Point(50, 385);
            this.First.Name = "First";
            this.First.Size = new System.Drawing.Size(35, 21);
            this.First.TabIndex = 88;
            this.First.Text = "1st";
            // 
            // ThirdProgressBar3
            // 
            this.ThirdProgressBar3.BorderColor = System.Drawing.Color.MidnightBlue;
            this.ThirdProgressBar3.BorderThickness = 1;
            this.ThirdProgressBar3.FillColor = System.Drawing.Color.White;
            this.ThirdProgressBar3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.ThirdProgressBar3.Location = new System.Drawing.Point(212, 276);
            this.ThirdProgressBar3.Maximum = 20;
            this.ThirdProgressBar3.Name = "ThirdProgressBar3";
            this.ThirdProgressBar3.ProgressColor = System.Drawing.Color.MidnightBlue;
            this.ThirdProgressBar3.ProgressColor2 = System.Drawing.Color.Yellow;
            this.ThirdProgressBar3.ShadowDecoration.Parent = this.ThirdProgressBar3;
            this.ThirdProgressBar3.Size = new System.Drawing.Size(30, 93);
            this.ThirdProgressBar3.TabIndex = 87;
            this.ThirdProgressBar3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ThirdProgressBar3.Value = 20;
            this.ThirdProgressBar3.MouseHover += new System.EventHandler(this.ThirdProgressBar3_MouseHover);
            // 
            // SecondProgressBar2
            // 
            this.SecondProgressBar2.BorderColor = System.Drawing.Color.MidnightBlue;
            this.SecondProgressBar2.BorderThickness = 1;
            this.SecondProgressBar2.FillColor = System.Drawing.Color.White;
            this.SecondProgressBar2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.SecondProgressBar2.Location = new System.Drawing.Point(133, 276);
            this.SecondProgressBar2.Maximum = 20;
            this.SecondProgressBar2.Name = "SecondProgressBar2";
            this.SecondProgressBar2.ProgressColor = System.Drawing.Color.MidnightBlue;
            this.SecondProgressBar2.ProgressColor2 = System.Drawing.Color.Yellow;
            this.SecondProgressBar2.ShadowDecoration.Parent = this.SecondProgressBar2;
            this.SecondProgressBar2.Size = new System.Drawing.Size(30, 93);
            this.SecondProgressBar2.TabIndex = 86;
            this.SecondProgressBar2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.SecondProgressBar2.Value = 20;
            this.SecondProgressBar2.MouseHover += new System.EventHandler(this.SecondProgressBar2_MouseHover);
            // 
            // FirstProgressBar1
            // 
            this.FirstProgressBar1.BorderColor = System.Drawing.Color.MidnightBlue;
            this.FirstProgressBar1.BorderThickness = 1;
            this.FirstProgressBar1.FillColor = System.Drawing.Color.White;
            this.FirstProgressBar1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.FirstProgressBar1.Location = new System.Drawing.Point(53, 276);
            this.FirstProgressBar1.Maximum = 20;
            this.FirstProgressBar1.Name = "FirstProgressBar1";
            this.FirstProgressBar1.ProgressColor = System.Drawing.Color.MidnightBlue;
            this.FirstProgressBar1.ProgressColor2 = System.Drawing.Color.Yellow;
            this.FirstProgressBar1.ShadowDecoration.Parent = this.FirstProgressBar1;
            this.FirstProgressBar1.Size = new System.Drawing.Size(30, 93);
            this.FirstProgressBar1.TabIndex = 85;
            this.FirstProgressBar1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.FirstProgressBar1.Value = 20;
            this.FirstProgressBar1.MouseHover += new System.EventHandler(this.FirstProgressBar1_MouseHover);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(89, 236);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 29);
            this.label11.TabIndex = 84;
            this.label11.Text = "Top3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(14, 237);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 29);
            this.label10.TabIndex = 83;
            this.label10.Text = "关键词";
            // 
            // most_mode
            // 
            this.most_mode.AutoSize = true;
            this.most_mode.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.most_mode.Location = new System.Drawing.Point(153, 56);
            this.most_mode.Name = "most_mode";
            this.most_mode.Size = new System.Drawing.Size(52, 24);
            this.most_mode.TabIndex = 82;
            this.most_mode.Text = "1.4";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(14, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 29);
            this.label8.TabIndex = 81;
            this.label8.Text = "情感众数：";
            // 
            // negativeModeBar
            // 
            this.negativeModeBar.BorderThickness = 1;
            this.negativeModeBar.FillColor = System.Drawing.Color.White;
            this.negativeModeBar.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.negativeModeBar.Location = new System.Drawing.Point(188, 192);
            this.negativeModeBar.Name = "negativeModeBar";
            this.negativeModeBar.ProgressColor = System.Drawing.Color.AliceBlue;
            this.negativeModeBar.ProgressColor2 = System.Drawing.Color.Black;
            this.negativeModeBar.ShadowDecoration.Parent = this.negativeModeBar;
            this.negativeModeBar.Size = new System.Drawing.Size(79, 19);
            this.negativeModeBar.TabIndex = 79;
            this.negativeModeBar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.negativeModeBar.Value = 67;
            this.negativeModeBar.MouseHover += new System.EventHandler(this.negativeModeBar_MouseHover);
            // 
            // neutralModeBar
            // 
            this.neutralModeBar.BorderThickness = 1;
            this.neutralModeBar.FillColor = System.Drawing.Color.White;
            this.neutralModeBar.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.neutralModeBar.Location = new System.Drawing.Point(188, 155);
            this.neutralModeBar.Name = "neutralModeBar";
            this.neutralModeBar.ProgressColor = System.Drawing.Color.AliceBlue;
            this.neutralModeBar.ProgressColor2 = System.Drawing.Color.MediumBlue;
            this.neutralModeBar.ShadowDecoration.Parent = this.neutralModeBar;
            this.neutralModeBar.Size = new System.Drawing.Size(79, 19);
            this.neutralModeBar.TabIndex = 78;
            this.neutralModeBar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.neutralModeBar.Value = 67;
            this.neutralModeBar.MouseHover += new System.EventHandler(this.neutralModeBar_MouseHover);
            // 
            // positiveModeBar
            // 
            this.positiveModeBar.BorderThickness = 1;
            this.positiveModeBar.FillColor = System.Drawing.Color.White;
            this.positiveModeBar.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.positiveModeBar.Location = new System.Drawing.Point(188, 122);
            this.positiveModeBar.Name = "positiveModeBar";
            this.positiveModeBar.ProgressColor = System.Drawing.Color.AliceBlue;
            this.positiveModeBar.ProgressColor2 = System.Drawing.Color.Red;
            this.positiveModeBar.ShadowDecoration.Parent = this.positiveModeBar;
            this.positiveModeBar.Size = new System.Drawing.Size(79, 19);
            this.positiveModeBar.TabIndex = 77;
            this.positiveModeBar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.positiveModeBar.Value = 67;
            this.positiveModeBar.MouseHover += new System.EventHandler(this.positiveModeBar_MouseHover);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(14, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(181, 29);
            this.label7.TabIndex = 76;
            this.label7.Text = "负面情感比例：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(14, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(181, 29);
            this.label6.TabIndex = 75;
            this.label6.Text = "中性情感比例：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(14, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 29);
            this.label5.TabIndex = 74;
            this.label5.Text = "正面情感比例：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(14, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 29);
            this.label4.TabIndex = 73;
            this.label4.Text = "平均情感：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("等线", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 29);
            this.label2.TabIndex = 72;
            this.label2.Text = "小记";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("等线", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(350, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 29);
            this.label1.TabIndex = 73;
            this.label1.Text = "账单";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("等线", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(685, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 29);
            this.label3.TabIndex = 74;
            this.label3.Text = "自拍";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("方正粗黑宋简体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "男性",
            "女性"});
            this.comboBox1.Location = new System.Drawing.Point(871, 49);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 30);
            this.comboBox1.TabIndex = 75;
            this.comboBox1.Text = "男性";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.Font = new System.Drawing.Font("方正粗黑宋简体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "过去一周",
            "过去一月"});
            this.comboBox2.Location = new System.Drawing.Point(16, 12);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 30);
            this.comboBox2.TabIndex = 76;
            this.comboBox2.Text = "过去一月";
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // UCinbox_statics
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NotePanel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.BillPanel);
            this.Controls.Add(this.FacePanel);
            this.Name = "UCinbox_statics";
            this.Size = new System.Drawing.Size(1015, 672);
            this.FacePanel.ResumeLayout(false);
            this.FacePanel.PerformLayout();
            this.BillPanel.ResumeLayout(false);
            this.BillPanel.PerformLayout();
            this.NotePanel.ResumeLayout(false);
            this.NotePanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2ShadowPanel FacePanel;
        private Guna.UI2.WinForms.Guna2ShadowPanel BillPanel;
        private Guna.UI2.WinForms.Guna2CircleButton CloseButton;
        private Guna.UI2.WinForms.Guna2ShadowPanel NotePanel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label mean_mode;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator3;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator2;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator1;
        private Guna.UI2.WinForms.Guna2ProgressBar wordAccountProgressBar5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label Third;
        private System.Windows.Forms.Label Second;
        private System.Windows.Forms.Label First;
        private Guna.UI2.WinForms.Guna2VProgressBar ThirdProgressBar3;
        private Guna.UI2.WinForms.Guna2VProgressBar SecondProgressBar2;
        private Guna.UI2.WinForms.Guna2VProgressBar FirstProgressBar1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label most_mode;
        private System.Windows.Forms.Label label8;
        private Guna.UI2.WinForms.Guna2ProgressBar negativeModeBar;
        private Guna.UI2.WinForms.Guna2ProgressBar neutralModeBar;
        private Guna.UI2.WinForms.Guna2ProgressBar positiveModeBar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator4;
        private System.Windows.Forms.Label label28;
        private Guna.UI2.WinForms.Guna2ProgressBar investmentRatioProgressBar8;
        private System.Windows.Forms.Label label27;
        private Guna.UI2.WinForms.Guna2ProgressBar salaryRatioProgressBar7;
        private System.Windows.Forms.Label mean_outcome;
        private System.Windows.Forms.Label mean_income;
        private System.Windows.Forms.Label label24;
        private Guna.UI2.WinForms.Guna2ProgressBar outcomeRatioProgressBar4;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label medium_outcome;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label medium_income;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private Guna.UI2.WinForms.Guna2ProgressBar incomeRatioProgressBar6;
        private System.Windows.Forms.Label label16;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator6;
        private Guna.UI2.WinForms.Guna2VSeparator guna2VSeparator1;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator5;
        private Guna.UI2.WinForms.Guna2ProgressBar smileRatioProgressBar9;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label recordFre;
        private Guna.UI2.WinForms.Guna2RatingStar beautyRatingStar1;
        private LiveCharts.WinForms.PieChart pieChart2;
        private System.Windows.Forms.Label label38;
        private LiveCharts.WinForms.PieChart pieChart1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label Third_emotion;
        private System.Windows.Forms.Label Second_emotion;
        private System.Windows.Forms.Label First_emotion;
        private Guna.UI2.WinForms.Guna2VProgressBar Third_emotionProgressBar4;
        private Guna.UI2.WinForms.Guna2VProgressBar Second_emotionProgressBar5;
        private Guna.UI2.WinForms.Guna2VProgressBar First_emotionProgressBar6;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
    }
}
