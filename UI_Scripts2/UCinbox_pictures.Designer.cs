﻿namespace UI_Scripts2
{
    partial class UCinbox_pictures
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCinbox_pictures));
            this.guna2DateTimePicker1 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2ShadowPanel1 = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.guna2TileButton1 = new Guna.UI2.WinForms.Guna2TileButton();
            this.guna2ClearButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.guna2GetButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.guna2PostButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.listView1 = new System.Windows.Forms.ListView();
            this.ImgIndex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ImgPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CloseButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.SuspendLayout();
            // 
            // guna2DateTimePicker1
            // 
            this.guna2DateTimePicker1.BackColor = System.Drawing.Color.Transparent;
            this.guna2DateTimePicker1.BorderRadius = 10;
            this.guna2DateTimePicker1.CheckedState.Parent = this.guna2DateTimePicker1;
            this.guna2DateTimePicker1.FillColor = System.Drawing.Color.MidnightBlue;
            this.guna2DateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.guna2DateTimePicker1.ForeColor = System.Drawing.Color.White;
            this.guna2DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.guna2DateTimePicker1.HoverState.Parent = this.guna2DateTimePicker1;
            this.guna2DateTimePicker1.Location = new System.Drawing.Point(15, 15);
            this.guna2DateTimePicker1.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.guna2DateTimePicker1.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.guna2DateTimePicker1.Name = "guna2DateTimePicker1";
            this.guna2DateTimePicker1.ShadowDecoration.Parent = this.guna2DateTimePicker1;
            this.guna2DateTimePicker1.Size = new System.Drawing.Size(337, 70);
            this.guna2DateTimePicker1.TabIndex = 0;
            this.guna2DateTimePicker1.Value = new System.DateTime(2020, 5, 27, 17, 10, 57, 0);
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 30;
            this.guna2Elipse1.TargetControl = this;
            // 
            // guna2ShadowPanel1
            // 
            this.guna2ShadowPanel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2ShadowPanel1.FillColor = System.Drawing.Color.White;
            this.guna2ShadowPanel1.Location = new System.Drawing.Point(429, 131);
            this.guna2ShadowPanel1.Name = "guna2ShadowPanel1";
            this.guna2ShadowPanel1.Radius = 20;
            this.guna2ShadowPanel1.ShadowColor = System.Drawing.Color.Black;
            this.guna2ShadowPanel1.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.guna2ShadowPanel1.Size = new System.Drawing.Size(567, 455);
            this.guna2ShadowPanel1.TabIndex = 2;
            this.guna2ShadowPanel1.Click += new System.EventHandler(this.guna2ShadowPanel1_Click);
            this.guna2ShadowPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.guna2ShadowPanel1_Paint);
            // 
            // guna2TileButton1
            // 
            this.guna2TileButton1.BorderRadius = 10;
            this.guna2TileButton1.CheckedState.Parent = this.guna2TileButton1;
            this.guna2TileButton1.CustomImages.Parent = this.guna2TileButton1;
            this.guna2TileButton1.FillColor = System.Drawing.Color.MidnightBlue;
            this.guna2TileButton1.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2TileButton1.ForeColor = System.Drawing.Color.White;
            this.guna2TileButton1.HoverState.Parent = this.guna2TileButton1;
            this.guna2TileButton1.Location = new System.Drawing.Point(98, 534);
            this.guna2TileButton1.Name = "guna2TileButton1";
            this.guna2TileButton1.ShadowDecoration.Parent = this.guna2TileButton1;
            this.guna2TileButton1.Size = new System.Drawing.Size(182, 52);
            this.guna2TileButton1.TabIndex = 19;
            this.guna2TileButton1.Text = "从文件夹导入";
            this.guna2TileButton1.Click += new System.EventHandler(this.guna2TileButton1_Click);
            // 
            // guna2ClearButton
            // 
            this.guna2ClearButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.guna2ClearButton.BorderThickness = 2;
            this.guna2ClearButton.CheckedState.Parent = this.guna2ClearButton;
            this.guna2ClearButton.CustomImages.Parent = this.guna2ClearButton;
            this.guna2ClearButton.FillColor = System.Drawing.Color.White;
            this.guna2ClearButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2ClearButton.ForeColor = System.Drawing.Color.White;
            this.guna2ClearButton.HoverState.Parent = this.guna2ClearButton;
            this.guna2ClearButton.Image = ((System.Drawing.Image)(resources.GetObject("guna2ClearButton.Image")));
            this.guna2ClearButton.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2ClearButton.Location = new System.Drawing.Point(759, 73);
            this.guna2ClearButton.Name = "guna2ClearButton";
            this.guna2ClearButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2ClearButton.ShadowDecoration.Parent = this.guna2ClearButton;
            this.guna2ClearButton.Size = new System.Drawing.Size(50, 50);
            this.guna2ClearButton.TabIndex = 21;
            this.guna2ClearButton.Click += new System.EventHandler(this.guna2ClearButton_Click);
            this.guna2ClearButton.MouseHover += new System.EventHandler(this.guna2ClearButton_MouseHover);
            // 
            // guna2GetButton
            // 
            this.guna2GetButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.guna2GetButton.BorderThickness = 2;
            this.guna2GetButton.CheckedState.Parent = this.guna2GetButton;
            this.guna2GetButton.CustomImages.Parent = this.guna2GetButton;
            this.guna2GetButton.FillColor = System.Drawing.Color.White;
            this.guna2GetButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2GetButton.ForeColor = System.Drawing.Color.White;
            this.guna2GetButton.HoverState.Parent = this.guna2GetButton;
            this.guna2GetButton.Image = ((System.Drawing.Image)(resources.GetObject("guna2GetButton.Image")));
            this.guna2GetButton.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2GetButton.Location = new System.Drawing.Point(586, 73);
            this.guna2GetButton.Name = "guna2GetButton";
            this.guna2GetButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2GetButton.ShadowDecoration.Parent = this.guna2GetButton;
            this.guna2GetButton.Size = new System.Drawing.Size(50, 50);
            this.guna2GetButton.TabIndex = 22;
            this.guna2GetButton.Click += new System.EventHandler(this.guna2GetButton_Click);
            this.guna2GetButton.MouseHover += new System.EventHandler(this.guna2GetButton_MouseHover);
            // 
            // guna2PostButton
            // 
            this.guna2PostButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.guna2PostButton.BorderThickness = 2;
            this.guna2PostButton.CheckedState.Parent = this.guna2PostButton;
            this.guna2PostButton.CustomImages.Parent = this.guna2PostButton;
            this.guna2PostButton.FillColor = System.Drawing.Color.White;
            this.guna2PostButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2PostButton.ForeColor = System.Drawing.Color.White;
            this.guna2PostButton.HoverState.Parent = this.guna2PostButton;
            this.guna2PostButton.Image = ((System.Drawing.Image)(resources.GetObject("guna2PostButton.Image")));
            this.guna2PostButton.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2PostButton.Location = new System.Drawing.Point(672, 73);
            this.guna2PostButton.Name = "guna2PostButton";
            this.guna2PostButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2PostButton.ShadowDecoration.Parent = this.guna2PostButton;
            this.guna2PostButton.Size = new System.Drawing.Size(50, 50);
            this.guna2PostButton.TabIndex = 23;
            this.guna2PostButton.Click += new System.EventHandler(this.guna2PostButton_Click);
            this.guna2PostButton.MouseHover += new System.EventHandler(this.guna2PostButton_MouseHover);
            // 
            // listView1
            // 
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ImgIndex,
            this.ImgPath});
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(15, 131);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(381, 379);
            this.listView1.TabIndex = 24;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SizeChanged += new System.EventHandler(this.listView1_SizeChanged);
            // 
            // ImgIndex
            // 
            this.ImgIndex.Text = "ImgIndex";
            this.ImgIndex.Width = 120;
            // 
            // ImgPath
            // 
            this.ImgPath.Text = "ImgPath";
            this.ImgPath.Width = 328;
            // 
            // CloseButton
            // 
            this.CloseButton.CheckedState.Parent = this.CloseButton;
            this.CloseButton.CustomImages.Parent = this.CloseButton;
            this.CloseButton.FillColor = System.Drawing.Color.White;
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.CloseButton.ForeColor = System.Drawing.Color.MidnightBlue;
            this.CloseButton.HoverState.Parent = this.CloseButton;
            this.CloseButton.Location = new System.Drawing.Point(972, 9);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.CloseButton.ShadowDecoration.Parent = this.CloseButton;
            this.CloseButton.Size = new System.Drawing.Size(32, 32);
            this.CloseButton.TabIndex = 25;
            this.CloseButton.Text = "×";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // UCinbox_pictures
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.guna2PostButton);
            this.Controls.Add(this.guna2GetButton);
            this.Controls.Add(this.guna2ClearButton);
            this.Controls.Add(this.guna2TileButton1);
            this.Controls.Add(this.guna2ShadowPanel1);
            this.Controls.Add(this.guna2DateTimePicker1);
            this.Name = "UCinbox_pictures";
            this.Size = new System.Drawing.Size(1015, 672);
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2DateTimePicker guna2DateTimePicker1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2ShadowPanel guna2ShadowPanel1;
        private Guna.UI2.WinForms.Guna2TileButton guna2TileButton1;
        private Guna.UI2.WinForms.Guna2CircleButton guna2ClearButton;
        private Guna.UI2.WinForms.Guna2CircleButton guna2GetButton;
        private Guna.UI2.WinForms.Guna2CircleButton guna2PostButton;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader ImgIndex;
        private System.Windows.Forms.ColumnHeader ImgPath;
        private Guna.UI2.WinForms.Guna2CircleButton CloseButton;
    }
}
