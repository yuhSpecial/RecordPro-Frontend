﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;

namespace UI_Scripts2
{
    class Api
    {
        public enum StatusCode
        {
            Created,                  //表明创建成功
            Conflict,                 //表明创建重复
            OK,                       //200
        }
        public const string baseurl = "https://yuh.ziqiang.net.cn/";
        public const string APIKey = "INSERT_API_KEY_HERE";
        //public string jsonRaw;
        private static HttpClient HttpClient;
        private static Regex rx;
        public static string Token;
        static Api()
        {
            HttpClient = new HttpClient();
            string pattern;
            pattern = @"token"":""(.*)""}";
            rx = new Regex(pattern, RegexOptions.Compiled);
            
        }
        //User-Information
        //用于用户的登入注册
        public string SignUpUserAsync(UserBase user)
        {
            //HttpResponseMessage x = await HttpClient.PostAsync(baseurl + "register?"+user, content);
            HttpContent content = new StringContent(" ");
            var x = HttpClient.PostAsync(baseurl + "register?"+user.ToString(), content).Result;
            return x.StatusCode.ToString();
        }
        public bool SignIn(UserBase user)
        {
            HttpContent content = new StringContent(" ");
            var x = HttpClient.PostAsync(baseurl + "login?" + user.ToString(), content).Result;
            if (x.StatusCode.ToString() == "OK")
            {
                Token = rx.Match(x.Content.ReadAsStringAsync().Result).Groups[1].Value;
;
                return true;
            }
            else
            {
                return false;
            }
        }
        
        
        //UserBills
        public List<UserBills> GetUserBillsAsync(string token,int days)
        {
            List<UserBills> userbills = null;
            try
            {
                HttpResponseMessage x = HttpClient.GetAsync("https://yuh.ziqiang.net.cn/api/UserBills?token=" + token + "&days=" + days).Result;
                string tempdata = x.Content.ReadAsStringAsync().Result;
                userbills = JsonConvert.DeserializeObject<List<UserBills>>(tempdata);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            return userbills;

        }
        public string PostUserBillsAsync(PostBill postbill)
        {

            string json = JsonConvert.SerializeObject(postbill);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            HttpResponseMessage x = HttpClient.PostAsync(baseurl + "api/UserBills?", content).Result;
            return x.StatusCode.ToString();
        }
        public string DeleteUserBillsAsync(string token,int id)
        {
            HttpResponseMessage x = HttpClient.DeleteAsync(baseurl + "api/UserBills?" + "id=" + id + "&token=" + token).Result;
            return x.StatusCode.ToString();

        }
        public string PutUserBillsAsync(string token, int id, PostBill postbill)
        {
            
            string json = JsonConvert.SerializeObject(postbill);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            HttpResponseMessage x = HttpClient.PutAsync(baseurl + "api/UserBills?" + "id=" + id + "&token=" + token, content).Result;
            return x.StatusCode.ToString(); 
        }
        public BillStatistics GetBillStatisticsAsync(string token, string period)
        {
            BillStatistics billStatistics = null;
            try
            {
                HttpResponseMessage x = HttpClient.GetAsync("https://yuh.ziqiang.net.cn/api/UserBills/statistics?token=" + token + "&period=" + period).Result;
                string tempdata = x.Content.ReadAsStringAsync().Result;
                billStatistics = JsonConvert.DeserializeObject<BillStatistics>(tempdata);
                Console.WriteLine(x.StatusCode.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return billStatistics;
        }


        //UserNotes
        public List<UserNotes> GetUserNotesAsync(string token, int days)
        {
            List<UserNotes> usernotes = null;
            try
            {
                HttpResponseMessage x = HttpClient.GetAsync("https://yuh.ziqiang.net.cn/api/UserNotes?token=" + token + "&days=" + days).Result;
                string tempdata = x.Content.ReadAsStringAsync().Result;
                usernotes = JsonConvert.DeserializeObject<List<UserNotes>>(tempdata);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return usernotes;

        }
        public string PostUserNotesAsync(PostNote postnote)
        {

            string json = JsonConvert.SerializeObject(postnote);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            HttpResponseMessage x = HttpClient.PostAsync(baseurl + "api/UserNotes?", content).Result;
            return x.StatusCode.ToString();
        }
        public string DeleteUserNotesAsync(string token, int id)
        {
            HttpResponseMessage x = HttpClient.DeleteAsync(baseurl + "api/UserNotes?" + "id=" + id + "&token=" + token).Result;
            return x.StatusCode.ToString();

        }
        public string PutUserNotesAsync(string token, int id, PostNote postnotes)
        {

            string json = JsonConvert.SerializeObject(postnotes);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            HttpResponseMessage x = HttpClient.PutAsync(baseurl + "api/UserNotes?" + "id=" + id + "&token=" + token, content).Result;
            return x.StatusCode.ToString();
        }


        //UserFaces
        public List<UserFaces> GetUserFacesAsync(string token, int days)
        {
            List<UserFaces> userfaces = null;
            try
            {
                HttpResponseMessage x = HttpClient.GetAsync("https://yuh.ziqiang.net.cn/api/UserFaces?token=" + token + "&days=" + days).Result;
                string tempdata = x.Content.ReadAsStringAsync().Result;
                userfaces = JsonConvert.DeserializeObject<List<UserFaces>>(tempdata);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return userfaces;

        }
        public string DeleteUserFacesAsync(string token, int id)
        {
            HttpResponseMessage x = HttpClient.DeleteAsync(baseurl + "api/UserFaces?" + "id=" + id + "&token=" + token).Result;
            return x.StatusCode.ToString();

        }
        public string PostUserFacesAsync(string token,byte[] img,string name)
        {

            var content =new MultipartFormDataContent();


            content.Add(new ByteArrayContent(img),"file","2");
            //HttpContent content = new ByteArrayContent(img);
            //var boundary = "---------------" + DateTime.Now.Ticks.ToString("x");

            //content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("multipart/form-data; boundary=" + boundary);
            HttpResponseMessage x = HttpClient.PostAsync(baseurl + "api/UserFaces?" + "token=" + token + "&datetime=" + DateTime.Now.ToString().Replace(" ", "%20").Replace(":", "%3A").Replace("/", "-"), content).Result;
            
            Console.WriteLine(baseurl + "api/UserFaces?" + "token=" + token + "&datetime=" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss").Replace(" ", "%20").Replace(":", "%3A").Replace("/", "-"));
            //.Replace(" ", "%20").Replace(":", "%3A").Replace("/","-")
            return x.StatusCode.ToString();
            //return HttpUtility.UrlEncode(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            //return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")

        }
        public FaceStatistic GetUserFacesAnalysisAsync(string token, string period, int gender)
        {
            FaceStatistic analysis = null;
            try
            {
                HttpResponseMessage x = HttpClient.GetAsync("https://yuh.ziqiang.net.cn/api/UserFaces/statistics?token=" + token + "&period=" + period + "&gender="+gender.ToString()).Result;
                string tempdata = x.Content.ReadAsStringAsync().Result;
                analysis = JsonConvert.DeserializeObject<FaceStatistic>(tempdata);
            }
            catch (Exception e)
            {
                
                Console.WriteLine(e.Message);
            }
            //Console.WriteLine(analysis.ToString());
            return analysis;

        }


        //Body
        public List<UserGetBody> GetUserBodies(string token)
        {
            List<UserGetBody> userbodies = null;
            try
            {
                HttpResponseMessage x = HttpClient.GetAsync("https://yuh.ziqiang.net.cn/api/UserBodies?token=" + token).Result;
                string tempdata = x.Content.ReadAsStringAsync().Result;
                userbodies = JsonConvert.DeserializeObject<List<UserGetBody>>(tempdata);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return userbodies;
        }
        public string PostUserBody(UserBody body)
        {
            string json = JsonConvert.SerializeObject(body);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            HttpResponseMessage x = HttpClient.PostAsync(baseurl + "api/UserBodies?", content).Result;
            return x.StatusCode.ToString();
        }
        public string PutUserBody( int id, UserBody userbody)
        {

            string json = JsonConvert.SerializeObject(userbody);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            HttpResponseMessage x = HttpClient.PutAsync(baseurl + "api/UserBodies?" + "id=" + id, content).Result;
            return x.StatusCode.ToString();
        }
        public string DeleteUserBody(string token, int id)
        {
            HttpResponseMessage x = HttpClient.DeleteAsync(baseurl + "api/UserBodies?" + "id=" + id + "&token=" + token).Result;
            return x.StatusCode.ToString();

        }


        //memorials
        public List<UserMemorials> GetUserMemosAsync(string token)
        {
            List<UserMemorials> usermemos = null;
            try
            {
                HttpResponseMessage x = HttpClient.GetAsync("https://yuh.ziqiang.net.cn/api/UserMemorials?token=" + token).Result;
                string tempdata = x.Content.ReadAsStringAsync().Result;
                usermemos = JsonConvert.DeserializeObject<List<UserMemorials>>(tempdata);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return usermemos;

        }
        public string PostUserMemosAsync(PostMemo postmemo)
        {

            string json = JsonConvert.SerializeObject(postmemo);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            HttpResponseMessage x = HttpClient.PostAsync(baseurl + "api/UserMemorials?", content).Result;
            return x.StatusCode.ToString();
        }
        public string DeleteUserMemosAsync(string token, int id)
        {
            HttpResponseMessage x = HttpClient.DeleteAsync(baseurl + "api/UserMemorials?" + "id=" + id + "&token=" + token).Result;
            return x.StatusCode.ToString();
        }
        public string PutUserMemosAsync(int userid, PutMemo putmemos)
        {

            string json = JsonConvert.SerializeObject(putmemos);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            HttpResponseMessage x = HttpClient.PutAsync(baseurl + "api/UserMemorials?" + "token=" + Api.Token, content).Result;
            return x.StatusCode.ToString();
        }
        //NoteStatistic
        public NotesStatistic GetNoteStatistic(string token, string period)
        {
            NotesStatistic analysis = null;
            try
            {
                HttpResponseMessage x = HttpClient.GetAsync("https://yuh.ziqiang.net.cn/api/UserNotes/statistics?token=" + token + "&period=" + period).Result;
                string tempdata = x.Content.ReadAsStringAsync().Result;
                analysis = JsonConvert.DeserializeObject<NotesStatistic>(tempdata);
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            //Console.WriteLine(analysis.ToString());
            return analysis;

        }






    }
}
