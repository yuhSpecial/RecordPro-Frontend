﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace UI_Scripts2
{
    public class UserGetBody
    {
        //        {
        //  "weight": 0,
        //  "shoulder": 0,
        //  "height": 0,
        //  "bust": 0,
        //  "waist": 0,
        //  "hips": 0,
        //  "dateTime": "2020-06-17T02:48:05.349Z",
        //  "token": "string"
        //}
        [JsonProperty("id")]
        public int id;

        [JsonProperty("weight")]
        public double weight;

        [JsonProperty("shoulder")]
        public double shoulder;

        [JsonProperty("height")]
        public double height;

        [JsonProperty("bust")]
        public double bust;

        [JsonProperty("waist")]
        public double waist;

        [JsonProperty("hips")]
        public double hips;

        [JsonProperty("dateTime")]
        public DateTime dateTime;

        [JsonProperty("token")]
        public string token;

        [JsonProperty("userid")]
        public int userid;


    }
    public class UserBody
    {
        [JsonProperty("weight")]
        public double weight;

        [JsonProperty("shoulder")]
        public double shoulder;

        [JsonProperty("height")]
        public double height;

        [JsonProperty("bust")]
        public double bust;

        [JsonProperty("waist")]
        public double waist;

        [JsonProperty("hips")]
        public double hips;

        [JsonProperty("dateTime")]
        public DateTime dateTime;

        [JsonProperty("token")]
        public string token;

        public UserBody(double weight, double shoulder, double height, double bust, double waist, double hips,DateTime dateTime,string token)
        {
            this.weight = weight;
            this.shoulder = shoulder;
            this.height = height;
            this.bust = bust;
            this.waist = waist;
            this.hips = hips;
            this.token = token;
            this.dateTime = dateTime;
        }
        public UserBody()
        {

        }
        public UserBody(UserGetBody body)
        {
            this.weight = body.weight;
            this.shoulder = body.shoulder;
            this.height = body.height;
            this.bust = body.bust;
            this.waist = body.waist;
            this.hips = body.hips;
            this.token = body.token;
            this.dateTime = body.dateTime;

        }
    }

}
