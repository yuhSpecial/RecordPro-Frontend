﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Scripts2
{
    public partial class NoteCell : UserControl
    {
        private Api api;
        public int NoteID;
        

        public NoteCell()
        {
            InitializeComponent();
            api = new Api();
            
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            UCinbox_notes panel = (UCinbox_notes)EditButton.Parent.Parent.Parent;
            panel.AddingNotePanel.Visible = true;
            panel.timer1.Start();
            panel.AddButton.Text = "UPDATE NOTE";
            panel.NoteID = this.NoteID;

            panel.NotesTextBox.Text = ContentTextBox.Text;
            panel.AmountLabel.Text = WordsAmountLabel.Text;
            panel.DateTimePicker.Value = Convert.ToDateTime(TimeLabel.Text);
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            UCinbox_notes panel = (UCinbox_notes)EditButton.Parent.Parent.Parent;
            string state = api.DeleteUserNotesAsync(Api.Token, NoteID);
            if (state == "OK")
            {
                MessageBox.Show("Deleting succeed");
                panel.WordsAmountShow();
                panel.SentiShow();
                panel.NotesShow();
            }
            else
                MessageBox.Show(state);
        }
    }
}
