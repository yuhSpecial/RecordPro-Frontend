﻿namespace UI_Scripts2
{
    partial class NoteCell
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NoteCell));
            this.TimeLabel = new System.Windows.Forms.Label();
            this.EditButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.DeleteButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.SentiLabel = new System.Windows.Forms.Label();
            this.WordsAmountLabel = new System.Windows.Forms.Label();
            this.TagLabel = new System.Windows.Forms.Label();
            this.ContentTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // TimeLabel
            // 
            this.TimeLabel.AutoSize = true;
            this.TimeLabel.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeLabel.Location = new System.Drawing.Point(14, 138);
            this.TimeLabel.Name = "TimeLabel";
            this.TimeLabel.Size = new System.Drawing.Size(65, 27);
            this.TimeLabel.TabIndex = 8;
            this.TimeLabel.Text = "14:43";
            // 
            // EditButton
            // 
            this.EditButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.EditButton.BorderThickness = 2;
            this.EditButton.CheckedState.Parent = this.EditButton;
            this.EditButton.CustomImages.Parent = this.EditButton;
            this.EditButton.FillColor = System.Drawing.Color.White;
            this.EditButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.EditButton.ForeColor = System.Drawing.Color.White;
            this.EditButton.HoverState.Parent = this.EditButton;
            this.EditButton.Image = ((System.Drawing.Image)(resources.GetObject("EditButton.Image")));
            this.EditButton.Location = new System.Drawing.Point(417, 19);
            this.EditButton.Name = "EditButton";
            this.EditButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.EditButton.ShadowDecoration.Parent = this.EditButton;
            this.EditButton.Size = new System.Drawing.Size(48, 48);
            this.EditButton.TabIndex = 6;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DeleteButton.BorderThickness = 2;
            this.DeleteButton.CheckedState.Parent = this.DeleteButton;
            this.DeleteButton.CustomImages.Parent = this.DeleteButton;
            this.DeleteButton.FillColor = System.Drawing.Color.White;
            this.DeleteButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.DeleteButton.ForeColor = System.Drawing.Color.White;
            this.DeleteButton.HoverState.Parent = this.DeleteButton;
            this.DeleteButton.Image = ((System.Drawing.Image)(resources.GetObject("DeleteButton.Image")));
            this.DeleteButton.Location = new System.Drawing.Point(417, 74);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.DeleteButton.ShadowDecoration.Parent = this.DeleteButton;
            this.DeleteButton.Size = new System.Drawing.Size(48, 48);
            this.DeleteButton.TabIndex = 4;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // SentiLabel
            // 
            this.SentiLabel.AutoSize = true;
            this.SentiLabel.Font = new System.Drawing.Font("方正粗黑宋简体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SentiLabel.ForeColor = System.Drawing.Color.MidnightBlue;
            this.SentiLabel.Location = new System.Drawing.Point(270, 164);
            this.SentiLabel.Name = "SentiLabel";
            this.SentiLabel.Size = new System.Drawing.Size(112, 25);
            this.SentiLabel.TabIndex = 10;
            this.SentiLabel.Text = "情感极性：";
            // 
            // WordsAmountLabel
            // 
            this.WordsAmountLabel.AutoSize = true;
            this.WordsAmountLabel.Font = new System.Drawing.Font("方正粗黑宋简体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.WordsAmountLabel.ForeColor = System.Drawing.Color.MidnightBlue;
            this.WordsAmountLabel.Location = new System.Drawing.Point(268, 139);
            this.WordsAmountLabel.Name = "WordsAmountLabel";
            this.WordsAmountLabel.Size = new System.Drawing.Size(72, 25);
            this.WordsAmountLabel.TabIndex = 11;
            this.WordsAmountLabel.Text = "字数：";
            // 
            // TagLabel
            // 
            this.TagLabel.AutoSize = true;
            this.TagLabel.Font = new System.Drawing.Font("方正粗黑宋简体", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TagLabel.ForeColor = System.Drawing.Color.MidnightBlue;
            this.TagLabel.Location = new System.Drawing.Point(15, 190);
            this.TagLabel.Name = "TagLabel";
            this.TagLabel.Size = new System.Drawing.Size(73, 20);
            this.TagLabel.TabIndex = 12;
            this.TagLabel.Text = "关键词：";
            // 
            // ContentTextBox
            // 
            this.ContentTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ContentTextBox.Location = new System.Drawing.Point(19, 19);
            this.ContentTextBox.Name = "ContentTextBox";
            this.ContentTextBox.ReadOnly = true;
            this.ContentTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.ContentTextBox.Size = new System.Drawing.Size(386, 117);
            this.ContentTextBox.TabIndex = 13;
            this.ContentTextBox.Text = "";
            // 
            // NoteCell
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ContentTextBox);
            this.Controls.Add(this.TagLabel);
            this.Controls.Add(this.WordsAmountLabel);
            this.Controls.Add(this.SentiLabel);
            this.Controls.Add(this.TimeLabel);
            this.Controls.Add(this.EditButton);
            this.Controls.Add(this.DeleteButton);
            this.Name = "NoteCell";
            this.Size = new System.Drawing.Size(483, 228);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2CircleButton EditButton;
        private Guna.UI2.WinForms.Guna2CircleButton DeleteButton;
        public System.Windows.Forms.Label TimeLabel;
        public System.Windows.Forms.Label SentiLabel;
        public System.Windows.Forms.Label WordsAmountLabel;
        public System.Windows.Forms.Label TagLabel;
        public System.Windows.Forms.RichTextBox ContentTextBox;
    }
}
