﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LiveCharts;
using LiveCharts.Wpf;
using LiveCharts.Helpers;

namespace UI_Scripts2
{
    public partial class FaceDaily : Form
    {

        private Api api;
        private List<UserFaces> faces { get; set; }
        private List<Being> Beings { get; set; }
        //定义一个bool变量标识是否拖动窗体
        private bool isMove = false;
        //记录鼠标的位置
        private Point point;
        private List<Double> health;
        private List<Double> emotion;
        private List<Double> yanquan;
        private List<DateTime> datetime;
        private List<Double> beauty;
        public FaceDaily(List<UserFaces> faces)
        {
            this.api = new Api();
            this.faces = faces;
            Beings = new List<Being>();
            health = new List<double>();
            emotion = new List<double>();
            yanquan = new List<double>();
            beauty = new List<double>();
            datetime = new List<DateTime>();
            foreach (UserFaces face in faces)
            {
                this.Beings.Add(new Being(face));
            }
            //for(int i=0;i<faces.Count;i++)
            //{
            //    this.Beings.Add(new Being(faces[i]));
            //}
            foreach(Being being in Beings)
            {
                health.Add(being.health);
                emotion.Add(being.emotion);
                yanquan.Add(being.yanquan);
                datetime.Add(being.datetime);
                beauty.Add(being.beauty);
            }
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void FaceDaily_Load(object sender, EventArgs e)
        {
            //Console.WriteLine(faces[0].age);
            //this.chart1.DataSource = Beings;
            //this.chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line; 
            //this.chart1.Series[0].YValueMembers = "emotion";
            //this.chart1.Series[1].YValueMembers = "health";
            //this.chart1.Series[2].YValueMembers = "beauty";
            //this.chart1.Series[3].YValueMembers = "yanquan";

            //this.chart1.Series[0].XValueMember = "datetime";
            //this.chart1.Series[1].XValueMember = "datetime";
            //this.chart1.Series[2].XValueMember = "datetime";
            //this.chart1.Series[3].XValueMember = "datetime";
            //chart1.DataBind();

            cartesianChart1.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(34, 46, 49));
            cartesianChart2.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(34, 46, 49));
            cartesianChart3.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(34, 46, 49));
            cartesianChart4.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(34, 46, 49));
            cartesianChart1.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title="情绪",
                    Values=this.emotion.AsChartValues(),
                    StrokeThickness = 1,
                    StrokeDashArray = new System.Windows.Media.DoubleCollection(20),
                    Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(107, 185, 69)),
                    Fill = System.Windows.Media.Brushes.Transparent,
                    LineSmoothness = 0,
                    PointGeometry = null
                },
            };

            cartesianChart2.Series = new SeriesCollection
            {

                new LineSeries
                {
                    Title="健康",
                    Values=this.health.AsChartValues(),
                    StrokeThickness = 2,
                Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(28, 142, 196)),
                Fill = System.Windows.Media.Brushes.Transparent,
                LineSmoothness = 1,
                PointGeometrySize = 15,
                //PointForeround = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(34, 46, 49))

                },
                };
            cartesianChart3.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title="黑眼圈",
                    Values=this.yanquan.AsChartValues(),
                    StrokeThickness = 3,
                    LineSmoothness = 1,
                    Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(251, 185, 69)),
                    Fill = System.Windows.Media.Brushes.Transparent,


                },
            };
            cartesianChart4.Series = new SeriesCollection
            {
                new LineSeries
            {
                Title = "美丽",
                Values = this.beauty.AsChartValues(),
                Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(24, 150, 79)),
                Fill = System.Windows.Media.Brushes.Transparent,


            },
             };














            
            Func < double,string> formatFunc =(x)=> string.Format("{0:D}", x);
            IList<string> transform(List<DateTime> datetime)
            {
                IList<string> list = new List<string>();

                foreach(DateTime time in datetime)
                {
                    list.Add(time.ToString("d"));
                }
                return list;
            }
            IList<string >Time = transform(datetime);
            cartesianChart1.AxisX.Add(new Axis
            {
                Title = "时间",
                Labels = Time,
                LabelFormatter= formatFunc,
            });
            cartesianChart2.AxisX.Add(new Axis
            {
                Title = "时间",
                Labels = Time,
            });
            cartesianChart3.AxisX.Add(new Axis
            {
                Title = "时间",
                Labels = Time,
            });
            cartesianChart4.AxisX.Add(new Axis
            {
                Title = "时间",
                Labels = Time,
            });
























        }


        private void FaceDaily_MouseDown_1(object sender, MouseEventArgs e)
        {
            isMove = true;
            //记录鼠标的位置
            point = e.Location;
        }

        private void FaceDaily_MouseMove_1(object sender, MouseEventArgs e)
        {
            if (isMove)
            {
                Point p = e.Location;
                int x = p.X - point.X;
                int y = p.Y - point.Y;
                //窗体的位置
                this.Location = new Point(this.Location.X + x, this.Location.Y + y);
            }

        }



        private void FaceDaily_MouseUp(object sender, MouseEventArgs e)
        {
            isMove = false;
        }

        private void label2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Analysis_Click(object sender, EventArgs e)
        {
            FaceStatistic analysis = api.GetUserFacesAnalysisAsync(Api.Token,"week",1);
            this.Close();
            Form FaceAnalysis =new FaceAnalysis(analysis);
            FaceAnalysis.Show();
        }
    }
}
