﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
//using System.Collections.Generic;
namespace UI_Scripts2
{
    public class FaceStatistic
    {
        [JsonProperty("beauty_ratio")]
        public float beauty_ratio;

        [JsonProperty("record_frequency")]
        public float record_frequency;

        [JsonProperty("smile_ratio")]
        public float smile_ratio;

        [JsonProperty("emotion_mode")]
        public List<Emotion> emotion_mode;

        [JsonProperty("black_eye_ratio")]
        public black_eye black_eye_ratio;

        [JsonProperty("skin_health_ratio")]
        public skin skin_health_ratio;

        public override string ToString()
        {
            return beauty_ratio.ToString()+ record_frequency.ToString();
        }
    }
    public class Emotion
    {
        [JsonProperty("key")]
        public string key;

        [JsonProperty("count")]
        public int count;
    }
    public class skin
    {
        [JsonProperty("healthy")]
        public Health healthy;

        [JsonProperty("basically_healthy")]
        public basicHealth basically_healthy;

        [JsonProperty("unhealthy")]
        public Unhealth unhealthy;
    }
    public class black_eye
    {

        [JsonProperty("healthy")]
        public Health healthy;

        [JsonProperty("basically_healthy")]
        public basicHealth basically_healthy;

        [JsonProperty("unhealthy")]
        public Unhealth unhealthy;

    }
    public class Health
    {
        [JsonProperty("ratio")]
        public float ratio;

    }
    public class basicHealth
    {
        [JsonProperty("ratio")]
        public float ratio;
    }
    public class Unhealth
    {
        [JsonProperty("ratio")]
        public float ratio;
    }
}
