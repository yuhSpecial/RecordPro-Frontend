﻿namespace UI_Scripts2
{
    partial class UserSignIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.NameTxtField = new Guna.UI2.WinForms.Guna2TextBox();
            this.PswdTxtField = new Guna.UI2.WinForms.Guna2TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SignInButton = new Guna.UI2.WinForms.Guna2TileButton();
            this.SignUpButton = new Guna.UI2.WinForms.Guna2TileButton();
            this.pieChart1 = new LiveCharts.WinForms.PieChart();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.CloseButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 30;
            this.guna2Elipse1.TargetControl = this;
            // 
            // NameTxtField
            // 
            this.NameTxtField.BorderRadius = 10;
            this.NameTxtField.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.NameTxtField.DefaultText = "";
            this.NameTxtField.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.NameTxtField.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.NameTxtField.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.NameTxtField.DisabledState.Parent = this.NameTxtField;
            this.NameTxtField.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.NameTxtField.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.NameTxtField.FocusedState.Parent = this.NameTxtField;
            this.NameTxtField.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.NameTxtField.ForeColor = System.Drawing.Color.Black;
            this.NameTxtField.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.NameTxtField.HoverState.Parent = this.NameTxtField;
            this.NameTxtField.Location = new System.Drawing.Point(272, 203);
            this.NameTxtField.Margin = new System.Windows.Forms.Padding(4);
            this.NameTxtField.Name = "NameTxtField";
            this.NameTxtField.PasswordChar = '\0';
            this.NameTxtField.PlaceholderText = "";
            this.NameTxtField.SelectedText = "";
            this.NameTxtField.ShadowDecoration.Parent = this.NameTxtField;
            this.NameTxtField.Size = new System.Drawing.Size(267, 42);
            this.NameTxtField.TabIndex = 0;
            // 
            // PswdTxtField
            // 
            this.PswdTxtField.BorderRadius = 10;
            this.PswdTxtField.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.PswdTxtField.DefaultText = "";
            this.PswdTxtField.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.PswdTxtField.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.PswdTxtField.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.PswdTxtField.DisabledState.Parent = this.PswdTxtField;
            this.PswdTxtField.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.PswdTxtField.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.PswdTxtField.FocusedState.Parent = this.PswdTxtField;
            this.PswdTxtField.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.PswdTxtField.ForeColor = System.Drawing.Color.Black;
            this.PswdTxtField.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.PswdTxtField.HoverState.Parent = this.PswdTxtField;
            this.PswdTxtField.Location = new System.Drawing.Point(272, 302);
            this.PswdTxtField.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PswdTxtField.Name = "PswdTxtField";
            this.PswdTxtField.PasswordChar = '*';
            this.PswdTxtField.PlaceholderText = "";
            this.PswdTxtField.SelectedText = "";
            this.PswdTxtField.ShadowDecoration.Parent = this.PswdTxtField;
            this.PswdTxtField.Size = new System.Drawing.Size(267, 42);
            this.PswdTxtField.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(268, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 29);
            this.label1.TabIndex = 2;
            this.label1.Text = "用户名";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("方正粗黑宋简体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(268, 266);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 29);
            this.label2.TabIndex = 3;
            this.label2.Text = "密码";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // SignInButton
            // 
            this.SignInButton.BorderRadius = 10;
            this.SignInButton.CheckedState.Parent = this.SignInButton;
            this.SignInButton.CustomImages.Parent = this.SignInButton;
            this.SignInButton.FillColor = System.Drawing.Color.White;
            this.SignInButton.Font = new System.Drawing.Font("JetBrains Mono Medium", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignInButton.ForeColor = System.Drawing.Color.MidnightBlue;
            this.SignInButton.HoverState.Parent = this.SignInButton;
            this.SignInButton.Location = new System.Drawing.Point(220, 433);
            this.SignInButton.Name = "SignInButton";
            this.SignInButton.ShadowDecoration.Parent = this.SignInButton;
            this.SignInButton.Size = new System.Drawing.Size(153, 65);
            this.SignInButton.TabIndex = 4;
            this.SignInButton.Text = "Sign In";
            this.SignInButton.Click += new System.EventHandler(this.SignInButton_Click);
            // 
            // SignUpButton
            // 
            this.SignUpButton.BorderRadius = 10;
            this.SignUpButton.CheckedState.Parent = this.SignUpButton;
            this.SignUpButton.CustomImages.Parent = this.SignUpButton;
            this.SignUpButton.FillColor = System.Drawing.Color.White;
            this.SignUpButton.Font = new System.Drawing.Font("JetBrains Mono Medium", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignUpButton.ForeColor = System.Drawing.Color.MidnightBlue;
            this.SignUpButton.HoverState.Parent = this.SignUpButton;
            this.SignUpButton.Location = new System.Drawing.Point(449, 433);
            this.SignUpButton.Name = "SignUpButton";
            this.SignUpButton.ShadowDecoration.Parent = this.SignUpButton;
            this.SignUpButton.Size = new System.Drawing.Size(153, 65);
            this.SignUpButton.TabIndex = 5;
            this.SignUpButton.Text = "Sign Up";
            this.SignUpButton.Click += new System.EventHandler(this.SignUpButton_Click);
            // 
            // pieChart1
            // 
            this.pieChart1.Location = new System.Drawing.Point(800, 186);
            this.pieChart1.Name = "pieChart1";
            this.pieChart1.Size = new System.Drawing.Size(200, 99);
            this.pieChart1.TabIndex = 6;
            this.pieChart1.Text = "pieChart1";
            this.pieChart1.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 30;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // CloseButton
            // 
            this.CloseButton.CheckedState.Parent = this.CloseButton;
            this.CloseButton.CustomImages.Parent = this.CloseButton;
            this.CloseButton.FillColor = System.Drawing.Color.MidnightBlue;
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.CloseButton.ForeColor = System.Drawing.Color.White;
            this.CloseButton.HoverState.Parent = this.CloseButton;
            this.CloseButton.Location = new System.Drawing.Point(776, 13);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.CloseButton.ShadowDecoration.Parent = this.CloseButton;
            this.CloseButton.Size = new System.Drawing.Size(33, 22);
            this.CloseButton.TabIndex = 7;
            this.CloseButton.Text = "×";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("JetBrains Mono Medium", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(273, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(266, 54);
            this.label3.TabIndex = 8;
            this.label3.Text = "RecordPRO";
            // 
            // UserSignIn
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.ClientSize = new System.Drawing.Size(821, 554);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.pieChart1);
            this.Controls.Add(this.SignUpButton);
            this.Controls.Add(this.SignInButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PswdTxtField);
            this.Controls.Add(this.NameTxtField);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(821, 554);
            this.Name = "UserSignIn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UserSignIn";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.UserSignIn_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.UserSignIn_MouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2TileButton SignUpButton;
        private Guna.UI2.WinForms.Guna2TileButton SignInButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2TextBox PswdTxtField;
        private Guna.UI2.WinForms.Guna2TextBox NameTxtField;
        private LiveCharts.WinForms.PieChart pieChart1;
        private System.Windows.Forms.Timer timer1;
        private Guna.UI2.WinForms.Guna2CircleButton CloseButton;
        private System.Windows.Forms.Label label3;
    }
}