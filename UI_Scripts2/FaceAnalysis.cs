﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Scripts2
{

    public partial class FaceAnalysis : Form
    {
        private FaceStatistic analysis { get; set; }
        private bool isMove = false;
        //记录鼠标的位置
        private Point point;
        public FaceAnalysis(FaceStatistic analysis)
        {
            this.analysis = analysis;
            InitializeComponent();
            FillText(analysis);
        }
        private void FillText(FaceStatistic analysis)
        {
            string[] emotion = { "anger","surprise", "anger", "neutral" };
            string Text1, Text2, Text3,Text4,Text5;
            if (analysis.record_frequency>=1)
            {
                Text1 = "你是一个自信大方的人，喜欢每天记录下自己的脸庞，分析每日的健康状态。\n";
            }
            else
            {
                Text1 = "首先，Record Pro建议你每天坚持自拍留下自己的照片噢，这样的话我们就可以根据您的照片更加精准地分析你的状态拉。";
            }
            if(analysis.beauty_ratio<0.6)
            {
                Text2 = "您是属于少数的仪貌大方者，长相精致又不失去特色。";
            }
            else
            {
                Text2 = "可以从您的肤质，五官张力看出来，您目前的身体状态可能并不是很好，您需要更加认真地看我们接下来的分析\n。";

            }
            if(analysis.emotion_mode[0].key!=emotion[3])
            {
                Text3 = "您最近的表情分析显示，这一周您的情绪状态比较多样化，其中{analysis.emotion_mode[0].key}共出现了{analysis.emotion_mode[0].count}次。\n";
            }
            else
            {
                Text3 = "您近一周自拍时心态平和，表情温柔的时候更多，说明您最近的心情不错，这是一件好事，您的皮肤在这种情况下会更加具有活力，并且更加容易缓解衰老。\n";
            }
            if(analysis.skin_health_ratio.basically_healthy.ratio>0.8)
            {
                Text4 = "您最近的皮肤状态不错，属于人群前列，肤色轻华有光泽，整个人看起来非常的具有活力并且精神.\n";
            }
            else
            {
                Text4 = "您最近的皮肤状态可能不是很健康，脸上可能存在了不少的痘印黑头等杂质，这里我们希望您能更加爱惜您的皮肤，提升您的肤质有益于找一个好配偶2333\n";

            }
            if(analysis.black_eye_ratio.basically_healthy.ratio>0.8)
            {
                Text5 = "看来最近您没有怎么熬夜，这对您的皮肤保养来说大有益处,优秀的作息是保护好自己皮肤最简单也最有效的措施。希望您能保持下去，当然您也不能忘记了其他的皮肤护理噢。\n";

            }
            else
            {
                Text5 = "看来您最近熬夜不少，黑眼圈已经比较明显了，尽管最近不论是学习压力还是生活压力都比较大，但是我们还是衷心的希望您能够保持健康作息，爱惜好自己的身体，把要做的事情于白天解决掉，留一个没有烦恼的夜晚。";

            }

            this.ContentTextBox.Text = Text1 + Text2 + Text3+"\n\n"+Text4+Text5;
        }

        private void FaceAnalysis_MouseDown(object sender, MouseEventArgs e)
        {
            isMove = true;
            //记录鼠标的位置
            point = e.Location;
        }

        private void FaceAnalysis_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMove)
            {
                Point p = e.Location;
                int x = p.X - point.X;
                int y = p.Y - point.Y;
                //窗体的位置
                this.Location = new Point(this.Location.X + x, this.Location.Y + y);
            }
        }

        private void FaceAnalysis_MouseUp(object sender, MouseEventArgs e)
        {
            isMove = false;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
