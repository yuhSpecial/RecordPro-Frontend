﻿namespace UI_Scripts2
{
    partial class UCinbox_account
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.AddAccountButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.AccountTable = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.DaysAmount = new Guna.UI2.WinForms.Guna2NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.AnalysisPanel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.xuiFlatTab1 = new XanderUI.XUIFlatTab();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.guna2HtmlLabel2 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel1 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.SumIncome = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.SumExpense = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pieChart2 = new LiveCharts.WinForms.PieChart();
            this.pieChart1 = new LiveCharts.WinForms.PieChart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cartesianChart1 = new LiveCharts.WinForms.CartesianChart();
            this.AddingPanel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.AddButton = new Guna.UI2.WinForms.Guna2Button();
            this.AmountTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.CategoryPanel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.ExpendCateGroup = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Dining = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.Transport = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.Daily = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.Entertainment = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.Medicine = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.Clothing = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.RevenueChosen = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.RevChosen = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.ExpendChosen = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.DateTimePicker = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.RemarkTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.RevenueCateGroup = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Investment = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.Salaries = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.Living = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.Gifts = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.label17 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.showBills = new Guna.UI2.WinForms.Guna2Button();
            this.xuiWifiPercentageAPI1 = new XanderUI.XUIWifiPercentageAPI();
            this.CloseButton = new Guna.UI2.WinForms.Guna2CircleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DaysAmount)).BeginInit();
            this.AnalysisPanel.SuspendLayout();
            this.xuiFlatTab1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.AddingPanel.SuspendLayout();
            this.CategoryPanel.SuspendLayout();
            this.ExpendCateGroup.SuspendLayout();
            this.RevenueCateGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 30;
            this.guna2Elipse1.TargetControl = this;
            // 
            // AddAccountButton
            // 
            this.AddAccountButton.BackColor = System.Drawing.Color.Transparent;
            this.AddAccountButton.CheckedState.Parent = this.AddAccountButton;
            this.AddAccountButton.CustomImages.Parent = this.AddAccountButton;
            this.AddAccountButton.FillColor = System.Drawing.Color.MidnightBlue;
            this.AddAccountButton.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddAccountButton.ForeColor = System.Drawing.Color.White;
            this.AddAccountButton.HoverState.Parent = this.AddAccountButton;
            this.AddAccountButton.Location = new System.Drawing.Point(0, 568);
            this.AddAccountButton.Name = "AddAccountButton";
            this.AddAccountButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.AddAccountButton.ShadowDecoration.Parent = this.AddAccountButton;
            this.AddAccountButton.Size = new System.Drawing.Size(70, 70);
            this.AddAccountButton.TabIndex = 22;
            this.AddAccountButton.Text = "+";
            this.AddAccountButton.TextOffset = new System.Drawing.Point(0, -2);
            this.AddAccountButton.Click += new System.EventHandler(this.AddAccountButton_Click);
            // 
            // AccountTable
            // 
            this.AccountTable.AutoScroll = true;
            this.AccountTable.BackColor = System.Drawing.Color.Transparent;
            this.AccountTable.EdgeWidth = 1;
            this.AccountTable.FillColor = System.Drawing.Color.White;
            this.AccountTable.Location = new System.Drawing.Point(7, 86);
            this.AccountTable.Name = "AccountTable";
            this.AccountTable.Radius = 20;
            this.AccountTable.ShadowColor = System.Drawing.Color.MidnightBlue;
            this.AccountTable.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.AccountTable.Size = new System.Drawing.Size(403, 483);
            this.AccountTable.TabIndex = 46;
            // 
            // DaysAmount
            // 
            this.DaysAmount.BackColor = System.Drawing.Color.Transparent;
            this.DaysAmount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.DaysAmount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.DaysAmount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.DaysAmount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.DaysAmount.DisabledState.Parent = this.DaysAmount;
            this.DaysAmount.DisabledState.UpDownButtonFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(177)))), ((int)(((byte)(177)))));
            this.DaysAmount.DisabledState.UpDownButtonForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(203)))), ((int)(((byte)(203)))));
            this.DaysAmount.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.DaysAmount.FocusedState.Parent = this.DaysAmount;
            this.DaysAmount.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DaysAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.DaysAmount.Location = new System.Drawing.Point(58, 17);
            this.DaysAmount.Name = "DaysAmount";
            this.DaysAmount.ShadowDecoration.Parent = this.DaysAmount;
            this.DaysAmount.Size = new System.Drawing.Size(65, 30);
            this.DaysAmount.TabIndex = 47;
            this.DaysAmount.UpDownButtonFillColor = System.Drawing.Color.MidnightBlue;
            this.DaysAmount.UpDownButtonForeColor = System.Drawing.Color.White;
            this.DaysAmount.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 29);
            this.label1.TabIndex = 48;
            this.label1.Text = "Last";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(129, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 29);
            this.label2.TabIndex = 49;
            this.label2.Text = "Days";
            // 
            // AnalysisPanel
            // 
            this.AnalysisPanel.BackColor = System.Drawing.Color.Transparent;
            this.AnalysisPanel.Controls.Add(this.xuiFlatTab1);
            this.AnalysisPanel.FillColor = System.Drawing.Color.White;
            this.AnalysisPanel.Location = new System.Drawing.Point(433, 66);
            this.AnalysisPanel.Name = "AnalysisPanel";
            this.AnalysisPanel.Radius = 10;
            this.AnalysisPanel.ShadowColor = System.Drawing.Color.Black;
            this.AnalysisPanel.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.AnalysisPanel.Size = new System.Drawing.Size(566, 572);
            this.AnalysisPanel.TabIndex = 51;
            // 
            // xuiFlatTab1
            // 
            this.xuiFlatTab1.ActiveHeaderColor = System.Drawing.Color.MidnightBlue;
            this.xuiFlatTab1.ActiveTextColor = System.Drawing.Color.White;
            this.xuiFlatTab1.BorderColor = System.Drawing.Color.MidnightBlue;
            this.xuiFlatTab1.Controls.Add(this.tabPage1);
            this.xuiFlatTab1.Controls.Add(this.tabPage2);
            this.xuiFlatTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xuiFlatTab1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.xuiFlatTab1.HeaderBackgroundColor = System.Drawing.Color.White;
            this.xuiFlatTab1.InActiveHeaderColor = System.Drawing.Color.White;
            this.xuiFlatTab1.InActiveTextColor = System.Drawing.Color.MidnightBlue;
            this.xuiFlatTab1.ItemSize = new System.Drawing.Size(240, 16);
            this.xuiFlatTab1.Location = new System.Drawing.Point(0, 0);
            this.xuiFlatTab1.Name = "xuiFlatTab1";
            this.xuiFlatTab1.PageColor = System.Drawing.Color.White;
            this.xuiFlatTab1.SelectedIndex = 0;
            this.xuiFlatTab1.ShowBorder = false;
            this.xuiFlatTab1.Size = new System.Drawing.Size(566, 572);
            this.xuiFlatTab1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.guna2HtmlLabel2);
            this.tabPage1.Controls.Add(this.guna2HtmlLabel1);
            this.tabPage1.Controls.Add(this.SumIncome);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.SumExpense);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.pieChart2);
            this.tabPage1.Controls.Add(this.pieChart1);
            this.tabPage1.Font = new System.Drawing.Font("等线", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 20);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(558, 548);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "饼状图";
            // 
            // guna2HtmlLabel2
            // 
            this.guna2HtmlLabel2.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel2.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.guna2HtmlLabel2.Location = new System.Drawing.Point(366, 449);
            this.guna2HtmlLabel2.Name = "guna2HtmlLabel2";
            this.guna2HtmlLabel2.Size = new System.Drawing.Size(31, 38);
            this.guna2HtmlLabel2.TabIndex = 9;
            this.guna2HtmlLabel2.Text = "￥";
            // 
            // guna2HtmlLabel1
            // 
            this.guna2HtmlLabel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel1.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.guna2HtmlLabel1.Location = new System.Drawing.Point(93, 449);
            this.guna2HtmlLabel1.Name = "guna2HtmlLabel1";
            this.guna2HtmlLabel1.Size = new System.Drawing.Size(31, 38);
            this.guna2HtmlLabel1.TabIndex = 8;
            this.guna2HtmlLabel1.Text = "￥";
            // 
            // SumIncome
            // 
            this.SumIncome.BackColor = System.Drawing.Color.Transparent;
            this.SumIncome.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SumIncome.Location = new System.Drawing.Point(415, 449);
            this.SumIncome.Name = "SumIncome";
            this.SumIncome.Size = new System.Drawing.Size(19, 38);
            this.SumIncome.TabIndex = 7;
            this.SumIncome.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("方正粗黑宋简体", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(361, 398);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(122, 26);
            this.label15.TabIndex = 6;
            this.label15.Text = "周期总收入";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("方正粗黑宋简体", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(88, 398);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(122, 26);
            this.label14.TabIndex = 5;
            this.label14.Text = "周期总支出";
            // 
            // SumExpense
            // 
            this.SumExpense.BackColor = System.Drawing.Color.Transparent;
            this.SumExpense.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SumExpense.Location = new System.Drawing.Point(137, 449);
            this.SumExpense.Name = "SumExpense";
            this.SumExpense.Size = new System.Drawing.Size(19, 38);
            this.SumExpense.TabIndex = 4;
            this.SumExpense.Text = "0";
            this.SumExpense.Click += new System.EventHandler(this.guna2HtmlLabel1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("方正粗黑宋简体", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(385, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 26);
            this.label4.TabIndex = 3;
            this.label4.Text = "收入";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("方正粗黑宋简体", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(116, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "支出";
            // 
            // pieChart2
            // 
            this.pieChart2.Location = new System.Drawing.Point(279, 3);
            this.pieChart2.Name = "pieChart2";
            this.pieChart2.Size = new System.Drawing.Size(265, 440);
            this.pieChart2.TabIndex = 1;
            this.pieChart2.Text = "pieChart2";
            // 
            // pieChart1
            // 
            this.pieChart1.Location = new System.Drawing.Point(6, 3);
            this.pieChart1.Name = "pieChart1";
            this.pieChart1.Size = new System.Drawing.Size(265, 440);
            this.pieChart1.TabIndex = 0;
            this.pieChart1.Text = "pieChart1";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.cartesianChart1);
            this.tabPage2.Font = new System.Drawing.Font("等线", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 20);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(558, 548);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "条形图";
            // 
            // cartesianChart1
            // 
            this.cartesianChart1.Dock = System.Windows.Forms.DockStyle.Top;
            this.cartesianChart1.Location = new System.Drawing.Point(3, 3);
            this.cartesianChart1.Name = "cartesianChart1";
            this.cartesianChart1.Size = new System.Drawing.Size(552, 440);
            this.cartesianChart1.TabIndex = 0;
            this.cartesianChart1.Text = "cartesianChart1";
            // 
            // AddingPanel
            // 
            this.AddingPanel.BackColor = System.Drawing.Color.Transparent;
            this.AddingPanel.Controls.Add(this.AddButton);
            this.AddingPanel.Controls.Add(this.AmountTextBox);
            this.AddingPanel.Controls.Add(this.CategoryPanel);
            this.AddingPanel.Controls.Add(this.RevenueChosen);
            this.AddingPanel.Controls.Add(this.label5);
            this.AddingPanel.Controls.Add(this.RevChosen);
            this.AddingPanel.Controls.Add(this.ExpendChosen);
            this.AddingPanel.Controls.Add(this.DateTimePicker);
            this.AddingPanel.Controls.Add(this.RemarkTextBox);
            this.AddingPanel.FillColor = System.Drawing.Color.MidnightBlue;
            this.AddingPanel.Location = new System.Drawing.Point(422, 644);
            this.AddingPanel.Name = "AddingPanel";
            this.AddingPanel.Radius = 10;
            this.AddingPanel.ShadowColor = System.Drawing.Color.Black;
            this.AddingPanel.ShadowDepth = 0;
            this.AddingPanel.Size = new System.Drawing.Size(337, 621);
            this.AddingPanel.TabIndex = 52;
            // 
            // AddButton
            // 
            this.AddButton.BorderRadius = 10;
            this.AddButton.CheckedState.Parent = this.AddButton;
            this.AddButton.CustomImages.Parent = this.AddButton;
            this.AddButton.FillColor = System.Drawing.Color.White;
            this.AddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.AddButton.ForeColor = System.Drawing.Color.MidnightBlue;
            this.AddButton.HoverState.Parent = this.AddButton;
            this.AddButton.Location = new System.Drawing.Point(37, 532);
            this.AddButton.Name = "AddButton";
            this.AddButton.ShadowDecoration.Parent = this.AddButton;
            this.AddButton.Size = new System.Drawing.Size(268, 55);
            this.AddButton.TabIndex = 61;
            this.AddButton.Text = "ADD";
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // AmountTextBox
            // 
            this.AmountTextBox.BackColor = System.Drawing.Color.Transparent;
            this.AmountTextBox.BorderRadius = 10;
            this.AmountTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.AmountTextBox.DefaultText = "";
            this.AmountTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.AmountTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.AmountTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.AmountTextBox.DisabledState.Parent = this.AmountTextBox;
            this.AmountTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.AmountTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.AmountTextBox.FocusedState.Parent = this.AmountTextBox;
            this.AmountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AmountTextBox.ForeColor = System.Drawing.Color.Black;
            this.AmountTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.AmountTextBox.HoverState.Parent = this.AmountTextBox;
            this.AmountTextBox.Location = new System.Drawing.Point(235, 455);
            this.AmountTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.AmountTextBox.Name = "AmountTextBox";
            this.AmountTextBox.PasswordChar = '\0';
            this.AmountTextBox.PlaceholderText = "￥";
            this.AmountTextBox.SelectedText = "";
            this.AmountTextBox.ShadowDecoration.Parent = this.AmountTextBox;
            this.AmountTextBox.Size = new System.Drawing.Size(70, 56);
            this.AmountTextBox.TabIndex = 60;
            // 
            // CategoryPanel
            // 
            this.CategoryPanel.BackColor = System.Drawing.Color.Transparent;
            this.CategoryPanel.Controls.Add(this.ExpendCateGroup);
            this.CategoryPanel.FillColor = System.Drawing.Color.White;
            this.CategoryPanel.Location = new System.Drawing.Point(35, 125);
            this.CategoryPanel.Name = "CategoryPanel";
            this.CategoryPanel.Radius = 10;
            this.CategoryPanel.ShadowColor = System.Drawing.Color.Black;
            this.CategoryPanel.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.CategoryPanel.Size = new System.Drawing.Size(270, 310);
            this.CategoryPanel.TabIndex = 59;
            // 
            // ExpendCateGroup
            // 
            this.ExpendCateGroup.BackColor = System.Drawing.Color.White;
            this.ExpendCateGroup.Controls.Add(this.label11);
            this.ExpendCateGroup.Controls.Add(this.label7);
            this.ExpendCateGroup.Controls.Add(this.label10);
            this.ExpendCateGroup.Controls.Add(this.Dining);
            this.ExpendCateGroup.Controls.Add(this.label9);
            this.ExpendCateGroup.Controls.Add(this.Transport);
            this.ExpendCateGroup.Controls.Add(this.label8);
            this.ExpendCateGroup.Controls.Add(this.Daily);
            this.ExpendCateGroup.Controls.Add(this.Entertainment);
            this.ExpendCateGroup.Controls.Add(this.label6);
            this.ExpendCateGroup.Controls.Add(this.Medicine);
            this.ExpendCateGroup.Controls.Add(this.Clothing);
            this.ExpendCateGroup.Location = new System.Drawing.Point(18, 17);
            this.ExpendCateGroup.Name = "ExpendCateGroup";
            this.ExpendCateGroup.Size = new System.Drawing.Size(228, 270);
            this.ExpendCateGroup.TabIndex = 68;
            this.ExpendCateGroup.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label11.Location = new System.Drawing.Point(56, 222);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 22);
            this.label11.TabIndex = 67;
            this.label11.Text = "Clothing";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label7.Location = new System.Drawing.Point(56, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 22);
            this.label7.TabIndex = 63;
            this.label7.Text = "Transport";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label10.Location = new System.Drawing.Point(56, 180);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 22);
            this.label10.TabIndex = 66;
            this.label10.Text = "Medicine";
            // 
            // Dining
            // 
            this.Dining.BackColor = System.Drawing.Color.Transparent;
            this.Dining.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Dining.CheckedState.BorderThickness = 0;
            this.Dining.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Dining.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Dining.CheckedState.Parent = this.Dining;
            this.Dining.Location = new System.Drawing.Point(15, 21);
            this.Dining.Name = "Dining";
            this.Dining.ShadowDecoration.Parent = this.Dining;
            this.Dining.Size = new System.Drawing.Size(20, 20);
            this.Dining.TabIndex = 56;
            this.Dining.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Dining.UncheckedState.BorderThickness = 2;
            this.Dining.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Dining.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Dining.UncheckedState.Parent = this.Dining;
            this.Dining.CheckedChanged += new System.EventHandler(this.Dining_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label9.Location = new System.Drawing.Point(56, 137);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 22);
            this.label9.TabIndex = 65;
            this.label9.Text = "Entertainment";
            // 
            // Transport
            // 
            this.Transport.BackColor = System.Drawing.Color.Transparent;
            this.Transport.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Transport.CheckedState.BorderThickness = 0;
            this.Transport.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Transport.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Transport.CheckedState.Parent = this.Transport;
            this.Transport.Location = new System.Drawing.Point(15, 59);
            this.Transport.Name = "Transport";
            this.Transport.ShadowDecoration.Parent = this.Transport;
            this.Transport.Size = new System.Drawing.Size(20, 20);
            this.Transport.TabIndex = 57;
            this.Transport.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Transport.UncheckedState.BorderThickness = 2;
            this.Transport.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Transport.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Transport.UncheckedState.Parent = this.Transport;
            this.Transport.CheckedChanged += new System.EventHandler(this.Transport_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label8.Location = new System.Drawing.Point(56, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 22);
            this.label8.TabIndex = 64;
            this.label8.Text = "Daily";
            // 
            // Daily
            // 
            this.Daily.BackColor = System.Drawing.Color.Transparent;
            this.Daily.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Daily.CheckedState.BorderThickness = 0;
            this.Daily.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Daily.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Daily.CheckedState.Parent = this.Daily;
            this.Daily.Location = new System.Drawing.Point(15, 97);
            this.Daily.Name = "Daily";
            this.Daily.ShadowDecoration.Parent = this.Daily;
            this.Daily.Size = new System.Drawing.Size(20, 20);
            this.Daily.TabIndex = 58;
            this.Daily.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Daily.UncheckedState.BorderThickness = 2;
            this.Daily.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Daily.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Daily.UncheckedState.Parent = this.Daily;
            this.Daily.CheckedChanged += new System.EventHandler(this.Daily_CheckedChanged);
            // 
            // Entertainment
            // 
            this.Entertainment.BackColor = System.Drawing.Color.Transparent;
            this.Entertainment.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Entertainment.CheckedState.BorderThickness = 0;
            this.Entertainment.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Entertainment.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Entertainment.CheckedState.Parent = this.Entertainment;
            this.Entertainment.Location = new System.Drawing.Point(15, 137);
            this.Entertainment.Name = "Entertainment";
            this.Entertainment.ShadowDecoration.Parent = this.Entertainment;
            this.Entertainment.Size = new System.Drawing.Size(20, 20);
            this.Entertainment.TabIndex = 59;
            this.Entertainment.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Entertainment.UncheckedState.BorderThickness = 2;
            this.Entertainment.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Entertainment.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Entertainment.UncheckedState.Parent = this.Entertainment;
            this.Entertainment.CheckedChanged += new System.EventHandler(this.Entertainment_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label6.Location = new System.Drawing.Point(56, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 22);
            this.label6.TabIndex = 62;
            this.label6.Text = "Dining";
            // 
            // Medicine
            // 
            this.Medicine.BackColor = System.Drawing.Color.Transparent;
            this.Medicine.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Medicine.CheckedState.BorderThickness = 0;
            this.Medicine.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Medicine.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Medicine.CheckedState.Parent = this.Medicine;
            this.Medicine.Location = new System.Drawing.Point(15, 180);
            this.Medicine.Name = "Medicine";
            this.Medicine.ShadowDecoration.Parent = this.Medicine;
            this.Medicine.Size = new System.Drawing.Size(20, 20);
            this.Medicine.TabIndex = 60;
            this.Medicine.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Medicine.UncheckedState.BorderThickness = 2;
            this.Medicine.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Medicine.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Medicine.UncheckedState.Parent = this.Medicine;
            this.Medicine.CheckedChanged += new System.EventHandler(this.Medicine_CheckedChanged);
            // 
            // Clothing
            // 
            this.Clothing.BackColor = System.Drawing.Color.Transparent;
            this.Clothing.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Clothing.CheckedState.BorderThickness = 0;
            this.Clothing.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Clothing.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Clothing.CheckedState.Parent = this.Clothing;
            this.Clothing.Location = new System.Drawing.Point(15, 222);
            this.Clothing.Name = "Clothing";
            this.Clothing.ShadowDecoration.Parent = this.Clothing;
            this.Clothing.Size = new System.Drawing.Size(20, 20);
            this.Clothing.TabIndex = 61;
            this.Clothing.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Clothing.UncheckedState.BorderThickness = 2;
            this.Clothing.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Clothing.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Clothing.UncheckedState.Parent = this.Clothing;
            this.Clothing.CheckedChanged += new System.EventHandler(this.Clothing_CheckedChanged);
            // 
            // RevenueChosen
            // 
            this.RevenueChosen.AutoSize = true;
            this.RevenueChosen.BackColor = System.Drawing.Color.Transparent;
            this.RevenueChosen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.RevenueChosen.ForeColor = System.Drawing.Color.White;
            this.RevenueChosen.Location = new System.Drawing.Point(231, 87);
            this.RevenueChosen.Name = "RevenueChosen";
            this.RevenueChosen.Size = new System.Drawing.Size(82, 22);
            this.RevenueChosen.TabIndex = 58;
            this.RevenueChosen.Text = "Revenue";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(61, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 22);
            this.label5.TabIndex = 57;
            this.label5.Text = "Expenditure";
            // 
            // RevChosen
            // 
            this.RevChosen.BackColor = System.Drawing.Color.Transparent;
            this.RevChosen.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RevChosen.CheckedState.BorderThickness = 0;
            this.RevChosen.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RevChosen.CheckedState.InnerColor = System.Drawing.Color.White;
            this.RevChosen.CheckedState.Parent = this.RevChosen;
            this.RevChosen.Location = new System.Drawing.Point(207, 87);
            this.RevChosen.Name = "RevChosen";
            this.RevChosen.ShadowDecoration.Parent = this.RevChosen;
            this.RevChosen.Size = new System.Drawing.Size(20, 20);
            this.RevChosen.TabIndex = 56;
            this.RevChosen.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.RevChosen.UncheckedState.BorderThickness = 2;
            this.RevChosen.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.RevChosen.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.RevChosen.UncheckedState.Parent = this.RevChosen;
            // 
            // ExpendChosen
            // 
            this.ExpendChosen.BackColor = System.Drawing.Color.Transparent;
            this.ExpendChosen.Checked = true;
            this.ExpendChosen.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.ExpendChosen.CheckedState.BorderThickness = 0;
            this.ExpendChosen.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.ExpendChosen.CheckedState.InnerColor = System.Drawing.Color.White;
            this.ExpendChosen.CheckedState.Parent = this.ExpendChosen;
            this.ExpendChosen.Location = new System.Drawing.Point(37, 87);
            this.ExpendChosen.Name = "ExpendChosen";
            this.ExpendChosen.ShadowDecoration.Parent = this.ExpendChosen;
            this.ExpendChosen.Size = new System.Drawing.Size(20, 20);
            this.ExpendChosen.TabIndex = 55;
            this.ExpendChosen.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.ExpendChosen.UncheckedState.BorderThickness = 2;
            this.ExpendChosen.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.ExpendChosen.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.ExpendChosen.UncheckedState.Parent = this.ExpendChosen;
            this.ExpendChosen.CheckedChanged += new System.EventHandler(this.ExpendChosen_CheckedChanged);
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.BackColor = System.Drawing.Color.Transparent;
            this.DateTimePicker.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DateTimePicker.BorderRadius = 10;
            this.DateTimePicker.BorderThickness = 2;
            this.DateTimePicker.CheckedState.Parent = this.DateTimePicker;
            this.DateTimePicker.FillColor = System.Drawing.Color.White;
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.DateTimePicker.ForeColor = System.Drawing.Color.MidnightBlue;
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.DateTimePicker.HoverState.Parent = this.DateTimePicker;
            this.DateTimePicker.Location = new System.Drawing.Point(35, 18);
            this.DateTimePicker.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.DateTimePicker.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.ShadowDecoration.Parent = this.DateTimePicker;
            this.DateTimePicker.Size = new System.Drawing.Size(200, 56);
            this.DateTimePicker.TabIndex = 54;
            this.DateTimePicker.Value = new System.DateTime(2020, 5, 23, 15, 36, 56, 695);
            // 
            // RemarkTextBox
            // 
            this.RemarkTextBox.BackColor = System.Drawing.Color.Transparent;
            this.RemarkTextBox.BorderRadius = 10;
            this.RemarkTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.RemarkTextBox.DefaultText = "";
            this.RemarkTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.RemarkTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.RemarkTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.RemarkTextBox.DisabledState.Parent = this.RemarkTextBox;
            this.RemarkTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.RemarkTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RemarkTextBox.FocusedState.Parent = this.RemarkTextBox;
            this.RemarkTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RemarkTextBox.ForeColor = System.Drawing.Color.Black;
            this.RemarkTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RemarkTextBox.HoverState.Parent = this.RemarkTextBox;
            this.RemarkTextBox.Location = new System.Drawing.Point(35, 455);
            this.RemarkTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.RemarkTextBox.Name = "RemarkTextBox";
            this.RemarkTextBox.PasswordChar = '\0';
            this.RemarkTextBox.PlaceholderText = "Remark";
            this.RemarkTextBox.SelectedText = "";
            this.RemarkTextBox.ShadowDecoration.Parent = this.RemarkTextBox;
            this.RemarkTextBox.Size = new System.Drawing.Size(198, 56);
            this.RemarkTextBox.TabIndex = 53;
            // 
            // RevenueCateGroup
            // 
            this.RevenueCateGroup.BackColor = System.Drawing.Color.White;
            this.RevenueCateGroup.Controls.Add(this.label12);
            this.RevenueCateGroup.Controls.Add(this.Investment);
            this.RevenueCateGroup.Controls.Add(this.label13);
            this.RevenueCateGroup.Controls.Add(this.Salaries);
            this.RevenueCateGroup.Controls.Add(this.Living);
            this.RevenueCateGroup.Controls.Add(this.label16);
            this.RevenueCateGroup.Controls.Add(this.Gifts);
            this.RevenueCateGroup.Controls.Add(this.label17);
            this.RevenueCateGroup.Location = new System.Drawing.Point(136, 595);
            this.RevenueCateGroup.Name = "RevenueCateGroup";
            this.RevenueCateGroup.Size = new System.Drawing.Size(228, 270);
            this.RevenueCateGroup.TabIndex = 69;
            this.RevenueCateGroup.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label12.Location = new System.Drawing.Point(56, 138);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 22);
            this.label12.TabIndex = 66;
            this.label12.Text = "Investment";
            // 
            // Investment
            // 
            this.Investment.BackColor = System.Drawing.Color.Transparent;
            this.Investment.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Investment.CheckedState.BorderThickness = 0;
            this.Investment.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Investment.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Investment.CheckedState.Parent = this.Investment;
            this.Investment.Location = new System.Drawing.Point(15, 139);
            this.Investment.Name = "Investment";
            this.Investment.ShadowDecoration.Parent = this.Investment;
            this.Investment.Size = new System.Drawing.Size(20, 20);
            this.Investment.TabIndex = 65;
            this.Investment.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Investment.UncheckedState.BorderThickness = 2;
            this.Investment.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Investment.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Investment.UncheckedState.Parent = this.Investment;
            this.Investment.CheckedChanged += new System.EventHandler(this.Investment_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label13.Location = new System.Drawing.Point(56, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 22);
            this.label13.TabIndex = 63;
            this.label13.Text = "Living costs";
            // 
            // Salaries
            // 
            this.Salaries.BackColor = System.Drawing.Color.Transparent;
            this.Salaries.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Salaries.CheckedState.BorderThickness = 0;
            this.Salaries.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Salaries.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Salaries.CheckedState.Parent = this.Salaries;
            this.Salaries.Location = new System.Drawing.Point(15, 21);
            this.Salaries.Name = "Salaries";
            this.Salaries.ShadowDecoration.Parent = this.Salaries;
            this.Salaries.Size = new System.Drawing.Size(20, 20);
            this.Salaries.TabIndex = 56;
            this.Salaries.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Salaries.UncheckedState.BorderThickness = 2;
            this.Salaries.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Salaries.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Salaries.UncheckedState.Parent = this.Salaries;
            this.Salaries.CheckedChanged += new System.EventHandler(this.Salaries_CheckedChanged);
            // 
            // Living
            // 
            this.Living.BackColor = System.Drawing.Color.Transparent;
            this.Living.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Living.CheckedState.BorderThickness = 0;
            this.Living.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Living.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Living.CheckedState.Parent = this.Living;
            this.Living.Location = new System.Drawing.Point(15, 61);
            this.Living.Name = "Living";
            this.Living.ShadowDecoration.Parent = this.Living;
            this.Living.Size = new System.Drawing.Size(20, 20);
            this.Living.TabIndex = 57;
            this.Living.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Living.UncheckedState.BorderThickness = 2;
            this.Living.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Living.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Living.UncheckedState.Parent = this.Living;
            this.Living.CheckedChanged += new System.EventHandler(this.Living_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label16.Location = new System.Drawing.Point(56, 100);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 22);
            this.label16.TabIndex = 64;
            this.label16.Text = "Gifts";
            // 
            // Gifts
            // 
            this.Gifts.BackColor = System.Drawing.Color.Transparent;
            this.Gifts.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Gifts.CheckedState.BorderThickness = 0;
            this.Gifts.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Gifts.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Gifts.CheckedState.Parent = this.Gifts;
            this.Gifts.Location = new System.Drawing.Point(15, 99);
            this.Gifts.Name = "Gifts";
            this.Gifts.ShadowDecoration.Parent = this.Gifts;
            this.Gifts.Size = new System.Drawing.Size(20, 20);
            this.Gifts.TabIndex = 58;
            this.Gifts.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Gifts.UncheckedState.BorderThickness = 2;
            this.Gifts.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Gifts.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Gifts.UncheckedState.Parent = this.Gifts;
            this.Gifts.CheckedChanged += new System.EventHandler(this.Gifts_CheckedChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label17.Location = new System.Drawing.Point(56, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 22);
            this.label17.TabIndex = 62;
            this.label17.Text = "Salaries";
            // 
            // timer1
            // 
            this.timer1.Interval = 5;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.DaysAmount);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(27, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 57);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // showBills
            // 
            this.showBills.BorderRadius = 10;
            this.showBills.CheckedState.Parent = this.showBills;
            this.showBills.CustomImages.Parent = this.showBills;
            this.showBills.FillColor = System.Drawing.Color.MidnightBlue;
            this.showBills.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.showBills.ForeColor = System.Drawing.Color.White;
            this.showBills.HoverState.Parent = this.showBills;
            this.showBills.Location = new System.Drawing.Point(251, 32);
            this.showBills.Name = "showBills";
            this.showBills.ShadowDecoration.Parent = this.showBills;
            this.showBills.Size = new System.Drawing.Size(130, 45);
            this.showBills.TabIndex = 62;
            this.showBills.Text = "SHOW";
            this.showBills.Click += new System.EventHandler(this.showBills_Click);
            // 
            // xuiWifiPercentageAPI1
            // 
            this.xuiWifiPercentageAPI1.Enabled = true;
            this.xuiWifiPercentageAPI1.Interval = 3000;
            // 
            // CloseButton
            // 
            this.CloseButton.CheckedState.Parent = this.CloseButton;
            this.CloseButton.CustomImages.Parent = this.CloseButton;
            this.CloseButton.FillColor = System.Drawing.Color.White;
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.CloseButton.ForeColor = System.Drawing.Color.MidnightBlue;
            this.CloseButton.HoverState.Parent = this.CloseButton;
            this.CloseButton.Location = new System.Drawing.Point(972, 9);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.CloseButton.ShadowDecoration.Parent = this.CloseButton;
            this.CloseButton.Size = new System.Drawing.Size(32, 32);
            this.CloseButton.TabIndex = 71;
            this.CloseButton.Text = "×";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // UCinbox_account
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.RevenueCateGroup);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.AddingPanel);
            this.Controls.Add(this.showBills);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.AccountTable);
            this.Controls.Add(this.AddAccountButton);
            this.Controls.Add(this.AnalysisPanel);
            this.Name = "UCinbox_account";
            this.Size = new System.Drawing.Size(1015, 672);
            ((System.ComponentModel.ISupportInitialize)(this.DaysAmount)).EndInit();
            this.AnalysisPanel.ResumeLayout(false);
            this.xuiFlatTab1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.AddingPanel.ResumeLayout(false);
            this.AddingPanel.PerformLayout();
            this.CategoryPanel.ResumeLayout(false);
            this.ExpendCateGroup.ResumeLayout(false);
            this.ExpendCateGroup.PerformLayout();
            this.RevenueCateGroup.ResumeLayout(false);
            this.RevenueCateGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2ShadowPanel AccountTable;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2NumericUpDown DaysAmount;
        private Guna.UI2.WinForms.Guna2ShadowPanel AnalysisPanel;
        private XanderUI.XUIFlatTab xuiFlatTab1;
        private System.Windows.Forms.TabPage tabPage1;
        private LiveCharts.WinForms.PieChart pieChart2;
        private LiveCharts.WinForms.PieChart pieChart1;
        private System.Windows.Forms.TabPage tabPage2;
        private LiveCharts.WinForms.CartesianChart cartesianChart1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2ShadowPanel CategoryPanel;
        private System.Windows.Forms.Label RevenueChosen;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private Guna.UI2.WinForms.Guna2Button showBills;
        private System.Windows.Forms.GroupBox ExpendCateGroup;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox RevenueCateGroup;
        private System.Windows.Forms.Label label13;
        private Guna.UI2.WinForms.Guna2CustomRadioButton Salaries;
        private Guna.UI2.WinForms.Guna2CustomRadioButton Living;
        private System.Windows.Forms.Label label16;
        private Guna.UI2.WinForms.Guna2CustomRadioButton Gifts;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label12;
        private Guna.UI2.WinForms.Guna2CustomRadioButton Investment;
        public System.Windows.Forms.Timer timer1;
        public Guna.UI2.WinForms.Guna2CircleButton AddAccountButton;
        public Guna.UI2.WinForms.Guna2Button AddButton;
        public Guna.UI2.WinForms.Guna2ShadowPanel AddingPanel;
        public Guna.UI2.WinForms.Guna2DateTimePicker DateTimePicker;
        public Guna.UI2.WinForms.Guna2TextBox RemarkTextBox;
        public Guna.UI2.WinForms.Guna2TextBox AmountTextBox;
        public Guna.UI2.WinForms.Guna2CustomRadioButton RevChosen;
        public Guna.UI2.WinForms.Guna2CustomRadioButton ExpendChosen;
        public Guna.UI2.WinForms.Guna2CustomRadioButton Dining;
        public Guna.UI2.WinForms.Guna2CustomRadioButton Transport;
        public Guna.UI2.WinForms.Guna2CustomRadioButton Daily;
        public Guna.UI2.WinForms.Guna2CustomRadioButton Entertainment;
        public Guna.UI2.WinForms.Guna2CustomRadioButton Medicine;
        public Guna.UI2.WinForms.Guna2CustomRadioButton Clothing;
        private System.Windows.Forms.Label label14;
        private Guna.UI2.WinForms.Guna2HtmlLabel SumExpense;
        private XanderUI.XUIWifiPercentageAPI xuiWifiPercentageAPI1;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel2;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel1;
        private Guna.UI2.WinForms.Guna2HtmlLabel SumIncome;
        private System.Windows.Forms.Label label15;
        private Guna.UI2.WinForms.Guna2CircleButton CloseButton;
    }
}
