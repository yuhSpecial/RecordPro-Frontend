﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Scripts2
{
    public partial class Face : Form
    {
        private UserFaces face { get; set; }
        private Image img { get; set; }
        //定义一个bool变量标识是否拖动窗体
        private bool isMove = false;
        //记录鼠标的位置
        private Point point;

        public Face(UserFaces face, Image img)
        {
            this.face = face;
            this.img = img;
            
            InitializeComponent();

        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Face_Load(object sender, EventArgs e)
        {
            this.pictureBox1.Image = img;
            this.Age.Text = face.age.ToString();
            if (face.gender == 1)
            {
                this.Gender.Text = "男";
            }
            else
            {
                this.Gender.Text = "女";
            }
            if (face.smile == 1)
            {
                this.Smile.Text = "微笑";
            }
            else
            {
                this.Smile.Text = "未笑";
            }
            this.Beauty.Text = face.beauty;
            this.Emotion.Text = face.emotion;
            
        }

        private void Face_MouseDown(object sender, MouseEventArgs e)
        {
            isMove = true;
            //记录鼠标的位置
            point = e.Location;
        }

        private void Face_MouseMove(object sender, MouseEventArgs e)
        {
            if(isMove)
            {
                Point p = e.Location;
                int x = p.X - point.X;
                int y = p.Y - point.Y;
                //窗体的位置
                this.Location = new Point(this.Location.X + x, this.Location.Y + y);
            }

        }

        private void Face_MouseUp(object sender, MouseEventArgs e)
        {
            isMove = false;
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
