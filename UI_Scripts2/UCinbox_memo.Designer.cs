﻿namespace UI_Scripts2
{
    partial class UCinbox_memo
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCinbox_memo));
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.formBorder = new System.Windows.Forms.Panel();
            this.closePic = new System.Windows.Forms.PictureBox();
            this.MemoTable = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.AddMemoPanel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.AddBtn = new Guna.UI2.WinForms.Guna2Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.remarkTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.DateTimePicker = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.Birth = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.Important = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.Special = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.todayLabel = new System.Windows.Forms.Label();
            this.AddMemoButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.memoCell9 = new UI_Scripts2.MemoCell();
            this.memoCell8 = new UI_Scripts2.MemoCell();
            this.memoCell7 = new UI_Scripts2.MemoCell();
            this.memoCell6 = new UI_Scripts2.MemoCell();
            this.memoCell5 = new UI_Scripts2.MemoCell();
            this.memoCell4 = new UI_Scripts2.MemoCell();
            this.memoCell3 = new UI_Scripts2.MemoCell();
            this.memoCell2 = new UI_Scripts2.MemoCell();
            this.memoCell1 = new UI_Scripts2.MemoCell();
            this.formBorder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closePic)).BeginInit();
            this.MemoTable.SuspendLayout();
            this.AddMemoPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 30;
            this.guna2Elipse1.TargetControl = this;
            // 
            // formBorder
            // 
            this.formBorder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.formBorder.Controls.Add(this.closePic);
            this.formBorder.Location = new System.Drawing.Point(862, 15);
            this.formBorder.Name = "formBorder";
            this.formBorder.Size = new System.Drawing.Size(137, 29);
            this.formBorder.TabIndex = 20;
            // 
            // closePic
            // 
            this.closePic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.closePic.BackColor = System.Drawing.Color.White;
            this.closePic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.closePic.Image = ((System.Drawing.Image)(resources.GetObject("closePic.Image")));
            this.closePic.Location = new System.Drawing.Point(114, 3);
            this.closePic.Name = "closePic";
            this.closePic.Size = new System.Drawing.Size(20, 20);
            this.closePic.TabIndex = 19;
            this.closePic.TabStop = false;
            this.closePic.Click += new System.EventHandler(this.closePic_Click);
            // 
            // MemoTable
            // 
            this.MemoTable.AutoScroll = true;
            this.MemoTable.BackColor = System.Drawing.Color.Transparent;
            this.MemoTable.Controls.Add(this.memoCell9);
            this.MemoTable.Controls.Add(this.memoCell8);
            this.MemoTable.Controls.Add(this.memoCell7);
            this.MemoTable.Controls.Add(this.memoCell6);
            this.MemoTable.Controls.Add(this.memoCell5);
            this.MemoTable.Controls.Add(this.memoCell4);
            this.MemoTable.Controls.Add(this.memoCell3);
            this.MemoTable.Controls.Add(this.memoCell2);
            this.MemoTable.Controls.Add(this.memoCell1);
            this.MemoTable.EdgeWidth = 1;
            this.MemoTable.FillColor = System.Drawing.Color.White;
            this.MemoTable.Location = new System.Drawing.Point(3, 86);
            this.MemoTable.Name = "MemoTable";
            this.MemoTable.Padding = new System.Windows.Forms.Padding(6);
            this.MemoTable.Radius = 20;
            this.MemoTable.ShadowColor = System.Drawing.Color.Black;
            this.MemoTable.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.MemoTable.Size = new System.Drawing.Size(996, 359);
            this.MemoTable.TabIndex = 47;
            // 
            // AddMemoPanel
            // 
            this.AddMemoPanel.BackColor = System.Drawing.Color.Transparent;
            this.AddMemoPanel.Controls.Add(this.AddBtn);
            this.AddMemoPanel.Controls.Add(this.label3);
            this.AddMemoPanel.Controls.Add(this.label2);
            this.AddMemoPanel.Controls.Add(this.label1);
            this.AddMemoPanel.Controls.Add(this.remarkTextBox);
            this.AddMemoPanel.Controls.Add(this.DateTimePicker);
            this.AddMemoPanel.Controls.Add(this.label7);
            this.AddMemoPanel.Controls.Add(this.Birth);
            this.AddMemoPanel.Controls.Add(this.Important);
            this.AddMemoPanel.Controls.Add(this.label8);
            this.AddMemoPanel.Controls.Add(this.Special);
            this.AddMemoPanel.Controls.Add(this.label6);
            this.AddMemoPanel.EdgeWidth = 3;
            this.AddMemoPanel.FillColor = System.Drawing.Color.White;
            this.AddMemoPanel.Location = new System.Drawing.Point(994, 598);
            this.AddMemoPanel.Name = "AddMemoPanel";
            this.AddMemoPanel.Radius = 20;
            this.AddMemoPanel.ShadowColor = System.Drawing.Color.Black;
            this.AddMemoPanel.ShadowDepth = 40;
            this.AddMemoPanel.ShadowShift = 7;
            this.AddMemoPanel.Size = new System.Drawing.Size(1080, 123);
            this.AddMemoPanel.TabIndex = 49;
            // 
            // AddBtn
            // 
            this.AddBtn.BorderRadius = 10;
            this.AddBtn.CheckedState.Parent = this.AddBtn;
            this.AddBtn.CustomImages.Parent = this.AddBtn;
            this.AddBtn.FillColor = System.Drawing.Color.MidnightBlue;
            this.AddBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.AddBtn.ForeColor = System.Drawing.Color.White;
            this.AddBtn.HoverState.Parent = this.AddBtn;
            this.AddBtn.Location = new System.Drawing.Point(781, 51);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.ShadowDecoration.Parent = this.AddBtn;
            this.AddBtn.Size = new System.Drawing.Size(164, 40);
            this.AddBtn.TabIndex = 71;
            this.AddBtn.Text = "ADD";
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("等线", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 25);
            this.label3.TabIndex = 75;
            this.label3.Text = "类型";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("等线", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(226, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 25);
            this.label2.TabIndex = 74;
            this.label2.Text = "日期";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("等线", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(455, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 25);
            this.label1.TabIndex = 73;
            this.label1.Text = "备注";
            // 
            // remarkTextBox
            // 
            this.remarkTextBox.AutoScroll = true;
            this.remarkTextBox.BorderColor = System.Drawing.Color.MidnightBlue;
            this.remarkTextBox.BorderRadius = 10;
            this.remarkTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.remarkTextBox.DefaultText = "";
            this.remarkTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.remarkTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.remarkTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.remarkTextBox.DisabledState.Parent = this.remarkTextBox;
            this.remarkTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.remarkTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.remarkTextBox.FocusedState.Parent = this.remarkTextBox;
            this.remarkTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.remarkTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.remarkTextBox.HoverState.Parent = this.remarkTextBox;
            this.remarkTextBox.Location = new System.Drawing.Point(459, 59);
            this.remarkTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.remarkTextBox.Multiline = true;
            this.remarkTextBox.Name = "remarkTextBox";
            this.remarkTextBox.PasswordChar = '\0';
            this.remarkTextBox.PlaceholderText = "";
            this.remarkTextBox.SelectedText = "";
            this.remarkTextBox.ShadowDecoration.Parent = this.remarkTextBox;
            this.remarkTextBox.Size = new System.Drawing.Size(280, 56);
            this.remarkTextBox.TabIndex = 72;
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.BackColor = System.Drawing.Color.Transparent;
            this.DateTimePicker.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DateTimePicker.BorderRadius = 10;
            this.DateTimePicker.BorderThickness = 2;
            this.DateTimePicker.CheckedState.Parent = this.DateTimePicker;
            this.DateTimePicker.FillColor = System.Drawing.Color.White;
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.DateTimePicker.ForeColor = System.Drawing.Color.MidnightBlue;
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.DateTimePicker.HoverState.Parent = this.DateTimePicker;
            this.DateTimePicker.Location = new System.Drawing.Point(230, 59);
            this.DateTimePicker.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.DateTimePicker.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.ShadowDecoration.Parent = this.DateTimePicker;
            this.DateTimePicker.Size = new System.Drawing.Size(200, 56);
            this.DateTimePicker.TabIndex = 71;
            this.DateTimePicker.Value = new System.DateTime(2020, 5, 23, 15, 36, 56, 695);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label7.Location = new System.Drawing.Point(123, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 22);
            this.label7.TabIndex = 69;
            this.label7.Text = "Important";
            // 
            // Birth
            // 
            this.Birth.BackColor = System.Drawing.Color.Transparent;
            this.Birth.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Birth.CheckedState.BorderThickness = 0;
            this.Birth.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Birth.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Birth.CheckedState.Parent = this.Birth;
            this.Birth.Location = new System.Drawing.Point(90, 33);
            this.Birth.Name = "Birth";
            this.Birth.ShadowDecoration.Parent = this.Birth;
            this.Birth.Size = new System.Drawing.Size(20, 20);
            this.Birth.TabIndex = 65;
            this.Birth.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Birth.UncheckedState.BorderThickness = 2;
            this.Birth.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Birth.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Birth.UncheckedState.Parent = this.Birth;
            // 
            // Important
            // 
            this.Important.BackColor = System.Drawing.Color.Transparent;
            this.Important.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Important.CheckedState.BorderThickness = 0;
            this.Important.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Important.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Important.CheckedState.Parent = this.Important;
            this.Important.Location = new System.Drawing.Point(90, 61);
            this.Important.Name = "Important";
            this.Important.ShadowDecoration.Parent = this.Important;
            this.Important.Size = new System.Drawing.Size(20, 20);
            this.Important.TabIndex = 66;
            this.Important.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Important.UncheckedState.BorderThickness = 2;
            this.Important.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Important.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Important.UncheckedState.Parent = this.Important;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label8.Location = new System.Drawing.Point(123, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 22);
            this.label8.TabIndex = 70;
            this.label8.Text = "Special";
            // 
            // Special
            // 
            this.Special.BackColor = System.Drawing.Color.Transparent;
            this.Special.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Special.CheckedState.BorderThickness = 0;
            this.Special.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Special.CheckedState.InnerColor = System.Drawing.Color.White;
            this.Special.CheckedState.Parent = this.Special;
            this.Special.Location = new System.Drawing.Point(90, 89);
            this.Special.Name = "Special";
            this.Special.ShadowDecoration.Parent = this.Special;
            this.Special.Size = new System.Drawing.Size(20, 20);
            this.Special.TabIndex = 67;
            this.Special.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.Special.UncheckedState.BorderThickness = 2;
            this.Special.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.Special.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.Special.UncheckedState.Parent = this.Special;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label6.Location = new System.Drawing.Point(123, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 22);
            this.label6.TabIndex = 68;
            this.label6.Text = "Birth";
            // 
            // todayLabel
            // 
            this.todayLabel.AutoSize = true;
            this.todayLabel.BackColor = System.Drawing.Color.Transparent;
            this.todayLabel.Font = new System.Drawing.Font("方正粗黑宋简体", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.todayLabel.Location = new System.Drawing.Point(322, 495);
            this.todayLabel.Name = "todayLabel";
            this.todayLabel.Size = new System.Drawing.Size(429, 51);
            this.todayLabel.TabIndex = 76;
            this.todayLabel.Text = "Today: 2020-06-18";
            // 
            // AddMemoButton
            // 
            this.AddMemoButton.CheckedState.Parent = this.AddMemoButton;
            this.AddMemoButton.CustomImages.Parent = this.AddMemoButton;
            this.AddMemoButton.FillColor = System.Drawing.Color.MidnightBlue;
            this.AddMemoButton.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddMemoButton.ForeColor = System.Drawing.Color.White;
            this.AddMemoButton.HoverState.Parent = this.AddMemoButton;
            this.AddMemoButton.Location = new System.Drawing.Point(453, 598);
            this.AddMemoButton.Name = "AddMemoButton";
            this.AddMemoButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.AddMemoButton.ShadowDecoration.Parent = this.AddMemoButton;
            this.AddMemoButton.Size = new System.Drawing.Size(70, 70);
            this.AddMemoButton.TabIndex = 77;
            this.AddMemoButton.Text = "+";
            this.AddMemoButton.TextOffset = new System.Drawing.Point(0, -2);
            this.AddMemoButton.Click += new System.EventHandler(this.AddMemoButton_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 5;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // memoCell9
            // 
            this.memoCell9.BackColor = System.Drawing.Color.White;
            this.memoCell9.Dock = System.Windows.Forms.DockStyle.Left;
            this.memoCell9.Location = new System.Drawing.Point(1606, 6);
            this.memoCell9.Name = "memoCell9";
            this.memoCell9.Size = new System.Drawing.Size(200, 321);
            this.memoCell9.TabIndex = 9;
            // 
            // memoCell8
            // 
            this.memoCell8.BackColor = System.Drawing.Color.White;
            this.memoCell8.Dock = System.Windows.Forms.DockStyle.Left;
            this.memoCell8.Location = new System.Drawing.Point(1406, 6);
            this.memoCell8.Name = "memoCell8";
            this.memoCell8.Size = new System.Drawing.Size(200, 321);
            this.memoCell8.TabIndex = 8;
            // 
            // memoCell7
            // 
            this.memoCell7.BackColor = System.Drawing.Color.White;
            this.memoCell7.Dock = System.Windows.Forms.DockStyle.Left;
            this.memoCell7.Location = new System.Drawing.Point(1206, 6);
            this.memoCell7.Name = "memoCell7";
            this.memoCell7.Size = new System.Drawing.Size(200, 321);
            this.memoCell7.TabIndex = 7;
            // 
            // memoCell6
            // 
            this.memoCell6.BackColor = System.Drawing.Color.White;
            this.memoCell6.Dock = System.Windows.Forms.DockStyle.Left;
            this.memoCell6.Location = new System.Drawing.Point(1006, 6);
            this.memoCell6.Name = "memoCell6";
            this.memoCell6.Size = new System.Drawing.Size(200, 321);
            this.memoCell6.TabIndex = 6;
            // 
            // memoCell5
            // 
            this.memoCell5.BackColor = System.Drawing.Color.White;
            this.memoCell5.Dock = System.Windows.Forms.DockStyle.Left;
            this.memoCell5.Location = new System.Drawing.Point(806, 6);
            this.memoCell5.Name = "memoCell5";
            this.memoCell5.Size = new System.Drawing.Size(200, 321);
            this.memoCell5.TabIndex = 5;
            // 
            // memoCell4
            // 
            this.memoCell4.BackColor = System.Drawing.Color.White;
            this.memoCell4.Dock = System.Windows.Forms.DockStyle.Left;
            this.memoCell4.Location = new System.Drawing.Point(606, 6);
            this.memoCell4.Name = "memoCell4";
            this.memoCell4.Size = new System.Drawing.Size(200, 321);
            this.memoCell4.TabIndex = 4;
            // 
            // memoCell3
            // 
            this.memoCell3.BackColor = System.Drawing.Color.White;
            this.memoCell3.Dock = System.Windows.Forms.DockStyle.Left;
            this.memoCell3.Location = new System.Drawing.Point(406, 6);
            this.memoCell3.Name = "memoCell3";
            this.memoCell3.Size = new System.Drawing.Size(200, 321);
            this.memoCell3.TabIndex = 3;
            // 
            // memoCell2
            // 
            this.memoCell2.BackColor = System.Drawing.Color.White;
            this.memoCell2.Dock = System.Windows.Forms.DockStyle.Left;
            this.memoCell2.Location = new System.Drawing.Point(206, 6);
            this.memoCell2.Name = "memoCell2";
            this.memoCell2.Size = new System.Drawing.Size(200, 321);
            this.memoCell2.TabIndex = 2;
            // 
            // memoCell1
            // 
            this.memoCell1.BackColor = System.Drawing.Color.White;
            this.memoCell1.Dock = System.Windows.Forms.DockStyle.Left;
            this.memoCell1.Location = new System.Drawing.Point(6, 6);
            this.memoCell1.Name = "memoCell1";
            this.memoCell1.Size = new System.Drawing.Size(200, 321);
            this.memoCell1.TabIndex = 1;
            // 
            // UCinbox_memo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.AddMemoButton);
            this.Controls.Add(this.todayLabel);
            this.Controls.Add(this.AddMemoPanel);
            this.Controls.Add(this.MemoTable);
            this.Controls.Add(this.formBorder);
            this.Name = "UCinbox_memo";
            this.Size = new System.Drawing.Size(1015, 672);
            this.formBorder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.closePic)).EndInit();
            this.MemoTable.ResumeLayout(false);
            this.AddMemoPanel.ResumeLayout(false);
            this.AddMemoPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private System.Windows.Forms.Panel formBorder;
        private System.Windows.Forms.PictureBox closePic;
        public Guna.UI2.WinForms.Guna2ShadowPanel MemoTable;
        private MemoCell memoCell9;
        private MemoCell memoCell8;
        private MemoCell memoCell7;
        private MemoCell memoCell6;
        private MemoCell memoCell5;
        private MemoCell memoCell4;
        private MemoCell memoCell3;
        private MemoCell memoCell2;
        private MemoCell memoCell1;
        private System.Windows.Forms.Label label7;
        public Guna.UI2.WinForms.Guna2CustomRadioButton Birth;
        public Guna.UI2.WinForms.Guna2CustomRadioButton Important;
        private System.Windows.Forms.Label label8;
        public Guna.UI2.WinForms.Guna2CustomRadioButton Special;
        private System.Windows.Forms.Label label6;
        public Guna.UI2.WinForms.Guna2DateTimePicker DateTimePicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public Guna.UI2.WinForms.Guna2Button AddBtn;
        public Guna.UI2.WinForms.Guna2ShadowPanel AddMemoPanel;
        public Guna.UI2.WinForms.Guna2TextBox remarkTextBox;
        private System.Windows.Forms.Label todayLabel;
        public Guna.UI2.WinForms.Guna2CircleButton AddMemoButton;
        public System.Windows.Forms.Timer timer1;
    }
}
