﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Scripts2
{
    public partial class AccountCell : UserControl
    {
        private Api api;
        public int AccountID;
        public bool type;
        
        public AccountCell()
        {
            InitializeComponent();
            api = new Api();
            
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            UCinbox_account panel = (UCinbox_account)EditButton.Parent.Parent.Parent;
            panel.AddingPanel.Visible = true;
            panel.timer1.Start();
            panel.AddButton.Text = "UPDATE";
            panel.AccountID = this.AccountID;
            if (type)
                panel.ExpendChosen.Checked = true;
            else
                panel.RevChosen.Checked = true;
            panel.RemarkTextBox.Text = RemarkLabel.Text;
            panel.AmountTextBox.Text = PriceLabel.Text;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            UCinbox_account panel = (UCinbox_account)EditButton.Parent.Parent.Parent;
            string state = api.DeleteUserBillsAsync(Api.Token, AccountID);
            if (state == "Created")
            {
                MessageBox.Show("Deleting succeed");
                panel.CartesianShow();
                panel.PieChartShow();
                panel.AccountsShow();
            }
            else
                MessageBox.Show(state);
        }
    }
}
