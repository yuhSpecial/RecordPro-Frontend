﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Guna.UI2.WinForms;
using XanderUI;

namespace UI_Scripts2
{
    public partial class MainPage : Form
    {
        private UCinbox_statics statics;
        private UCinbox_memo memo;
        private UCinbox_bodies body;
        private UCinbox_pictures pictures;
        private UCinbox_account accounts;
        private UCinbox_notes notes;
        private Api api;
        public MainPage()
        {
            InitializeComponent();
            api = new Api();
            
            UCinbox_notes notes = new UCinbox_notes();
            AddControlsToPanel(notes);
        }

        //移动button
        private void moveImageBox(object sender)
        {
            Guna2Button b = (Guna2Button)sender;
            imgSlide.Location = new Point(b.Location.X + 118, b.Location.Y - 30);
            imgSlide.SendToBack();
        }

        private void guna2Button1_CheckedChanged(object sender, EventArgs e)
        {
            moveImageBox(sender);
        }

        private void AddControlsToPanel(Control c)
        {
            c.Dock = DockStyle.Fill;
            panel1.Controls.Clear();
            panel1.Controls.Add(c);
        }
        private void guna2Button1_Click(object sender, EventArgs e) //按"notes"转换为notes interface
        {
            if(notes==null)
            {
                this.notes = new UCinbox_notes();
            }
            AddControlsToPanel(notes);
        }
        private void guna2Button2_Click(object sender, EventArgs e) //按"accounts"转换为accounts interface
        {
            if(accounts==null)
            {
                this.accounts = new UCinbox_account();
            }
            AddControlsToPanel(accounts);
        }
        private void guna2Button3_Click(object sender, EventArgs e) //按"pictures"转换为pictures interface
        {
            if (pictures == null)
            {
                this.pictures = new UCinbox_pictures();
            }
            AddControlsToPanel(pictures);

        }



        private Point offset;
        private void panel2_MouseDown(object sender, MouseEventArgs e) //拖动窗体
        {
            if (MouseButtons.Left != e.Button) return;
            Point cur = this.PointToScreen(e.Location);
            offset = new Point(cur.X - this.Left, cur.Y - this.Top);
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button) return;
            Point cur = MousePosition;
            this.Location = new Point(cur.X - offset.X, cur.Y - offset.Y);
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            if (body == null)
            {
                this.body = new UCinbox_bodies();
            }
            AddControlsToPanel(body);
        }
        private void guna2Button5_Click(object sender, EventArgs e)
        {
            if (memo == null)
            {
                this.memo = new UCinbox_memo();
            }
            AddControlsToPanel(memo);
        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            if (statics == null)
            {
                this.statics = new UCinbox_statics();
            }
            AddControlsToPanel(statics);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.Opacity >= 0.05)
                this.Opacity -= 0.05;
            else
            {
                timer1.Stop();
                UserSignIn userSignIn = new UserSignIn();
                this.Hide();
                userSignIn.ShowDialog();
            }
        }
    }
}
