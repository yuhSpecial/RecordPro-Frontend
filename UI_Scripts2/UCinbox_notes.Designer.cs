﻿namespace UI_Scripts2
{
    partial class UCinbox_notes
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.AddButton = new Guna.UI2.WinForms.Guna2TileButton();
            this.StartAddButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.AddingNotePanel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.guna2ShadowPanel4 = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.NotesTextBox = new System.Windows.Forms.RichTextBox();
            this.DateTimePicker = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.AmountLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.showNotes = new Guna.UI2.WinForms.Guna2Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DaysAmount = new Guna.UI2.WinForms.Guna2NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.NotesTable = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.AnalysisPanel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.cartesianChart2 = new LiveCharts.WinForms.CartesianChart();
            this.solidGauge1 = new LiveCharts.WinForms.SolidGauge();
            this.cartesianChart1 = new LiveCharts.WinForms.CartesianChart();
            this.CloseButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.AddingNotePanel.SuspendLayout();
            this.guna2ShadowPanel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DaysAmount)).BeginInit();
            this.AnalysisPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 30;
            this.guna2Elipse1.TargetControl = this;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("方正粗黑宋简体", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(9, 303);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 26);
            this.label3.TabIndex = 10;
            this.label3.Text = "情感统计";
            // 
            // AddButton
            // 
            this.AddButton.BorderRadius = 10;
            this.AddButton.CheckedState.Parent = this.AddButton;
            this.AddButton.CustomImages.Parent = this.AddButton;
            this.AddButton.FillColor = System.Drawing.Color.White;
            this.AddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddButton.ForeColor = System.Drawing.Color.MidnightBlue;
            this.AddButton.HoverState.Parent = this.AddButton;
            this.AddButton.Location = new System.Drawing.Point(20, 538);
            this.AddButton.Name = "AddButton";
            this.AddButton.ShadowDecoration.Parent = this.AddButton;
            this.AddButton.Size = new System.Drawing.Size(373, 54);
            this.AddButton.TabIndex = 11;
            this.AddButton.Text = "ADD NOTE";
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // StartAddButton
            // 
            this.StartAddButton.CheckedState.Parent = this.StartAddButton;
            this.StartAddButton.CustomImages.Parent = this.StartAddButton;
            this.StartAddButton.FillColor = System.Drawing.Color.MidnightBlue;
            this.StartAddButton.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartAddButton.ForeColor = System.Drawing.Color.White;
            this.StartAddButton.HoverState.Parent = this.StartAddButton;
            this.StartAddButton.Location = new System.Drawing.Point(0, 568);
            this.StartAddButton.Name = "StartAddButton";
            this.StartAddButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.StartAddButton.ShadowDecoration.Parent = this.StartAddButton;
            this.StartAddButton.Size = new System.Drawing.Size(70, 70);
            this.StartAddButton.TabIndex = 15;
            this.StartAddButton.Text = "+";
            this.StartAddButton.TextOffset = new System.Drawing.Point(0, -2);
            this.StartAddButton.Click += new System.EventHandler(this.StartAddButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("方正粗黑宋简体", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(9, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 26);
            this.label1.TabIndex = 20;
            this.label1.Text = "字数统计";
            // 
            // AddingNotePanel
            // 
            this.AddingNotePanel.BackColor = System.Drawing.Color.Transparent;
            this.AddingNotePanel.Controls.Add(this.guna2ShadowPanel4);
            this.AddingNotePanel.Controls.Add(this.DateTimePicker);
            this.AddingNotePanel.Controls.Add(this.AmountLabel);
            this.AddingNotePanel.Controls.Add(this.AddButton);
            this.AddingNotePanel.FillColor = System.Drawing.Color.MidnightBlue;
            this.AddingNotePanel.Location = new System.Drawing.Point(85, 640);
            this.AddingNotePanel.Name = "AddingNotePanel";
            this.AddingNotePanel.Radius = 10;
            this.AddingNotePanel.ShadowColor = System.Drawing.Color.Black;
            this.AddingNotePanel.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.AddingNotePanel.Size = new System.Drawing.Size(417, 617);
            this.AddingNotePanel.TabIndex = 23;
            // 
            // guna2ShadowPanel4
            // 
            this.guna2ShadowPanel4.BackColor = System.Drawing.Color.Transparent;
            this.guna2ShadowPanel4.Controls.Add(this.NotesTextBox);
            this.guna2ShadowPanel4.FillColor = System.Drawing.Color.White;
            this.guna2ShadowPanel4.Location = new System.Drawing.Point(8, 57);
            this.guna2ShadowPanel4.Name = "guna2ShadowPanel4";
            this.guna2ShadowPanel4.Radius = 7;
            this.guna2ShadowPanel4.ShadowColor = System.Drawing.Color.Black;
            this.guna2ShadowPanel4.ShadowDepth = 0;
            this.guna2ShadowPanel4.Size = new System.Drawing.Size(390, 453);
            this.guna2ShadowPanel4.TabIndex = 57;
            // 
            // NotesTextBox
            // 
            this.NotesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NotesTextBox.BulletIndent = 4;
            this.NotesTextBox.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.NotesTextBox.Location = new System.Drawing.Point(12, 16);
            this.NotesTextBox.Name = "NotesTextBox";
            this.NotesTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.NotesTextBox.Size = new System.Drawing.Size(370, 423);
            this.NotesTextBox.TabIndex = 56;
            this.NotesTextBox.Text = "";
            this.NotesTextBox.TextChanged += new System.EventHandler(this.NotesTextBox_TextChanged);
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.BackColor = System.Drawing.Color.Transparent;
            this.DateTimePicker.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DateTimePicker.BorderRadius = 10;
            this.DateTimePicker.BorderThickness = 2;
            this.DateTimePicker.CheckedState.Parent = this.DateTimePicker;
            this.DateTimePicker.FillColor = System.Drawing.Color.White;
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTimePicker.ForeColor = System.Drawing.Color.MidnightBlue;
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.DateTimePicker.HoverState.Parent = this.DateTimePicker;
            this.DateTimePicker.Location = new System.Drawing.Point(8, 9);
            this.DateTimePicker.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.DateTimePicker.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.ShadowDecoration.Parent = this.DateTimePicker;
            this.DateTimePicker.Size = new System.Drawing.Size(188, 42);
            this.DateTimePicker.TabIndex = 55;
            this.DateTimePicker.Value = new System.DateTime(2020, 5, 23, 15, 36, 56, 695);
            // 
            // AmountLabel
            // 
            this.AmountLabel.AutoSize = true;
            this.AmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.AmountLabel.ForeColor = System.Drawing.Color.White;
            this.AmountLabel.Location = new System.Drawing.Point(223, 26);
            this.AmountLabel.Name = "AmountLabel";
            this.AmountLabel.Size = new System.Drawing.Size(181, 26);
            this.AmountLabel.TabIndex = 19;
            this.AmountLabel.Text = "Words Amount: 0";
            // 
            // timer1
            // 
            this.timer1.Interval = 5;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // showNotes
            // 
            this.showNotes.BorderRadius = 10;
            this.showNotes.CheckedState.Parent = this.showNotes;
            this.showNotes.CustomImages.Parent = this.showNotes;
            this.showNotes.FillColor = System.Drawing.Color.MidnightBlue;
            this.showNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.showNotes.ForeColor = System.Drawing.Color.White;
            this.showNotes.HoverState.Parent = this.showNotes;
            this.showNotes.Location = new System.Drawing.Point(251, 32);
            this.showNotes.Name = "showNotes";
            this.showNotes.ShadowDecoration.Parent = this.showNotes;
            this.showNotes.Size = new System.Drawing.Size(130, 45);
            this.showNotes.TabIndex = 65;
            this.showNotes.Text = "SHOW";
            this.showNotes.Click += new System.EventHandler(this.showNotes_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.DaysAmount);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(27, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 57);
            this.groupBox1.TabIndex = 64;
            this.groupBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(6, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 29);
            this.label5.TabIndex = 48;
            this.label5.Text = "Last";
            // 
            // DaysAmount
            // 
            this.DaysAmount.BackColor = System.Drawing.Color.Transparent;
            this.DaysAmount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.DaysAmount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.DaysAmount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.DaysAmount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.DaysAmount.DisabledState.Parent = this.DaysAmount;
            this.DaysAmount.DisabledState.UpDownButtonFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(177)))), ((int)(((byte)(177)))));
            this.DaysAmount.DisabledState.UpDownButtonForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(203)))), ((int)(((byte)(203)))));
            this.DaysAmount.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.DaysAmount.FocusedState.Parent = this.DaysAmount;
            this.DaysAmount.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DaysAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.DaysAmount.Location = new System.Drawing.Point(58, 17);
            this.DaysAmount.Name = "DaysAmount";
            this.DaysAmount.ShadowDecoration.Parent = this.DaysAmount;
            this.DaysAmount.Size = new System.Drawing.Size(65, 30);
            this.DaysAmount.TabIndex = 47;
            this.DaysAmount.UpDownButtonFillColor = System.Drawing.Color.MidnightBlue;
            this.DaysAmount.UpDownButtonForeColor = System.Drawing.Color.White;
            this.DaysAmount.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(129, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 29);
            this.label6.TabIndex = 49;
            this.label6.Text = "Days";
            // 
            // NotesTable
            // 
            this.NotesTable.AutoScroll = true;
            this.NotesTable.BackColor = System.Drawing.Color.Transparent;
            this.NotesTable.EdgeWidth = 1;
            this.NotesTable.FillColor = System.Drawing.Color.White;
            this.NotesTable.Location = new System.Drawing.Point(7, 86);
            this.NotesTable.Name = "NotesTable";
            this.NotesTable.Radius = 20;
            this.NotesTable.ShadowColor = System.Drawing.Color.MidnightBlue;
            this.NotesTable.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.NotesTable.Size = new System.Drawing.Size(500, 483);
            this.NotesTable.TabIndex = 63;
            // 
            // AnalysisPanel
            // 
            this.AnalysisPanel.AutoScroll = true;
            this.AnalysisPanel.BackColor = System.Drawing.Color.Transparent;
            this.AnalysisPanel.Controls.Add(this.cartesianChart2);
            this.AnalysisPanel.Controls.Add(this.solidGauge1);
            this.AnalysisPanel.Controls.Add(this.cartesianChart1);
            this.AnalysisPanel.Controls.Add(this.label1);
            this.AnalysisPanel.Controls.Add(this.label3);
            this.AnalysisPanel.EdgeWidth = 1;
            this.AnalysisPanel.FillColor = System.Drawing.Color.White;
            this.AnalysisPanel.Location = new System.Drawing.Point(512, 46);
            this.AnalysisPanel.Name = "AnalysisPanel";
            this.AnalysisPanel.Radius = 20;
            this.AnalysisPanel.ShadowColor = System.Drawing.Color.MidnightBlue;
            this.AnalysisPanel.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.ForwardDiagonal;
            this.AnalysisPanel.Size = new System.Drawing.Size(487, 594);
            this.AnalysisPanel.TabIndex = 66;
            // 
            // cartesianChart2
            // 
            this.cartesianChart2.Location = new System.Drawing.Point(160, 349);
            this.cartesianChart2.Name = "cartesianChart2";
            this.cartesianChart2.Size = new System.Drawing.Size(315, 208);
            this.cartesianChart2.TabIndex = 23;
            this.cartesianChart2.Text = "cartesianChart2";
            // 
            // solidGauge1
            // 
            this.solidGauge1.Location = new System.Drawing.Point(20, 399);
            this.solidGauge1.Name = "solidGauge1";
            this.solidGauge1.Size = new System.Drawing.Size(124, 143);
            this.solidGauge1.TabIndex = 22;
            this.solidGauge1.Text = "solidGauge1";
            // 
            // cartesianChart1
            // 
            this.cartesianChart1.Location = new System.Drawing.Point(13, 38);
            this.cartesianChart1.Name = "cartesianChart1";
            this.cartesianChart1.Size = new System.Drawing.Size(462, 251);
            this.cartesianChart1.TabIndex = 21;
            this.cartesianChart1.Text = "cartesianChart1";
            // 
            // CloseButton
            // 
            this.CloseButton.CheckedState.Parent = this.CloseButton;
            this.CloseButton.CustomImages.Parent = this.CloseButton;
            this.CloseButton.FillColor = System.Drawing.Color.White;
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.CloseButton.ForeColor = System.Drawing.Color.MidnightBlue;
            this.CloseButton.HoverState.Parent = this.CloseButton;
            this.CloseButton.Location = new System.Drawing.Point(972, 9);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.CloseButton.ShadowDecoration.Parent = this.CloseButton;
            this.CloseButton.Size = new System.Drawing.Size(32, 32);
            this.CloseButton.TabIndex = 71;
            this.CloseButton.Text = "×";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // UCinbox_notes
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.AddingNotePanel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.AnalysisPanel);
            this.Controls.Add(this.showNotes);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.NotesTable);
            this.Controls.Add(this.StartAddButton);
            this.Name = "UCinbox_notes";
            this.Size = new System.Drawing.Size(1015, 672);
            this.AddingNotePanel.ResumeLayout(false);
            this.AddingNotePanel.PerformLayout();
            this.guna2ShadowPanel4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DaysAmount)).EndInit();
            this.AnalysisPanel.ResumeLayout(false);
            this.AnalysisPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2ShadowPanel guna2ShadowPanel4;
        private Guna.UI2.WinForms.Guna2Button showNotes;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2NumericUpDown DaysAmount;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2ShadowPanel NotesTable;
        private Guna.UI2.WinForms.Guna2ShadowPanel AnalysisPanel;
        private LiveCharts.WinForms.CartesianChart cartesianChart2;
        private LiveCharts.WinForms.SolidGauge solidGauge1;
        private LiveCharts.WinForms.CartesianChart cartesianChart1;
        public Guna.UI2.WinForms.Guna2ShadowPanel AddingNotePanel;
        public System.Windows.Forms.Timer timer1;
        public Guna.UI2.WinForms.Guna2TileButton AddButton;
        public Guna.UI2.WinForms.Guna2CircleButton StartAddButton;
        public System.Windows.Forms.RichTextBox NotesTextBox;
        public Guna.UI2.WinForms.Guna2DateTimePicker DateTimePicker;
        public System.Windows.Forms.Label AmountLabel;
        private Guna.UI2.WinForms.Guna2CircleButton CloseButton;
    }
}
