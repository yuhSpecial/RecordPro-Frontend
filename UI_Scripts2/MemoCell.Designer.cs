﻿namespace UI_Scripts2
{
    partial class MemoCell
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MemoCell));
            this.guna2CircleButton1 = new Guna.UI2.WinForms.Guna2CircleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.LeftDaysLabel = new System.Windows.Forms.Label();
            this.TypeLabel = new System.Windows.Forms.Label();
            this.DateLabel = new System.Windows.Forms.Label();
            this.DescLabel = new Guna.UI2.WinForms.Guna2TextBox();
            this.EditButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.DeleteButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.SuspendLayout();
            // 
            // guna2CircleButton1
            // 
            this.guna2CircleButton1.CheckedState.BorderColor = System.Drawing.Color.Black;
            this.guna2CircleButton1.CheckedState.FillColor = System.Drawing.Color.MidnightBlue;
            this.guna2CircleButton1.CheckedState.Parent = this.guna2CircleButton1;
            this.guna2CircleButton1.CustomImages.Parent = this.guna2CircleButton1;
            this.guna2CircleButton1.FillColor = System.Drawing.Color.MidnightBlue;
            this.guna2CircleButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F);
            this.guna2CircleButton1.ForeColor = System.Drawing.Color.White;
            this.guna2CircleButton1.HoverState.BorderColor = System.Drawing.Color.Black;
            this.guna2CircleButton1.HoverState.FillColor = System.Drawing.Color.MidnightBlue;
            this.guna2CircleButton1.HoverState.Parent = this.guna2CircleButton1;
            this.guna2CircleButton1.Location = new System.Drawing.Point(0, 0);
            this.guna2CircleButton1.Name = "guna2CircleButton1";
            this.guna2CircleButton1.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2CircleButton1.ShadowDecoration.Parent = this.guna2CircleButton1;
            this.guna2CircleButton1.Size = new System.Drawing.Size(200, 200);
            this.guna2CircleButton1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.MidnightBlue;
            this.label1.Font = new System.Drawing.Font("等线", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(56, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "days left";
            // 
            // LeftDaysLabel
            // 
            this.LeftDaysLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LeftDaysLabel.BackColor = System.Drawing.Color.MidnightBlue;
            this.LeftDaysLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F);
            this.LeftDaysLabel.ForeColor = System.Drawing.Color.White;
            this.LeftDaysLabel.Location = new System.Drawing.Point(19, 70);
            this.LeftDaysLabel.Name = "LeftDaysLabel";
            this.LeftDaysLabel.Size = new System.Drawing.Size(163, 78);
            this.LeftDaysLabel.TabIndex = 2;
            this.LeftDaysLabel.Text = "354";
            this.LeftDaysLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TypeLabel
            // 
            this.TypeLabel.BackColor = System.Drawing.Color.MidnightBlue;
            this.TypeLabel.Font = new System.Drawing.Font("等线", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TypeLabel.ForeColor = System.Drawing.Color.White;
            this.TypeLabel.Location = new System.Drawing.Point(46, 20);
            this.TypeLabel.Name = "TypeLabel";
            this.TypeLabel.Size = new System.Drawing.Size(108, 23);
            this.TypeLabel.TabIndex = 3;
            this.TypeLabel.Text = "Important";
            this.TypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DateLabel
            // 
            this.DateLabel.AutoSize = true;
            this.DateLabel.BackColor = System.Drawing.Color.MidnightBlue;
            this.DateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic);
            this.DateLabel.ForeColor = System.Drawing.Color.White;
            this.DateLabel.Location = new System.Drawing.Point(36, 43);
            this.DateLabel.Name = "DateLabel";
            this.DateLabel.Size = new System.Drawing.Size(129, 29);
            this.DateLabel.TabIndex = 4;
            this.DateLabel.Text = "2020.08.23";
            // 
            // DescLabel
            // 
            this.DescLabel.AutoScroll = true;
            this.DescLabel.BorderColor = System.Drawing.Color.White;
            this.DescLabel.BorderRadius = 15;
            this.DescLabel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.DescLabel.DefaultText = "No Remarks";
            this.DescLabel.DisabledState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DescLabel.DisabledState.FillColor = System.Drawing.Color.White;
            this.DescLabel.DisabledState.ForeColor = System.Drawing.Color.MidnightBlue;
            this.DescLabel.DisabledState.Parent = this.DescLabel;
            this.DescLabel.DisabledState.PlaceholderForeColor = System.Drawing.Color.MidnightBlue;
            this.DescLabel.Enabled = false;
            this.DescLabel.FocusedState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DescLabel.FocusedState.Parent = this.DescLabel;
            this.DescLabel.Font = new System.Drawing.Font("方正粗黑宋简体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DescLabel.HoverState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DescLabel.HoverState.Parent = this.DescLabel;
            this.DescLabel.Location = new System.Drawing.Point(6, 208);
            this.DescLabel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.DescLabel.Multiline = true;
            this.DescLabel.Name = "DescLabel";
            this.DescLabel.PasswordChar = '\0';
            this.DescLabel.PlaceholderText = "";
            this.DescLabel.SelectedText = "";
            this.DescLabel.ShadowDecoration.Parent = this.DescLabel;
            this.DescLabel.Size = new System.Drawing.Size(200, 46);
            this.DescLabel.TabIndex = 5;
            this.DescLabel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // EditButton
            // 
            this.EditButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.EditButton.BorderThickness = 2;
            this.EditButton.CheckedState.Parent = this.EditButton;
            this.EditButton.CustomImages.Parent = this.EditButton;
            this.EditButton.FillColor = System.Drawing.Color.White;
            this.EditButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.EditButton.ForeColor = System.Drawing.Color.White;
            this.EditButton.HoverState.Parent = this.EditButton;
            this.EditButton.Image = ((System.Drawing.Image)(resources.GetObject("EditButton.Image")));
            this.EditButton.Location = new System.Drawing.Point(41, 262);
            this.EditButton.Name = "EditButton";
            this.EditButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.EditButton.ShadowDecoration.Parent = this.EditButton;
            this.EditButton.Size = new System.Drawing.Size(48, 48);
            this.EditButton.TabIndex = 7;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DeleteButton.BorderThickness = 2;
            this.DeleteButton.CheckedState.Parent = this.DeleteButton;
            this.DeleteButton.CustomImages.Parent = this.DeleteButton;
            this.DeleteButton.FillColor = System.Drawing.Color.White;
            this.DeleteButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.DeleteButton.ForeColor = System.Drawing.Color.White;
            this.DeleteButton.HoverState.Parent = this.DeleteButton;
            this.DeleteButton.Image = ((System.Drawing.Image)(resources.GetObject("DeleteButton.Image")));
            this.DeleteButton.Location = new System.Drawing.Point(108, 262);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.DeleteButton.ShadowDecoration.Parent = this.DeleteButton;
            this.DeleteButton.Size = new System.Drawing.Size(48, 48);
            this.DeleteButton.TabIndex = 8;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // MemoCell
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.EditButton);
            this.Controls.Add(this.DescLabel);
            this.Controls.Add(this.DateLabel);
            this.Controls.Add(this.TypeLabel);
            this.Controls.Add(this.LeftDaysLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.guna2CircleButton1);
            this.Name = "MemoCell";
            this.Size = new System.Drawing.Size(234, 336);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2CircleButton guna2CircleButton1;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2CircleButton EditButton;
        private Guna.UI2.WinForms.Guna2CircleButton DeleteButton;
        public System.Windows.Forms.Label LeftDaysLabel;
        public System.Windows.Forms.Label TypeLabel;
        public System.Windows.Forms.Label DateLabel;
        public Guna.UI2.WinForms.Guna2TextBox DescLabel;
    }
}
