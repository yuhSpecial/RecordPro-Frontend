﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LiveCharts;
using LiveCharts.Helpers;
using LiveCharts.Wpf;
namespace UI_Scripts2
{
    public partial class UCinbox_bodies : UserControl
    {
        private Api api;
        private List<UserGetBody> userbodies;
        private List<DateTime> dateTimes;
        private List<double> Weights;
        private List<double> Heights;
        private int Days { get; set; }
        public UCinbox_bodies()
        {
            api = new Api();
            Weights = new List<double>();
            Heights = new List<double>();
            dateTimes = new List<DateTime>();
            userbodies = GetBodies(Api.Token);
            InitializeComponent();
            
            this.AddingBodyPanel.Visible = false;
            PaintPanel();
        }
        //private void repaint()
        //{
        //    PaintPanel();
        //}
        public void PaintPanel()
        {
            userbodies = api.GetUserBodies(Api.Token);
            BodyTable.Controls.Clear();
            Weights.Clear();
            Heights.Clear();
            dateTimes.Clear();
            foreach(UserGetBody body in userbodies)
            {
                Weights.Add(body.weight);
                Heights.Add(body.height);
                dateTimes.Add(body.dateTime);
                Body Body1 = new Body(body.id, new UserBody(body), body.dateTime, 1);
                BodyTable.Controls.Add(Body1); 
                Body1.Dock = DockStyle.Top;
                Body1.Width= 439;
                Body1.Height = 523;
            }
            PaintChart(this.Weights, this.Heights, this.dateTimes);

        }
        
        private List<UserGetBody> GetBodies(string token)
        {
            try { 
                return api.GetUserBodies(Api.Token);
                    }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private void ShowBodies_Click(object sender, EventArgs e)
        {
            this.Days = (int)DaysAmount.Value;
            List<UserGetBody> usrbodies = api.GetUserBodies(Api.Token);
            List<UserGetBody> usrbodies2 = new List<UserGetBody>(usrbodies);
            foreach(UserGetBody usr in usrbodies2)
            {
                if(usr.dateTime<=DateTime.Now.AddDays(Days*-1) || usr.dateTime<=DateTime.Now)
                {
                    usrbodies.Remove(usr);
                }
            }
            PaintPanel();
        }
        

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private int loc_x = -591;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (StartAddButton.Text == "+") //add panel
            {
                if (loc_x < 4)
                {
                    loc_x += 20;
                    AddingBodyPanel.Location = new Point(loc_x, 31);
                    StartAddButton.Location = new Point(loc_x + 517, 562);
                }
                else
                {
                    StartAddButton.Text = "×";
                    timer1.Stop();
                    groupBox1.Visible = false;
                    ShowBodies.Visible = false;
                    return;
                }
            }
            else
            {
                if (loc_x > -417)
                {
                    loc_x -= 20;
                    AddingBodyPanel.Location = new Point(loc_x, 31);
                    StartAddButton.Location = new Point(loc_x + 591, 562);
                }
                else
                {
                    StartAddButton.Text = "+";
                    timer1.Stop();
                    AddingBodyPanel.Visible = false;
                    groupBox1.Visible = true;
                    ShowBodies.Visible = true;
                    return;
                }
            }
        }

        private void StartAddButton_Click(object sender, EventArgs e)
        {
            if (StartAddButton.Text == "+") //add panel
            {
                groupBox1.Visible = false;
                ShowBodies.Visible = false;
                AddingBodyPanel.Visible = true;
                AddingBodyPanel.Location = new Point(-417, 31);
                DateTimePicker.Value = DateTime.Now;
                timer1.Start();
            }
            else //cancel
            {
                timer1.Start();
            }
        }



        private void AddData_Click(object sender, EventArgs e)
        {
            string message;
            try { UserBody body = new UserBody(Convert.ToDouble(Weight_textbox.Text), Convert.ToDouble(Shoulder_textbox.Text), Convert.ToDouble(Height_textbox.Text), Convert.ToDouble(Bust_textbox.Text), Convert.ToDouble(Waist_textbox.Text), Convert.ToDouble(Hips_textbox.Text), DateTime.Now, Api.Token);
                message = api.PostUserBody(body);
                MessageBox.Show(message);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            PaintPanel();
        }

        private void UCinbox_bodies_Load(object sender, EventArgs e)
        {

            PaintChart(this.Weights, this.Heights, this.dateTimes);


        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            MainPage mainPage = (MainPage)CloseButton.Parent.Parent.Parent;
            mainPage.timer1.Start();
        }
        public void PaintChart(List<double>Weights, List<double> Heights,List<DateTime> dateTimes)
        {
            cartesianChart1.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(34, 46, 49));
            cartesianChart2.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(34, 46, 49));
            cartesianChart1.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title="身高",
                    Values=Heights.AsChartValues(),
                    StrokeThickness = 1,
                    StrokeDashArray = new System.Windows.Media.DoubleCollection(20),
                    Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(107, 185, 69)),
                    Fill = System.Windows.Media.Brushes.Transparent,
                    LineSmoothness = 0,
                    PointGeometry = null
                },
            };
            cartesianChart2.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title="体重",
                    Values=Weights.AsChartValues(),
                    StrokeThickness = 1,
                    StrokeDashArray = new System.Windows.Media.DoubleCollection(20),
                    Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(107, 185, 69)),
                    Fill = System.Windows.Media.Brushes.Transparent,
                    LineSmoothness = 0,
                    PointGeometry = null
                },
            };
            Func<double, string> formatFunc = (x) => string.Format("{0:D}", x);
            IList<string> transform(List<DateTime> datetime)
            {
                IList<string> list = new List<string>();

                foreach (DateTime time in datetime)
                {
                    list.Add(time.ToString("d"));
                }
                return list;
            }

            IList<string> Time = transform(dateTimes);
            cartesianChart1.AxisX.Clear();
            cartesianChart2.AxisX.Clear();
            cartesianChart1.AxisX.Add(new Axis
            {
                Title = "时间",
                Labels = Time,
                LabelFormatter = formatFunc,
            });
            cartesianChart2.AxisX.Add(new Axis
            {
                Title = "时间",
                Labels = Time,
            });

        }
    }
}
