﻿namespace UI_Scripts2
{
    partial class AccountCell
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccountCell));
            this.RemarkLabel = new System.Windows.Forms.Label();
            this.PriceLabel = new System.Windows.Forms.Label();
            this.EditButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.DeleteButton = new Guna.UI2.WinForms.Guna2CircleButton();
            this.Panel = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.DateLabel = new System.Windows.Forms.Label();
            this.category = new System.Windows.Forms.Label();
            this.typeLabel = new System.Windows.Forms.Label();
            this.Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // RemarkLabel
            // 
            this.RemarkLabel.AutoSize = true;
            this.RemarkLabel.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.RemarkLabel.Location = new System.Drawing.Point(16, 12);
            this.RemarkLabel.Name = "RemarkLabel";
            this.RemarkLabel.Size = new System.Drawing.Size(103, 30);
            this.RemarkLabel.TabIndex = 41;
            this.RemarkLabel.Text = "Remark";
            // 
            // PriceLabel
            // 
            this.PriceLabel.AutoSize = true;
            this.PriceLabel.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.PriceLabel.Location = new System.Drawing.Point(46, 54);
            this.PriceLabel.Name = "PriceLabel";
            this.PriceLabel.Size = new System.Drawing.Size(76, 22);
            this.PriceLabel.TabIndex = 42;
            this.PriceLabel.Text = "￥price";
            // 
            // EditButton
            // 
            this.EditButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.EditButton.BorderThickness = 2;
            this.EditButton.CheckedState.Parent = this.EditButton;
            this.EditButton.CustomImages.Parent = this.EditButton;
            this.EditButton.FillColor = System.Drawing.Color.White;
            this.EditButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.EditButton.ForeColor = System.Drawing.Color.White;
            this.EditButton.HoverState.Parent = this.EditButton;
            this.EditButton.Image = ((System.Drawing.Image)(resources.GetObject("EditButton.Image")));
            this.EditButton.Location = new System.Drawing.Point(226, 69);
            this.EditButton.Name = "EditButton";
            this.EditButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.EditButton.ShadowDecoration.Parent = this.EditButton;
            this.EditButton.Size = new System.Drawing.Size(40, 40);
            this.EditButton.TabIndex = 44;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DeleteButton.BorderThickness = 2;
            this.DeleteButton.CheckedState.Parent = this.DeleteButton;
            this.DeleteButton.CustomImages.Parent = this.DeleteButton;
            this.DeleteButton.FillColor = System.Drawing.Color.White;
            this.DeleteButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.DeleteButton.ForeColor = System.Drawing.Color.White;
            this.DeleteButton.HoverState.Parent = this.DeleteButton;
            this.DeleteButton.Image = ((System.Drawing.Image)(resources.GetObject("DeleteButton.Image")));
            this.DeleteButton.Location = new System.Drawing.Point(272, 69);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.DeleteButton.ShadowDecoration.Parent = this.DeleteButton;
            this.DeleteButton.Size = new System.Drawing.Size(40, 40);
            this.DeleteButton.TabIndex = 43;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // Panel
            // 
            this.Panel.BackColor = System.Drawing.Color.Transparent;
            this.Panel.Controls.Add(this.typeLabel);
            this.Panel.Controls.Add(this.RemarkLabel);
            this.Panel.Controls.Add(this.PriceLabel);
            this.Panel.FillColor = System.Drawing.Color.White;
            this.Panel.Location = new System.Drawing.Point(16, 31);
            this.Panel.Name = "Panel";
            this.Panel.Radius = 10;
            this.Panel.ShadowColor = System.Drawing.Color.Gray;
            this.Panel.ShadowStyle = Guna.UI2.WinForms.Guna2ShadowPanel.ShadowMode.Dropped;
            this.Panel.Size = new System.Drawing.Size(200, 94);
            this.Panel.TabIndex = 46;
            // 
            // DateLabel
            // 
            this.DateLabel.AutoSize = true;
            this.DateLabel.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.DateLabel.Location = new System.Drawing.Point(226, 37);
            this.DateLabel.Name = "DateLabel";
            this.DateLabel.Size = new System.Drawing.Size(53, 20);
            this.DateLabel.TabIndex = 47;
            this.DateLabel.Text = "label3";
            // 
            // category
            // 
            this.category.AutoSize = true;
            this.category.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.category.Location = new System.Drawing.Point(19, 8);
            this.category.Name = "category";
            this.category.Size = new System.Drawing.Size(53, 20);
            this.category.TabIndex = 48;
            this.category.Text = "label3";
            // 
            // typeLabel
            // 
            this.typeLabel.AutoSize = true;
            this.typeLabel.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.typeLabel.Location = new System.Drawing.Point(25, 54);
            this.typeLabel.Name = "typeLabel";
            this.typeLabel.Size = new System.Drawing.Size(21, 22);
            this.typeLabel.TabIndex = 43;
            this.typeLabel.Text = "+";
            // 
            // AccountCell
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.category);
            this.Controls.Add(this.DateLabel);
            this.Controls.Add(this.Panel);
            this.Controls.Add(this.EditButton);
            this.Controls.Add(this.DeleteButton);
            this.Name = "AccountCell";
            this.Size = new System.Drawing.Size(354, 136);
            this.Panel.ResumeLayout(false);
            this.Panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Guna.UI2.WinForms.Guna2ShadowPanel Panel;
        public System.Windows.Forms.Label DateLabel;
        public System.Windows.Forms.Label RemarkLabel;
        public System.Windows.Forms.Label PriceLabel;
        public Guna.UI2.WinForms.Guna2CircleButton EditButton;
        public Guna.UI2.WinForms.Guna2CircleButton DeleteButton;
        public System.Windows.Forms.Label category;
        public System.Windows.Forms.Label typeLabel;
    }
}
