﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Scripts2
{
    public partial class UserSignIn : Form
    {
        private Api api;
        private UserBase user;
        
        public UserSignIn()
        {
            InitializeComponent();
            api = new Api();
        }

        private void SignInButton_Click(object sender, EventArgs e)
        {
            //合法性判断
            if(NameTxtField.Text == "" || PswdTxtField.Text == "")
            {
                MessageBox.Show("账号或密码不能为空");
                return;
            }
            user = new UserBase(NameTxtField.Text, PswdTxtField.Text);
            bool a = api.SignIn(user);
            if (a)
            {
                //MainPage MainPage = new MainPage("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJSZWNvcmRQcm9BUEkiLCJpYXQiOiIyMDIwLzUvMjIgMjE6NDc6MjgiLCJuYW1lIjoieXloIiwiaWQiOjE4fQ.3ExJ8FmtYVRB1S3M9aPcVYq1tmiuVKpe1CYXeske_oI");
                MainPage MainPage = new MainPage();
                this.Hide();
                MainPage.ShowDialog();
            }
            else
            {
                MessageBox.Show("您的账号或者密码有误");
            }
        }

        private void SignUpButton_Click(object sender, EventArgs e)
        {
            //合法性判断
            if (NameTxtField.Text == "" || PswdTxtField.Text == "")
            {
                MessageBox.Show("账号或密码不能为空");
                return;
            }

            user = new UserBase(NameTxtField.Text, PswdTxtField.Text);
            string result = api.SignUpUserAsync(user);
            if (result == "Created")
            {
                MessageBox.Show("创建成功");
            }
            else if (result == "Conflict")
            {
                MessageBox.Show("此用户名已被申请");
            }
            else
            {
                MessageBox.Show("服务器或者网络波动，请稍后再试");
            }
            
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.Opacity >= 0.05)
                this.Opacity -= 0.05;
            else
            {
                timer1.Stop();
                this.Close();
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private Point offset;
        private void UserSignIn_MouseDown(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button) return;
            Point cur = this.PointToScreen(e.Location);
            offset = new Point(cur.X - this.Left, cur.Y - this.Top);
        }

        private void UserSignIn_MouseMove(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button) return;
            Point cur = MousePosition;
            this.Location = new Point(cur.X - offset.X, cur.Y - offset.Y);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
