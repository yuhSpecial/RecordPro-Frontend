﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Scripts2
{
    public partial class Body : UserControl
    {
        private int flag;
        //1 表示修改 0表示post
        private int id;
        private int userid;
        private Api api;
        private DateTime dateTime;
        public Body(int id,UserBody body,DateTime dateTime,int flag)
        {
            api = new Api();
            this.dateTime = dateTime;
            this.id = id;
            this.flag = flag;
            InitializeComponent();
            this.label1.Text = this.dateTime.ToString("D");
            if (flag==0)
            {
                this.Confirm.Hide();
                this.panel1.Hide();
                HideLabel();

            }
            else
            {
                HideText();
            }
            
            Fill(body);
        }
        private void Fill(UserBody body)
        {
            this.Weight.Text = body.weight.ToString();
            this.Shoulder.Text = body.shoulder.ToString();
            this.Bust.Text = body.bust.ToString();
            this.Height.Text = body.height.ToString();
            this.Hips.Text = body.hips.ToString();
            this.Waist.Text = body.waist.ToString();
        }
        private void HideLabel()
        {
            this.Weight.Hide();
            this.Shoulder.Hide();
            this.Bust.Hide();
            this.Waist.Hide();
            this.Height.Hide();
            this.Hips.Hide();
            
        }
        private void ShowLabel()
        {
            this.Weight.Show();
            this.Shoulder.Show();
            this.Bust.Show();
            this.Waist.Show();
            this.Height.Show();
            this.Hips.Show();

        }
        private void HideText()
        {
            this.Weight_textbox.Hide();
            this.Shoulder_textbox.Hide();
            this.Bust_textbox.Hide();
            this.Waist_textbox.Hide();
            this.Height_textbox.Hide();
            this.Hips_textbox.Hide();
            this.Confirm.Hide();
        }
        private void ShowText()
        {
            this.Weight_textbox.Show();
            this.Shoulder_textbox.Show();
            this.Bust_textbox.Show();
            this.Waist_textbox.Show();
            this.Height_textbox.Show();
            this.Hips_textbox.Show();
            this.Confirm.Show();
        }
        private void EditButton_Click(object sender, EventArgs e)
        {
            HideLabel();
            ShowText();
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(api.DeleteUserBody(Api.Token, this.id));
            UCinbox_bodies panel = (UCinbox_bodies)this.Parent.Parent;
            panel.PaintPanel();
                




        }

        private void Weight_textbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void Confirm_Click(object sender, EventArgs e)
        {
            try
            {
                UserBody body = new UserBody(Convert.ToDouble(this.Weight_textbox.Text), Convert.ToDouble(this.Shoulder_textbox.Text), Convert.ToDouble(this.Height_textbox.Text), Convert.ToDouble(this.Bust_textbox.Text), Convert.ToDouble(this.Waist_textbox.Text), Convert.ToDouble(this.Hips_textbox.Text), this.dateTime, Api.Token);
                if (this.flag == 1)
                {
                    string message = api.PutUserBody(this.id, body);
                    MessageBox.Show(message);
                }
                else if (this.flag == 0)
                {
                    string message = api.PostUserBody(body);
                    MessageBox.Show(message);
                }
                HideText();
                Fill(body);
                ShowLabel();

            }
            catch(Exception ex)
            {
                MessageBox.Show("您的输入格式有误");
            }
        }
    }
}
