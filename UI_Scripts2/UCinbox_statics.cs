﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LiveCharts;
using LiveCharts.Wpf;

namespace UI_Scripts2
{
    public partial class UCinbox_statics : UserControl
    {
        private Api api;
        FaceStatistic faceSta; //群体分析
        BillStatistics billSta;
        NotesStatistic noteSta;

        private void hover()
        {
            ToolTip toolTip1 = new ToolTip();
            // 设置显示样式
            //toolTip1.AutoPopDelay = 5000;//提示信息的可见时间
            toolTip1.InitialDelay = 500;//事件触发多久后出现提示
            //toolTip1.ReshowDelay = 500;//指针从一个控件移向另一个控件时，经过多久才会显示下一个提示框
            toolTip1.ShowAlways = true;//是否显示提示框
            //  设置伴随的对象.
            toolTip1.SetToolTip(positiveModeBar, positiveModeBar.Value.ToString());//设置提示按钮和提示内容
            toolTip1.SetToolTip(neutralModeBar, neutralModeBar.Value.ToString());//设置提示按钮和提示内容
            toolTip1.SetToolTip(negativeModeBar, negativeModeBar.Value.ToString());//设置提示按钮和提示内容
            toolTip1.SetToolTip(FirstProgressBar1, FirstProgressBar1.Value.ToString());
            toolTip1.SetToolTip(SecondProgressBar2, SecondProgressBar2.Value.ToString());
            toolTip1.SetToolTip(ThirdProgressBar3, ThirdProgressBar3.Value.ToString());
            toolTip1.SetToolTip(wordAccountProgressBar5, wordAccountProgressBar5.Value.ToString());
            toolTip1.SetToolTip(incomeRatioProgressBar6, incomeRatioProgressBar6.Value.ToString());
            toolTip1.SetToolTip(outcomeRatioProgressBar4, outcomeRatioProgressBar4.Value.ToString());
            toolTip1.SetToolTip(investmentRatioProgressBar8, investmentRatioProgressBar8.Value.ToString());
            toolTip1.SetToolTip(outcomeRatioProgressBar4, outcomeRatioProgressBar4.Value.ToString());
            toolTip1.SetToolTip(smileRatioProgressBar9, smileRatioProgressBar9.Value.ToString());
            toolTip1.SetToolTip(First_emotionProgressBar6, First_emotionProgressBar6.Value.ToString());
            toolTip1.SetToolTip(Second_emotionProgressBar5, Second_emotionProgressBar5.Value.ToString());
            toolTip1.SetToolTip(Third_emotionProgressBar4, Third_emotionProgressBar4.Value.ToString());


        }
        public UCinbox_statics()
        {
            InitializeComponent();
            api = new Api();
            billSta = api.GetBillStatisticsAsync(Api.Token, "month");
            faceSta = api.GetUserFacesAnalysisAsync(Api.Token, "month", 1);
            noteSta = api.GetNoteStatistic(Api.Token, "month");
            AccountStaShow();
            NoteStaShow();
            FaceStaShow();
            hover();
        }

        private void AccountStaShow()
        {
            
            try
            {
                this.mean_income.Text = billSta.avg_income.ToString();
                this.mean_outcome.Text = billSta.avg_expense.ToString();
                this.medium_income.Text = billSta.med_income.ToString();
                this.medium_outcome.Text = billSta.med_expense.ToString();
                this.incomeRatioProgressBar6.Value = 100- (int)(billSta.income_ratio * 100);
                this.outcomeRatioProgressBar4.Value = 100- (int)(billSta.expense_ratio * 100);
                this.salaryRatioProgressBar7.Value = 100 - (int)(billSta.salary_income_ratio * 100);
                this.investmentRatioProgressBar8.Value = 100 - (int)(billSta.invest_income_ratio * 100);
            }
            catch(Exception ex)
            {
                MessageBox.Show("您在该周期内记录不足！");

            }
            
        }

        private void NoteStaShow()
        {
            this.negativeModeBar.Value = (int)(noteSta.negative_ratio * 100);
            this.positiveModeBar.Value = (int)(noteSta.positive_ratio * 100);
            this.neutralModeBar.Value = (int)(noteSta.neutral_ratio * 100);
            this.mean_mode.Text = noteSta.sentimen_mean.ToString();
            this.most_mode.Text = noteSta.sentiment_mode.sentiment.ToString();
            this.FirstProgressBar1.Value = noteSta.topthree[0].count;
            this.SecondProgressBar2.Value = noteSta.topthree[1].count;
            this.ThirdProgressBar3.Value = noteSta.topthree[2].count;




            this.First.Text = noteSta.topthree[0].key;
            this.Second.Text = noteSta.topthree[1].key;
            this.Third.Text = noteSta.topthree[2].key;
            this.wordAccountProgressBar5.Value = (int)noteSta.wordcount_mean;


        }

        private void FaceStaShow()
        {
            this.beautyRatingStar1.Value = (float)Math.Round(5-faceSta.beauty_ratio * 5,2);
            this.recordFre.Text = Math.Round(faceSta.record_frequency,2).ToString();
            this.smileRatioProgressBar9.Value = (int)(faceSta.smile_ratio*100);
            this.First_emotion.Text = faceSta.emotion_mode[0].key;
            this.Second_emotion.Text= faceSta.emotion_mode[1].key;
            this.Third_emotion.Text= faceSta.emotion_mode[2].key;
            this.First_emotionProgressBar6.Value = (int)(faceSta.emotion_mode[0].count );
            this.Second_emotionProgressBar5.Value = (int)(faceSta.emotion_mode[1].count );
            this.Third_emotionProgressBar4.Value = (int)(faceSta.emotion_mode[2].count );
            //皮肤健康饼图
            pieChart1.Series.Clear();
            //Func<ChartPoint, string> PointLabel = chartPoint => string.Format("{0} ({1:P})", "基本健康", chartPoint.Participation);
            pieChart1.Series.Add(new PieSeries
            {
                Title = "基本健康",
                Values = new ChartValues<double> { Math.Round(faceSta.skin_health_ratio.basically_healthy.ratio) },
                DataLabels = false,
                //LabelPoint = PointLabel
            });
            pieChart1.Series.Add(new PieSeries
            {
                Title = "健康",
                Values = new ChartValues<double> { Math.Round(faceSta.skin_health_ratio.healthy.ratio) },
                DataLabels = false,
                //LabelPoint = PointLabel
            });
            pieChart1.Series.Add(new PieSeries
            {
                Title = "不健康",
                Values = new ChartValues<double> { Math.Round(faceSta.skin_health_ratio.unhealthy.ratio) },
                DataLabels = false,
                //LabelPoint = PointLabel
            });
            //黑眼圈饼图
            pieChart2.Series.Clear();
            //Func<ChartPoint, string> PointLabel1 = chartPoint => string.Format("{0} ({1:P})", "基本健康", chartPoint.Participation);
            pieChart2.Series.Add(new PieSeries
            {
                Title = "基本健康",
                Values = new ChartValues<double> { Math.Round(faceSta.black_eye_ratio.basically_healthy.ratio,2) },
                DataLabels = false,
                //LabelPoint = PointLabel1
            });
            pieChart2.Series.Add(new PieSeries
            {
                Title = "健康",
                Values = new ChartValues<double> { Math.Round(faceSta.black_eye_ratio.healthy.ratio,2) },
                DataLabels = false,
                //LabelPoint = PointLabel1
            });
            pieChart2.Series.Add(new PieSeries
            {
                Title = "不健康",
                Values = new ChartValues<double> { Math.Round(faceSta.black_eye_ratio.unhealthy.ratio,2) },
                DataLabels = false,
                //LabelPoint = PointLabel1
            });
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            MainPage mainPage = (MainPage)CloseButton.Parent.Parent.Parent;
            mainPage.timer1.Start();
        }

        private void NotePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void positiveModeBar_MouseHover(object sender, EventArgs e)
        {

        }

        private void neutralModeBar_MouseHover(object sender, EventArgs e)
        {

        }

        private void negativeModeBar_MouseHover(object sender, EventArgs e)
        {

        }

        private void FirstProgressBar1_MouseHover(object sender, EventArgs e)
        {

        }

        private void SecondProgressBar2_MouseHover(object sender, EventArgs e)
        {

        }

        private void ThirdProgressBar3_MouseHover(object sender, EventArgs e)
        {

        }

        private void wordAccountProgressBar5_MouseHover(object sender, EventArgs e)
        {

        }

        private void incomeRatioProgressBar6_MouseHover(object sender, EventArgs e)
        {

        }

        private void salaryRatioProgressBar7_MouseHover(object sender, EventArgs e)
        {

        }

        private void investmentRatioProgressBar8_MouseHover(object sender, EventArgs e)
        {

        }

        private void outcomeRatioProgressBar4_MouseHover(object sender, EventArgs e)
        {

        }

        private void smileRatioProgressBar9_MouseHover(object sender, EventArgs e)
        {

        }

        //first emotion
        private void guna2VProgressBar6_MouseHover(object sender, EventArgs e)
        {

        }

        //second emotion
        private void guna2VProgressBar5_MouseLeave(object sender, EventArgs e)
        {

        }
        //third emotion
        private void guna2VProgressBar5_MouseHover(object sender, EventArgs e)
        {

        }

        private void guna2VProgressBar4_MouseHover(object sender, EventArgs e)
        {

        }

        private void outcomeRatioProgressBar4_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.Text == "女性")
            {
                this.faceSta = api.GetUserFacesAnalysisAsync(Api.Token, "month", 0);
                FaceStaShow();
            }
            else
            {
                this.faceSta = api.GetUserFacesAnalysisAsync(Api.Token, "month", 1);
                FaceStaShow();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox2.Text == "过去一周")
            {
                billSta = api.GetBillStatisticsAsync(Api.Token, "week");
                if(this.comboBox1.Text == "男性")
                {
                    faceSta = api.GetUserFacesAnalysisAsync(Api.Token, "week", 1);
                }
                else
                {
                    faceSta = api.GetUserFacesAnalysisAsync(Api.Token, "week", 0);
                }
                noteSta = api.GetNoteStatistic(Api.Token, "week");
                AccountStaShow();
                NoteStaShow();
                FaceStaShow();
            }
            else
            {
                billSta = api.GetBillStatisticsAsync(Api.Token, "month");
                if (this.comboBox1.Text == "男性")
                {
                    faceSta = api.GetUserFacesAnalysisAsync(Api.Token, "month", 1);
                }
                else
                {
                    faceSta = api.GetUserFacesAnalysisAsync(Api.Token, "month", 0);
                }
                noteSta = api.GetNoteStatistic(Api.Token, "month");
                AccountStaShow();
                NoteStaShow();
                FaceStaShow();
            }
        }
    }
}
